package com.amber.client;

import java.util.HashMap;
import java.util.Map;

 

public class ProtocolEltCreator {
	
	public static Map<Integer,String> protocolType=new HashMap<Integer,String>();	
	public static Map<Integer,String> protocolTypeContent=new HashMap<Integer,String>();	
	 
	static{
		protocolType.put(Constant.PTYPE_LOGIN,"1001");
	}
	
	public String constructBody(int type){
		/*
	   return getProtocolHead()+getLength()+getProtocolVersion()+getObdId()+getProtocolType(Constant.PTYPE_LOGIN)+
	   getContent(Constant.PTYPE_LOGIN)+getCRC()+getProtocolTail();
	   */
      //return "40407F000431303031313132353239393837000000000000001001C1F06952FDF069529C91110000000000698300000C0000000000036401014C00030001190A0D04121A1480D60488C5721800000000AF4944445F3231364730325F532056312E322E31004944445F3231364730325F482056312E322E31000000DF640D0A";
	  //return "40405900043130303131313235323939383700000000000000400101C1F06952E7F069529C9111000000000069830000070000000400036401014C00030001190A0D0412041480D60488C57218000000009F01E803ED9A0D0A";
		return "40406000043130303131313235323939383700000000000000400705000000C1F0695249F469529C9111000000000069830000D80040000400036401014C04030001190A0D04201E1480D60488C5721800000000AF0101060F000F00EA1E0D0A";
	}
	
	public String getProtocolHead(){
	    	// 2 byte
	    	return "4040";
	}
	    
	public String getLength(){
		// 2 byte
	    	return "006a";
	}
	    
	public String getProtocolVersion(){
		// 1 byte
	    	return "03";
	}
	    
	    public String getObdId(){
	    	return "48656c6c6f20776f726c64206f62642069642038";
	    }
	    
	    public String getProtocolType(int type){
	    	return protocolType.get(type);
	    }
	    
	    public String getContent(int type){
	    	return "0c0201a313031303939393931313131313030303939aaaaaaaa746573747465737007465737420736f6674776172652076657273696f6e00202068617264776172652076657273696f6e00";
	    }
	    
	    public String getCRC(){
	    	return "3132"; 
	    }
	    
	    public String getProtocolTail(){
	    	return "0d0a";
	    }
}
