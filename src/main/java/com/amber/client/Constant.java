package com.amber.client;

import java.util.Date;

import com.amber.server.data.Vehicle;

public class Constant {
	public static final int PTYPE_LOGIN=1;	
	
	public static final String PTYPE_FIELD_CONTENT="content";
	public static final String PTYPE_FIELD_TYPE="protocol_type";
	public static final String PTYPE_FIELD_DEVICE_ID="obd_id";
	public static final String PTYPE_FIELD_PROTOCOL_VERSION="protocol_version";
	
	public static final String RESPONSE_TYPE_NO="NO_RESPONSE";
	
 
//	public static final String DUMMY_IMEI="3231334531313431373030303438000000000000";
//	public static final String DUMMY_VIN="VIN_001001001";
//	public static final String DUMMY_AMBER_TOKEN="DUMMY_AMBER_TOKEN_0011";	
	public class ActionType{
		public static final String INFO_TYPE_ENGINE_STATE="engine_state";
		public static final String INFO_TYPE_ENGINE_START="engine_start";
		public static final String INFO_TYPE_ENGINE_STOP="engine_stop";
		public static final String INFO_TYPE_NO_RESPONSE="no_response";
//		public static final String INFO_TYPE_GPS_DATA_COUNT="count";
//		public static final String INFO_TYPE_ALARM_TYPE="alarm_type";
//		public static final String INFO_TYPE_ALARM_ID="alarm_id";
	}	
	public class DtcType{
		public static final String DTC_POWERTRAIN="00";
		public static final String DTC_CHASSIS="01";
		public static final String DTC_BODY="10";
		public static final String DTC_NETWORK="11";
		
		public static final String DTC_TYPE_POWERTRAIN="Powertrain";		
		public static final String DTC_TYPE_CHASSIS="Chassis";		
		public static final String DTC_TYPE_NETWORK="Network";		
		public static final String DTC_TYPE_BODY="Body";		
	}
	public class InformationType{
		public static final String INFO_TYPE_LOGIN_LAUNCH="1001";
		public static final String INFO_TYPE_LOGIN_RESPONSE="9001";
		public static final String INFO_TYPE_LOGIN_CANCEL="1002";		
		public static final String TERMINAL_UPLOAD_TYPE_GPS="4001";
		public static final String TERMINAL_UPLOAD_TYPE_PID="4002";
		public static final String TERMINAL_UPLOAD_TYPE_SENSOR="4003";
		public static final String TERMINAL_UPLOAD_TYPE_SUPPORT="4004";
		public static final String TERMINAL_UPLOAD_TYPE_SNAPSHOT="4005";
		public static final String TERMINAL_UPLOAD_TYPE_DTC_PASSENGER="4006";
		public static final String TERMINAL_UPLOAD_TYPE_DTC_COMMERCIAL="400B";
		public static final String TERMINAL_UPLOAD_TYPE_ALARMCHANGE="4007";
		public static final String TERMINAL_UPLOAD_TYPE_BSTATION_PST="4008";
		public static final String TERMINAL_UPLOAD_TYPE_FIXED_UPLOAD="4009";
		public static final String TERMINAL_HEARTBEAT="1003";
		
	}
	
	public class ResultName{
		// (double fuel,double distance,double latitude,double longitude,int speed,Date time)
		// (Date startTime, String tripId, Vehicle vehicle)
		// (String alertType,int alertId, double latitude,double longitude,int speed,Date time){
		public static final String KEY_TOTAL_FUEL="total_fuel";
		public static final String KEY_CURRENT_FUEL="current_fuel";
		public static final String KEY_DISTANCE="distance";
		public static final String KEY_CURRENT_DISTANCE="current_distance";
		public static final String KEY_LATITUDE="latitude";
		public static final String KEY_LONGITUDE="longitude";
		public static final String KEY_SPEED="speed";
		public static final String KEY_GPS_ARRAY_DATA="gps_array";
		public static final String KEY_LATITUDE_DIRECTION="KEY_LATITUDE_DIRECTION";
		public static final String KEY_LONGITUDE_DIRECTION="KEY_LONGITUDE_DIRECTION";
		
		
		public static final String KEY_DATE="date";
		public static final String KEY_START_TIME="start_time";
		public static final String KEY_TRIP_ID="trip_id";
		public static final String KEY_ALERT_TYPE="alert_type";
		public static final String KEY_ALERT_ID="alert_id";
		public static final String KEY_ACCELARATION_X="accelaration_x";
		public static final String KEY_ACCELARATION_Y="accelaration_y";
		public static final String KEY_ACCELARATION_Z="accelaration_z";
		public static final String KEY_GPS_DATA_COUNT="count";
		public static final String KEY_ALARM_TYPE="alarm_type";
		public static final String KEY_ALARM_ID="alarm_id";
		public static final String KEY_GPS_HISTORICAL_DATA="gps_historical_data";
		public static final String KEY_ACC_TIME="ACC_TIME";
		
		public static final String KEY_DTC_FLAG_TYPE="dtc_flag_type";
		
		public static final String KEY_DTC_CODE_RESULT="key_dtc_code_result";
		public static final String KEY_DEVICE_ID="device_id";
	}
}



