package com.amber.server.device;

import com.amber.server.data.Vehicle;
//import com.amber.server.db.neo4j;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 8/21/14
 * Time: 3:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParsePackets {
    Vehicle vehicle=new Vehicle();
    ParseKLV parseKLV=new ParseKLV();
  //  neo4j neo;
    int newTripStart;
    int serial=1;

    public ParsePackets() throws UnknownHostException {//"DeviceAuthenticationToken" : "AF3OPRB4", "AmberDeviceId" : "M4UZV9WEYP", "VinNumber" : "121"
        vehicle.setAmberToken("AF3OPRB4");
        vehicle.setImei("M4UZV9WEYP");
        vehicle.setVin("121");
       // neo=new neo4j();
    }
    public String parsePacketPOSITION(StringBuilder packet) throws ParseException {//	Terminal position data
        String header=getPacketHeader(packet);
        String body=getPacketBody(packet);
        StringBuilder bodyParse=new StringBuilder();
        bodyParse.append(body);
        String status = Integer.toBinaryString(ParseData.fetchParseSHORT(bodyParse));
        //System.out.println("status:"+status);
        Date date=ParseData.parseDatePacketPosition(bodyParse);
        //System.out.println("date:"+date);
        double longitude=ParseData.fetchParseINTGER(bodyParse)/100000.0;
        //System.out.println("longitude:"+longitude);
        double latitude=ParseData.fetchParseINTGER(bodyParse)/100000.0;
        //System.out.println("latitude:"+latitude);
        int speed=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("speed:"+speed);
        int direction=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("direction:"+direction);
        int altitude=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("altitude:"+altitude);
        int odometer=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("odometer:"+odometer);
        odometer=odometer==0?speed:odometer;
        HashMap<String,String> hm=getKLV(bodyParse.toString());
        //for(Map.Entry<String,String> entry:hm.entrySet()){
        //    //System.out.println(entry.getKey()+"::"+entry.getValue());
        //}
        if(newTripStart==1){
          //  neo.tripStart(date,vehicle.getAmberToken()+date.getTime(),vehicle);
            newTripStart=0;
        }
        double fuel=hm.containsKey("ff41")?ParseData.parseSHORT(hm.get("ff41"))/10000.0f:0.0f;
        int distance=hm.containsKey("ff48")?ParseData.parseSHORT(hm.get("ff48")):0;
        //neo.insertGpsPoint(fuel,distance,latitude,longitude,odometer,date);
        //HashMap<String,String> hm=getKLV(body);
        //vehicle.setImei(parseKLV.parseKLVFF03(hm.get("ffo3")));
        //vehicle.setVin(parseKLV.parseKLVFF02(hm.get("ff02")));
        String responseBody="";
        return generateResponse(header,responseBody,"6601");
    }

    public String parsePacketLOGIN_REQ(StringBuilder packet,StringBuilder configHeader){//	Terminal login request
        newTripStart=1;
        String header=getPacketHeader(packet);
        configHeader.append(header);
        String body=getPacketBody(packet);
        HashMap<String,String> hm=getKLV(body);
        //vehicle.setImei(parseKLV.parseKLVFF03(hm.get("ffo3")));
        //vehicle.setVin(parseKLV.parseKLVFF02(hm.get("ff02")));
        String responseBody="00";
        return generateResponse(header,responseBody,"6603");
    }
    public String parsePacketCONFIG_REQ(StringBuilder packet){//	Terminal synchronize parameter request
        newTripStart=1;
        String header=getPacketHeader(packet);
        String body=getPacketBody(packet);
        HashMap<String,String> hm=getKLV(body);
        //vehicle.setImei(parseKLV.parseKLVFF03(hm.get("ffo3")));
        //vehicle.setVin(parseKLV.parseKLVFF02(hm.get("ff02")));
        String responseBody=vehicle.getAlertKlv();
        return generateResponse(header,responseBody,"6605");
    }
    public byte[] parsePacketSET_RSP(String byteData){//	Terminal setting parameter answer
        return null;
    }
    public byte[] parsePacketREAD_RSP(String byteData){//	Terminal reading parameter answer
        return null;
    }
    public byte[] parsePacketUPGRADE_RSP(String byteData){//	Terminal remote update answer
        return null;
    }
    public byte[] parsePacketDOWN_REQ(String byteData){//	Terminal update data download request
        return null;
    }
    public byte[] parsePacketSPEED_RSP(String byteData){//	Terminal over speed setting answer
        return null;
    }
    public byte[] parsePacketTIRE_RSP(String byteData){//	Terminal fatigue driving setting answer
        return null;
    }
    public byte[] parsePacketDRIVE_RSP(String byteData){//	Terminal driving behavior setting answer
        return null;
    }
    public byte[] parsePacketCALL_RSP(String byteData){//	Terminal position query answer
        return null;
    }
    public byte[] parsePacketDIAGNOSIS_RSP(String byteData){//	Terminal fault reading answer
        return null;
    }
    public byte[] parsePacketPREVENT_RSP(String byteData){//	Terminal protection setting answer
        return null;
    }
    public byte[] parsePacketUPFAULT_REQ(String byteData){//	Terminal report fault code request
        return null;
    }
    public String parsePacketALARM_REQ(StringBuilder packet) throws ParseException {//	Terminal alarm information request
        String header=getPacketHeader(packet);
        String body=getPacketBody(packet);
        StringBuilder bodyParse=new StringBuilder();
        bodyParse.append(body);
        int alarmId=ParseData.fetchParseBYTE(bodyParse);
        String alarmType=getAlarmType(alarmId);
        String status = Integer.toBinaryString(ParseData.fetchParseSHORT(bodyParse));
        //System.out.println("status:"+status);
        Date date=ParseData.parseDatePacketPosition(bodyParse);
        //System.out.println("date:"+date);
        double longitude=ParseData.fetchParseINTGER(bodyParse)/100000.0;
        //System.out.println("longitude:"+longitude);
        double latitude=ParseData.fetchParseINTGER(bodyParse)/100000.0;
        //System.out.println("latitude:"+latitude);
        int speed=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("speed:"+speed);
        int direction=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("direction:"+direction);
        int altitude=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("altitude:"+altitude);
        int odometer=ParseData.fetchParseSHORT(bodyParse);
        //System.out.println("odometer:"+odometer);
        odometer=odometer==0?speed:odometer;
        HashMap<String,String> hm=getKLV(bodyParse.toString());

        /*
        if(newTripStart==0){
            neo.insertAlertPoint(alarmType,alarmId,latitude,longitude,odometer,date);
        }else if(neo.findtrip(vehicle)){
            neo.insertAlertPoint(alarmType,alarmId,latitude,longitude,odometer,date);
        }

        neo.insertToMongoDirect(vehicle, alarmType, alarmId, latitude, longitude, odometer, date);
*/
        String responseBody="00";
        return generateResponse(header,responseBody,"6629");
    }

    private String getAlarmType(int alarmId) {
        switch (alarmId){
            case 0:
                return "Over speed alarm";
            case 1:
                return "Fatigue alarm";
            case 2:
                return "Protection alarm";
            case 3:
                return "SOS alarm";
            case 4:
                return "Harsh deceleration alarm";
            case 5:
                return "Harsh acceleration alarm";
            case 6:
                return "Harsh turning alarm";
            case 7:
                return "Impact alarm";
            case 8:
                return "Rollover alarm";
            case 9:
                return "High revolution";
            case 10:
                return "In-geo-fence alarm";
            case 11:
                return "Out-geo-fence alarm";
            case 12:
                return "Open lock alarm";
            case 13:
                return "Close lock alarm ";
            case 16:
                return "Revolution mismatches speed";
            case 17:
                return "Deviate from route alarm";
            case 18:
                return "Idling alarm";
            case 19:
                return "Power off alarm";
            case 20:
                return "Plug-in alarm";
            case 21:
                return "Plug-out alarm";
        }
        return "Unknown";
    }

    public String parsePacketHSO_REQ(StringBuilder packet){//	Terminal handshake request
        String header=getPacketHeader(packet);
        String responseBody="";
        return generateResponse(header,responseBody,"6631");
    }
    public byte[] parsePacketINQUIRE_RSP(String byteData){//	Terminal reading status answer
        return null;
    }
    public String parsePacketLOGOUT_REQ(StringBuilder packet){//	Terminal logout request
        String header=getPacketHeader(packet);
        //String body=getPacketBody(byteData);
        //HashMap<String,String> hm=getKLV(body);
        //vehicle.setImei(parseKLV.parseKLVFF03(hm.get("ffo3")));
        //vehicle.setVin(parseKLV.parseKLVFF02(hm.get("ff02")));
        //String responseBody="00";
        if(newTripStart==0){
          //  neo.endTrip();
        }

        return generateResponse(header,"","6635");
    }



    public int getlength(StringBuilder packet){
        return Integer.parseInt(packet.substring(8, 12), 16);
    }
    public String getPacketCommand(StringBuilder packet){
        return packet.substring(4, 8);
    }
    public String getPacketHeader(StringBuilder packet){
        return packet.substring(0,12*2);
    }

    public String getPacketBody(StringBuilder packet){
        return packet.substring((13-1)*2);
    }
    public HashMap<String,String> getKLV(String body){
        HashMap<String,String> hmKlv=new HashMap<String, String>();
        while(body.startsWith("ff")){
            String klv=body.substring(0,getklvlength(body)*2);
            body=body.substring(getklvlength(body)*2);
            hmKlv.put(getklvKey(klv),getklvValue(klv));
        }
        return hmKlv;
    }

    private String getklvValue(String klv) {
        return klv.substring((5-1)*2);
    }

    private String getklvKey(String klv) {
        return klv.substring(0, 2*2);
    }

    public int getklvlength(String packet){
        return Integer.parseInt(packet.substring((3-1)*2, 4*2), 16);
    }

    public String generateResponse(String header,String responseBody, String responseCommand){
        String responseLength=ParseData.encodeSHORT(12 + (responseBody.length() / 2));

        return header.substring(0,2*2)+responseCommand+responseLength+header.substring((7-1)*2)+responseBody;
    }

    public String generatePacketHeader(String header){
        String returnHeader=header.substring(0,8*2)+ParseData.encodeINTGER(serial);
        serial++;
        return returnHeader;
    }

    public int processAndReturn(StringBuilder packet, DataOutputStream out) throws IOException {
        System.out.println("Request::"+packet);
        String command=getPacketCommand(packet);
        if(command.equalsIgnoreCase("8800")){
            //POSITION	Terminal position data
            String response="";
            try {
                response=parsePacketPOSITION(packet);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            //System.out.println("Request Position"+packet.toString());
            //System.out.println("Response"+response);
            out.write(hexStringToByteArray(response));
//            answerPacketPOSITION_RSP(packet,out);
        }/*else if(command.equalsIgnoreCase("6601")){
            //POSITION_RSP	Platform position data answer
        }*/else if(command.equalsIgnoreCase("8802")){
            StringBuilder configHeader=new StringBuilder();
            String response=parsePacketLOGIN_REQ(packet,configHeader);
            //System.out.println("Request Login"+packet.toString());
            //System.out.println("Response"+response);
            out.write(hexStringToByteArray(response));


            //Set Request
            String headerSet= configHeader.toString();
            String body=vehicle.getAlertKlv();
            headerSet=generatePacketHeader(headerSet);
            String setReqPacket=generateResponse(headerSet,body,"6606");
            out.write(hexStringToByteArray(setReqPacket));
//            answerPacketLOGIN_RSP(packet,out);
            //LOGIN_REQ	Terminal login request
        }/*else
        if(command.equalsIgnoreCase("6603")){
            //LOGIN_RSP	Platform login answer
        }*/else if(command.equalsIgnoreCase("8804")){
            String response=parsePacketCONFIG_REQ(packet);
            out.write(hexStringToByteArray(response));
//            answerPacketCONFIG_RSP(packet);
            //CONFIG_REQ	Terminal synchronize parameter request
        }/*else if(command.equalsIgnoreCase("6605")){
            //CONFIG_RSP	Platform synchronize parameter answer
        }*/

        /*else if(command.equalsIgnoreCase("6606")){
            //SET_REQ	Platform setting parameter command
        }else
        if(command.equalsIgnoreCase("8807")){
            //SET_RSP	Terminal setting parameter answer
        }else
        if(command.equalsIgnoreCase("6608")){
            //READ_REQ	Platform reading parameter command
        }else
        if(command.equalsIgnoreCase("8809")){
            //READ_RSP	Terminal reading parameter answer
        }else
        if(command.equalsIgnoreCase("6610")){
            //UPGRADE_REQ	Platform remote update command
        }else
        if(command.equalsIgnoreCase("8811")){
            //UPGRADE_RSP	Terminal remote update answer
        }else
        if(command.equalsIgnoreCase("8812")){
            //DOWN_REQ	Terminal update data download request
        }else
        if(command.equalsIgnoreCase("6613")){
            //DOWN_RSP	Platform update data download answer
        }else
        if(command.equalsIgnoreCase("6614")){
            //SPEED_REQ	Platform over speed setting request
        }else
        if(command.equalsIgnoreCase("8815")){
            //SPEED_RSP	Terminal over speed setting answer
        }else
        if(command.equalsIgnoreCase("6616")){
            //TIRE_REQ	Platform fatigue driving setting request
        }else
        if(command.equalsIgnoreCase("8817")){
            //TIRE_RSP	Terminal fatigue driving setting answer
        }else
        if(command.equalsIgnoreCase("6618")){
            //DRIVE_REQ	Platform driving behavior setting request
        }else
        if(command.equalsIgnoreCase("8819")){
            //DRIVE_RSP	Terminal driving behavior setting answer
        }else
        if(command.equalsIgnoreCase("6620")){
            //CALL_REQ	Platform position query request
        }else
        if(command.equalsIgnoreCase("8821")){
            //CALL_RSP	Terminal position query answer
        }else
        if(command.equalsIgnoreCase("6622")){
            //DIAGNOSIS_REQ	Platform fault reading request
        }else
        if(command.equalsIgnoreCase("8823")){
            //DIAGNOSIS_RSP	Terminal fault reading answer
        }else
        if(command.equalsIgnoreCase("6624")){
            //PREVENT_REQ	Platform protection setting request
        }else
        if(command.equalsIgnoreCase("8825")){
            //PREVENT_RSP	Terminal protection setting answer
        }*/else if(command.equalsIgnoreCase("8826")){
//            parsePacketUPFAULT_REQ(packet);
//            answerPacketUPFAULT_RSP(packet);
            //UPFAULT_REQ	Terminal report fault code request
        }/*else if(command.equalsIgnoreCase("6627")){
            //UPFAULT_RSP	Platform report fault code answer
        }*/else if(command.equalsIgnoreCase("8828")){

            //ALARM_REQ	Terminal alarm information request
            String response="";
            try {
                response=parsePacketALARM_REQ(packet);
            } catch (ParseException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            out.write(hexStringToByteArray(response));
//            answerPacketALARM_RSP(packet)
        }/*else if(command.equalsIgnoreCase("6629")){
            //ALARM_RSP	Platform alarm information answer
        }*/else if(command.equalsIgnoreCase("8830")){
            //HSO_REQ	Terminal handshake request
            String response=parsePacketHSO_REQ(packet);
            //System.out.println("Request HSO"+packet.toString());
            ////System.out.println("Response"+response);
            out.write(hexStringToByteArray(response));


        }/*else if(command.equalsIgnoreCase("6631")){
            //HSO_RSP	Platform handshake answer
        }*/
        /*else if(command.equalsIgnoreCase("6632")){
            //INQUIRE_REQ	Platform reading status request
        }else if(command.equalsIgnoreCase("8833")){
            //INQUIRE_RSP	Terminal reading status answer
        }*/
        else if(command.equalsIgnoreCase("8834")){
            //LOGOUT_REQ	Terminal logout request
            String response=parsePacketLOGOUT_REQ(packet);
            //System.out.println("Request Logout"+packet.toString());
            ////System.out.println("Response"+response);
            out.write(hexStringToByteArray(response));
            return 1;
        }/*else if(command.equalsIgnoreCase("6635")){
            //LOGOUT_RSP	Platform logout answer
        }*/
        return 0;
    }

    public static byte[] hexStringToByteArray(String s) {
        Pattern p = Pattern.compile("(\\w\\w)");
        Matcher m = p.matcher(s);
        if (m.find()) {
            // replace first number with "number" and second number with the first
            s = m.replaceAll("$1 ");
            //System.out.println(ouput);
        }

        s="c0 "+s.replaceAll("db","db dd").replaceAll("c0", "db dc")+" c0";
        s=s.replace(" ","");
        System.out.println("Response::"+s);
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static void main(String args[]) throws ParseException, IOException {
        /*StringBuilder bytes=new StringBuilder();
        bytes.append("aa018800004e00000000002a000031343038323530393232343800000000000000000000000000000000ff3000060000ff49000500ff4000060000ff4100060000ff4800060000ff4400078700ff");
        ////System.out.println(bytes.substring(8, 12));
        ////System.out.println(bytes.substring(4,8));
        ////System.out.println(bytes.length()/2);
        ParsePackets parser=new ParsePackets();
        System.out.println("Length:"+parser.parsePacketLOGIN_REQ(bytes));
        //System.out.println(bytes);*/

        /*int port = Integer.parseInt(args[0]);
        try
        {
            Thread t = new ReceiveChainsway(port);
            t.start();
        }catch(IOException e)
        {
            e.printStackTrace();
        }*/
        /*DataOutputStream dos = new DataOutputStream(System.out);
        ParsePackets parser=new ParsePackets();
        //String[] packets={"aa018802003f0000000000d2ff0100154f42445f574d4e45575f56312e30332e30ff030013383639393234303039353134353439ff42000500ff4500060503","aa01880400280000000000d3ff0100154f42445f574d4e45575f56312e30332e30ff220007504944","aa018800004e0000000000d4a00031343038323631363430333000780faf0011aebd0036006100c20060ff3000064bf6ff49000500ff40000606aaff4100060775ff4800060506ff4400078009fe","aa018800004e0000000000d5a000313430383236313634313330007815540011adf2001f006400be0037ff3000065f6cff49000500ff4000060745ff4100060a39ff480006063dff4400078005c7","aa018800004e0000000000d6a00031343038323631363432333000781ab30011ad490036006000b60063ff3000065831ff49000500ff4000060766ff410006099dff48000605ebff440007800a19","aa018800004e0000000000d7a000313430383236313634333330007820d00011ad870035005a00bc0060ff30000664fbff49000500ff40000607afff4100060b6eff48000606b3ff440007800a02","aa018800004e0000000000d8a0003134303832363136343433300078270d0011ad950027005900a9004bff3000066700ff49000500ff400006073bff4100060b2eff48000606d1ff44000780075c","aa018800004e0000000000d9a00031343038323631363435333000782b910011adf7002f005200a50054ff3000064a87ff49000500ff4000060694ff4100060715ff48000604f6ff4400078008b9","aa018800004e0000000000daa0003134303832363136343633300078312e0011af400037004e00a30069ff3000065e87ff49000500ff4000060791ff4100060abdff480006064eff4400077f0a47","aa018800004e0000000000dba000313430383236313634373330007835a70011b06c0007004a00a8000fff3000064cf6ff49000500ff400006074eff4100060848ff4800060510ff4400078003c6","aa018800002a0000000000dca000313430383236313634383035007835d90011b076000d005c00a6001a","aa018800004e0000000000dda000313430383236313634383330007837520011b0b6002c005000a90053ff3000061b1dff49000500ff4000060a5cff4100060423ff48000601ddff440007800845","aa018800004e0000000000dea00031343038323631363439333000783cee0011b0430036005600a70060ff3000065d22ff49000500ff400006074dff4100060a04ff480006062bff4400078009fe","aa018800004e0000000000dfa000313430383236313635303330007841800011b18600340039009b0063ff30000651a7ff49000500ff4000060698ff41000607efff480006055dff4400078009bb","aa018800004e0000000000e0a000313430383236313635313330007846ae0011b4b10037003800950061ff3000066467ff49000500ff400006076cff4100060afaff48000606abff4400077f0a37","aa018800004e0000000000e1a00031343038323631363532333000784bc20011b7ef0037003900900061ff3000066444ff49000500ff400006077cff4100060b0eff48000606a9ff4400077f0a2e","aa018800004e0000000000e2a000313430383236313635333330007850d90011bb1f00360038008d0061ff30000663c0ff49000500ff4000060756ff4100060ac8ff4800060699ff4400077f0a16","aa018800004e0000000000e3a000313430383236313635343330007854cf0011bef2003100180091005aff3000065e58ff49000500ff4000060728ff41000609f2ff480006063eff440007800939","aa018800004e0000000000e4a000313430383236313635353330007858250011c2bf00350032008d0065ff3000065604ff49000500ff4000060728ff4100060911ff48000605b2ff4400078009de","aa018800004e0000000000e5a00031343038323631363536333000785da40011c5590037004c00870062ff30000666b1ff49000500ff400006077dff4100060b55ff48000606c6ff440007800a31","aa018800004e0000000000e6a000313430383236313635373330007862c30011c898003a002e007c006eff3000066618ff49000500ff400006077cff4100060b42ff48000606c8ff440007800ae1","aa018800004e0000000000e7a0003134303832363136353833300078660b0011cc88003300290080005cff3000065560ff49000500ff40000606deff41000608cdff48000605adff440007800979","aa018800004e0000000000e8a00031343038323631363539333000786a4f0011d1150038002c00800069ff300006674bff49000500ff40000607daff4100060bb8ff48000606e4ff440007800a67"};
        String[] packets={"aa018802003f0000000000d2ff0100154f42445f574d4e45575f56312e30332e30ff030013383639393234303039353134353439ff42000500ff4500060503","aa01880400280000000000d3ff0100154f42445f574d4e45575f56312e30332e30ff220007504944","aa018800004e0000000000e9a00031343038323631373030333000786ea50011d574003a002d0081006fff3000066639ff49000500ff40000607b4ff4100060bd3ff48000606ccff440007800ac4","aa018800004e0000000000eaa000313430383236313730313330007872c30011d9a200370028007d0068ff300006620eff49000500ff400006071aff4100060a43ff480006067cff440007800a4e","aa018800004e0000000000eba000313430383236313730323330007876ce0011de2a00370029007f005fff300006659dff49000500ff400006078bff4100060b4cff48000606b6ff440007800a49","aa018800004e0000000000eca00031343038323631373033333000787b1a0011e1ec0034004d007c0060ff3000066100ff49000500ff400006073dff4100060a58ff4800060674ff4400077f09c7","aa018800004e0000000000eda000313430383236313730343330007880ee0011e363003400360080005cff30000663a7ff49000500ff400006079dff4100060b2eff4800060697ff440007800984","aa018800004e0000000000eea0003134303832363137303533300078848d0011e7b200330043007a0061ff3000065fceff49000500ff4000060759ff4100060a5eff4800060658ff4400077f0984","aa018800004e0000000000efa00031343038323631373036333000788a290011e8df0036005d007d0062ff3000066127ff49000500ff4000060798ff4100060adfff480006066dff4400077f09f4","aa018800004e0000000000f0a00031343038323631373037333000788ef70011e8940005005c00790009ff30000650a2ff49000500ff40000606edff410006083aff4800060543ff4400077f032d","aa018800004e0000000000f1a00031343038323631373038333000788f2d0011e8900009005f00660017ff3000060398ff49000500ff4000062a92ff4100060241ff480006003cff4400077f0635","aa018800004e0000000000f2a000313430383236313730393330007893ee0011e8300036006300750065ff3000064cc9ff49000500ff40000607d5ff41000608ddff4800060537ff440007800a12","aa018800004e0000000000f3a00031343038323631373130333000789a010011e80400340058006b0060ff30000665b1ff49000500ff4000060776ff4100060b2dff48000606adff4400077f09a7","aa018800004e0000000000f4a00031343038323631373131333000789fa00011e97500340051006e005eff300006619dff49000500ff4000060768ff4100060aa7ff4800060661ff4400077f09dd","aa018800004e0000000000f5a0003134303832363137313233300078a50c0011e8a70030006800760054ff3000065bbbff49000500ff4000060714ff4100060991ff4800060609ff4400077f08da","aa018800004e0000000000f6a0003134303832363137313333300078aa950011e70f003300540077005aff3000065f82ff49000500ff4000060799ff4100060ae6ff4800060654ff4400077f0978","aa018800004e0000000000f7a0003134303832363137313433300078afa30011e70a0036006d00710069ff3000065836ff49000500ff400006073fff410006096aff48000605d1ff4400077f0a11","aa018800004e0000000000f8a0003134303832363137313533300078b59b0011e5b70030006600700059ff300006658eff49000500ff4000060794ff4100060b56ff48000606b0ff4400077f0905","aa018800004e0000000000f9a0003134303832363137313633300078bb610011e3ef003a0072006a0067ff300006638eff49000500ff40000607c1ff4100060b60ff48000606a0ff4400077f0ac3","aa018800004e0000000000faa0003134303832363137313733300078c1420011e0f50039006f0060006bff3000066d09ff49000500ff40000607d8ff4100060c99ff480006073dff440007800a87","aa018800004e0000000000fba0003134303832363137313833300078c7370011e14000370050005f0062ff3000066562ff49000500ff40000607b3ff4100060b80ff48000606adff440007800a33","aa018800004e0000000000fca0003134303832363137313933300078cd0c0011e0bb0032005f005d005bff300006613bff49000500ff4000060764ff4100060a96ff480006066cff4400077f094d","aa018800004e0000000000fda0003134303832363137323033300078d0ba0011ded3001b0076005f0032ff3000064600ff49000500ff400006063fff4100060671ff48000604a2ff4400077f0649","aa018800004e0000000000fea0003134303832363137323133300078d4840011dd930030006a0067005bff300006410eff49000500ff4000060775ff4100060726ff4800060460ff4400077f08fd","aa018800004e0000000000ffa0003134303832363137323233300078d9520011defa0018003e0061002dff3000065871ff49000500ff40000606e9ff4100060901ff48000605d1ff4400077e05af","aa018800004e000000000100a0003134303832363137323333300078de4a0011df520031006a005a0058ff3000065300ff49000500ff4000060746ff41000608e5ff4800060584ff4400077e092b","aa018800004e000000000101a0003134303832363137323433300078e3d40011ddfb00340063004e0061ff3000065ec4ff49000500ff4000060778ff4100060a6dff4800060648ff4400077f09bc","aa018800004e000000000102a0003134303832363137323533300078e9870011dc7900350067004f0062ff3000066231ff49000500ff40000607cbff4100060b46ff4800060679ff4400077f09ff","aa018800004e000000000103a0003134303832363137323633300078ef300011dacc0036007800480062ff30000662a2ff49000500ff40000607b5ff4100060b33ff4800060688ff4400077f09fb","aa018800004e000000000104a0003134303832363137323733300078f4110011d8320031009a004c005eff3000065e76ff49000500ff4000060771ff4100060a5bff4800060642ff4400077f0964","aa018800004e000000000105a0003134303832363137323833300078f51e0011d2e5002d009a00510050ff3000065b5dff49000500ff400006074eff41000609d5ff4800060610ff4400077f0832","aa018800004e000000000106a0003134303832363137323933300078f7350011d1e20026004b00570047ff3000063152ff49000500ff40000606f8ff4100060529ff4800060340ff4400077f0738","aa018800004e000000000107a0003134303832363137333033300078fc710011d16f00310061005c005aff3000065767ff49000500ff4000060765ff4100060986ff48000605c5ff4400077f0920","aa018800004e000000000108a000313430383236313733313330007901b40011d33800340042004c0062ff3000065c6cff49000500ff4000060749ff41000609ecff4800060628ff4400077e099b","aa018800004e000000000109a0003134303832363137333233300079060c0011d6590033000d004b0063ff3000065dddff49000500ff4000060753ff4100060a21ff480006063bff4400077e0983","aa018800004e00000000010aa000313430383236313733333330007908570011db36001c0020004e003bff3000065b40ff49000500ff40000606d3ff410006092dff4800060611ff4400077e0551","aa018800004e00000000010ba0003134303832363137333433300079094c0011dcd00000000000480000ff3000061f8eff49000500ff400006087cff41000603f1ff4800060213ff4400077f02ef","aa018800004e00000000010ca000313430383236313733353330007909a20011dd58001b0022004b0033ff3000060989ff49000500ff4000061427ff41000602d4ff48000600b8ff4400077e0659","aa018800004e00000000010da00031343038323631373336333000790dc20011e02a0032002700450059ff30000654bbff49000500ff400006075aff410006092dff48000605a1ff4400077f0938","aa018800004e00000000010ea000313430383236313733373330007911c50011e457003400310040005fff300006608eff49000500ff40000607abff4100060ae9ff480006066dff4400077f09aa","aa018800004e00000000010fa000313430383236323132313536007a4c5a0013e6f60000000000100000ff3000060000ff49000500ff4000060000ff410006023aff4800060000ff4400077e0352","aa018800004e000000000110a000313430383236323132323536007a4c580013e6f700000000000e0000ff3000060000ff49000500ff4000060000ff410006023cff4800060000ff4400077e0351","aa018800004e000000000111a000313430383236323132333536007a4c560013e6f600000000000f0000ff3000060000ff49000500ff4000060000ff410006023dff4800060000ff4400077f0354","aa018800004e000000000112a000313430383236323132343536007a4c540013e6f50000000000100000ff3000060000ff49000500ff4000060000ff410006023dff4800060000ff4400077f0352","aa018800004e000000000113a000313430383236323132353536007a4c500013e6f500000000000f0000ff3000060000ff49000500ff4000060000ff410006023cff4800060000ff440007800352","aa018800004e000000000114a000313430383236323132363536007a4c4d0013e6f500000000000e0000ff3000060000ff49000500ff4000060000ff410006023cff4800060000ff440007800350","aa018800004e000000000115a000313430383236323132373536007a4c4f0013e6f70000000000120000ff3000060000ff49000500ff4000060000ff410006023cff4800060000ff440007810352","aa018800004e000000000116a000313430383236323132383536007a4c520013e6fa0000000000100000ff3000060000ff49000500ff4000060000ff410006023bff4800060000ff440007810352","aa018800004e000000000117a000313430383236323132393536007a4c560013e6f80000000000110000ff3000060000ff49000500ff4000060000ff4100060239ff4800060000ff440007820351","aa018800004e000000000118a000313430383236323133303536007a4c580013e6f80000000000110000ff3000060000ff49000500ff4000060000ff4100060238ff4800060000ff440007820352","aa018800004e000000000119a000313430383236323133313536007a4c590013e6f70000000000150000ff3000060000ff49000500ff4000060000ff4100060237ff4800060000ff440007820353","aa018800004e00000000011aa000313430383236323133323536007a4c570013e6f70000000000140000ff3000060000ff49000500ff4000060000ff4100060236ff4800060000ff440007830353","aa018800004e00000000011ba000313430383236323133333536007a4c570013e6f900000000000e0000ff3000060000ff49000500ff4000060000ff4100060235ff4800060000ff440007830352","aa018800004e00000000011ca000313430383236323133343536007a4c560013e6fa0000000000110000ff3000060000ff49000500ff4000060000ff4100060237ff4800060000ff440007830354","aa018800004e00000000011da000313430383236323133353536007a4c580013e6f90000000000120000ff3000060000ff49000500ff4000060000ff4100060235ff4800060000ff440007840352","aa018800004e00000000011ea000313430383236323133363536007a4c580013e6f80000000000130000ff3000060000ff49000500ff4000060000ff4100060a56ff4800060000ff4400078400ff","aa018834000c00000000011f"};
        for(String packet:packets){
            parser.processAndReturn(new StringBuilder(packet),dos);
        }
        dos.flush();*/

        /*Pattern p = Pattern.compile("(\\w\\w)");
        String input = "aa018802003f00000000007fff0100154f42445f574d4e45575f56312e30332e30ff030013383639393234303039353134353439ff42000500ff4500060543";
        Matcher m = p.matcher(input);
        if (m.find()) {
            // replace first number with "number" and second number with the first
            String ouput = m.replaceAll("$1 ");
            System.out.println(ouput);
        }*/


        String data="ff04000501ff110006001eff120006003cff13000550ff1400060078ff150008000000f0ff1600060e10ff46000502ff47000501";
        ParsePackets parser=new ParsePackets();

        for(Map.Entry<String,String> entry:parser.getKLV(data).entrySet()){
            System.out.println(entry.getKey()+"\t"+entry.getValue());
        }
    }
}
