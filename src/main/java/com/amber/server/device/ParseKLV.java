package com.amber.server.device;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 8/21/14
 * Time: 5:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParseKLV {
    public String parseKLVFF01(String byteData){
        //CSTRING
        //Software version  Number
        //Client end and terminal must synchronize this parameter
        return ParseData.parseCSTRING(byteData);
    }

    public String encodeKLVFF01(String byteData){
        //CSTRING
        //Software version  Number
        //Client end and terminal must synchronize this parameter
        return ParseData.parseCSTRING(byteData);
    }

    public String parseKLVFF02(String byteData){
        //CSTRING
        //VIN number
        //VIN number
        return ParseData.parseCSTRING(byteData);
    }
    public String parseKLVFF03(String byteData){
        //CSTRING
        //IMEI number
        //IMEI number
        return ParseData.parseCSTRING(byteData);
    }
    public int parseKLVFF04(String byteData){
        //BYTE
        //Protection status
        //0x00 Not in protection 0x01 In protection
        return ParseData.parseBYTE(byteData);
    }
    public int parseKLVFF05(String byteData){
        //BYTE
        //Location status
        //0x00 Disable auto-reporting location 0x01 Enable auto-reporting location
        return ParseData.parseBYTE(byteData);
    }
    public int parseKLVFF06(String byteData){
        //BYTE
        //Fault status
        //0x00 Device is normal 0x01 Device in fault
        return ParseData.parseBYTE(byteData);
    }
    public String parseKLVFF07(String byteData){
        //CSTRING
        //IP address
        //Login address
        return ParseData.parseCSTRING(byteData);
    }
    public String parseKLVFF08(String byteData){
        //CSTRING
        //Port number
        //Login port
        return ParseData.parseCSTRING(byteData);
    }
    public String parseKLVFF09(String byteData){
        //CSTRING
        //APN user
        //APN user
        return ParseData.parseCSTRING(byteData);
    }
    public String parseKLVFF10(String byteData){
        //CSTRING
        //APN password
        //APN password
        return ParseData.parseCSTRING(byteData);
    }
    public int parseKLVFF11(String byteData){
        //SHORT
        //Report interval
        //Unit: second
        return ParseData.parseSHORT(byteData);
    }
    public int parseKLVFF12(String byteData){
        //SHORT
        //Engine off delay  interval
        //Unit: second
        return ParseData.parseSHORT(byteData);
    }
    public int parseKLVFF13(String byteData){
        //BYTE
        //Over speed limit
        //Unit: km
        return ParseData.parseBYTE(byteData);
    }
    public int parseKLVFF14(String byteData){
        //SHORT
        //Over speed delay  interval
        //Unit: second
        return ParseData.parseSHORT(byteData);
    }
    public int parseKLVFF15(String byteData){
        //INTGER
        //Fatigue driving  length
        //Unit: second
        return ParseData.parseINTGER(byteData);
    }
    public int parseKLVFF16(String byteData){
        //SHORT
        //Fatigue driving  delay
        //Unit: second
        return ParseData.parseSHORT(byteData);
    }
    public int parseKLVFF17(String byteData){
        //SHORT
        //Harsh acceleration  threshold
        //
        return ParseData.parseSHORT(byteData);
    }
    public int parseKLVFF18(String byteData){
        //SHORT
        //Harsh deceleration  threshold
        //
        return ParseData.parseSHORT(byteData);
    }
    public int parseKLVFF19(String byteData){
        //SHORT
        //Harsh turn threshold
        //
        return ParseData.parseSHORT(byteData);
    }
    public int parseKLVFF20(String byteData){
        //SHORT
        //High revolution  threshold
        //Unit: RPM (round/minute) Default value: 5000 RPM
        return ParseData.parseSHORT(byteData);
    }
    public String parseKLVFF21(String byteData){
        //BIN
        //Mismatching threshold between  vehicle speed and  engine revolution
        //The threshold is combination between vehicle speed and engine revolution, default value as below:
        return ParseData.parseBIN(byteData);
    }
    public String parseKLVFF22(String byteData){
        //CSTRING
        //PID supported by  device
        //PID list supported by device
        return ParseData.parseCSTRING(byteData);
    }
    public String parseKLVFF23(String byteData){
        //CSTRING
        //Current OBD  fault code
        //Current fault code
        return ParseData.parseCSTRING(byteData);
    }
    public String parseKLVFF24(String byteData){
        //BIN
        //GPS trace info
        //Please see complicated KLV format definition
        return ParseData.parseBIN(byteData);
    }
    public String parseKLVFF25(String byteData){
        //CSTRING
        //Device update  file flag
        //30-bytes fixed length, fill will 0x00 if not enough bytes. If synchronization parameters answer or login answer contains 0XA049 and 0XA050, device start remote update.
        return ParseData.parseCSTRING(byteData);
    }
    public int parseKLVFF26(String byteData){
        //SHORT
        //Update data  packet quantity
        //Synchronization parameters answer. If login answer contains 0XA049 and 0XA050, device start remote update
        return ParseData.parseSHORT(byteData);
    }
    public float parseKLVFF27(String byteData){
        //INTGER
        //Total fuel  consumption J1939
        //Unit: 0.5L 50=25L
        return ParseData.parseINTGER(byteData)*0.5f;
    }
    public float parseKLVFF28(String byteData){
        //SHORT
        //Engine revolution  number J1939
        //Unit: 0.125rpm 200=200*0.125=25rpm
        return ParseData.parseSHORT(byteData)*0.125f;
    }
    public float parseKLVFF29(String byteData){
        //BYTE
        //Fuel level J1939
        //Unit: 0.4% 20=20*0.4%=5%
        return ParseData.parseBYTE(byteData)*0.4f;
    }
    public int parseKLVFF30(String byteData){
        //SHORT
        //Average speed: J1939/OBD
        //Unit: 1/256 Km/h 25600=100km/h
        return ParseData.parseSHORT(byteData)/256;
    }
    public int parseKLVFF31(String byteData){
        //INTGER
        //High accuracy total mileage J1939
        //Unit: 5m 50=250m
        return ParseData.parseINTGER(byteData)*5;
    }
    public int parseKLVFF32(String byteData){
        //INTGER
        //High accuracy short distance mileage
        //Unit: 5m 50=250m
        return ParseData.parseINTGER(byteData)*5;
    }
    public float parseKLVFF33(String byteData){
        //SHORT
        //Fuel consumption  speed J1939
        //Unit: 0.05L/h 100=5L/h
        return ParseData.parseSHORT(byteData)*0.05f;
    }
    public float parseKLVFF34(String byteData){
        //SHORT
        //Current fuel  economy in current vehicle speed J1939
        //Unit: 1/512 Km/L 25600=50km/L
        return ParseData.parseSHORT(byteData)/512;
    }
    public float parseKLVFF35(String byteData){
        //SHORT
        //Average real time  fuel economy during vehicle run time J1939
        //Unit: 1/512Km/l 25600=50km/L
        return ParseData.parseSHORT(byteData)/512;
    }
    public float parseKLVFF36(String byteData){
        //INTGER
        //Total mileage J1939
        //Unit: 0.125km 200=25km
        return ParseData.parseINTGER(byteData)*0.125f;
    }
    public String parseKLVFF37(String byteData){
        //CSTRING
        //Device fault code J1939
        //Fault code (Usually unit is 8-byte)
        return ParseData.parseCSTRING(byteData);
    }
    public int parseKLVFF38(String byteData){
        //INTGER
        //3 axis acceleration  value
        //High bit------low bit Combination acceleration value X axis Y axis Z axis, unit: 0.1G  10000010 means combination  Acceleration value 1G, X=0G, Y=0G, Z=1G
        return ParseData.parseINTGER(byteData);
    }
    public float parseKLVFF39(String byteData){
        //BYTE
        //Acceleration value when alarm generate
        //Unit: 0.1g 5=0.5g
        return ParseData.parseBYTE(byteData)*0.1f;
    }
    public float parseKLVFF40(String byteData){
        //SHORT
        //Fuel economy in  current average  vehicle speed OBD
        //Unit: 1/100 L/100Km 2500=25 L/100Km
        return ParseData.parseSHORT(byteData)/100;
    }
    public float parseKLVFF41(String byteData){
        //SHORT
        //Comprehensive fuel  interval
        //1/10000L
        return ParseData.parseSHORT(byteData)/10000;
    }
    public int parseKLVFF42(String byteData){
        //BYTE
        //Battery voltage  status
        //First byte: 0 normal, 1 low voltage
        return ParseData.parseBYTE(byteData);
    }
    public int parseKLVFF43(String byteData){
        //BYTE
        //Battery status
        //0 normal, 1 abnormal
        return ParseData.parseBYTE(byteData);
    }
    public String parseKLVFF44(String byteData){
        //BIN
        //Revolution temperature
        //1-byte coolant temperature (-40 to 215 C) and 2-byte revolution (max 65535)
        return ParseData.parseBIN(byteData);
    }
    public String parseKLVFF45(String byteData){
        //BIN
        //Voltage
        //2-byte voltage (1227 is 12.27 V, there t must be decimal)
        return ParseData.parseBIN(byteData);
    }
    public int parseKLVFF46(String byteData){
        //BYTE
        //Vibration alarm  sensitivity
        //1 byte, hexadecimal 0x01 Very sensitive 0x02 Sensitive 0x04 Low sensitive 0x06 Not sensitive
        return ParseData.parseBYTE(byteData);
    }
    public int parseKLVFF47(String byteData){
        //BYTE
        //OBD start status
        //0x00 Disable 0x01 Enable
        return ParseData.parseBYTE(byteData);
    }
    public int parseKLVFF48(String byteData){
        //SHORT
        //Comprehensive interval mileage
        //Unit: meter
        return ParseData.parseSHORT(byteData);
    }
    public float parseKLVFF49(String byteData){
        //BYTE
        //OBD fuel left percentage
        //Unit: 0.4% 20=20*0.4%=5%
        return ParseData.parseBYTE(byteData)*0.4f;
    }

}
