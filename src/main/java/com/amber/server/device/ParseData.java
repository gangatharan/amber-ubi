package com.amber.server.device;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 8/20/14
 * Time: 8:59 PM
 * To change this template use File | Settings | File Templates.
 */
public class ParseData {
    static int BYTE=1;
    static int SHORT=2;
    static int INTGER=4;
    static int LONG=8;
    static int FLOAT=4;
    static int DOUBLE=8;

    public static int fetchParseBYTE(StringBuilder bytedata){
        String data=bytedata.substring(0,2);
        //System.out.println(data);
        bytedata.delete(0,2);
        return parsetoInt(data);
    }
    public static int parseBYTE(String bytedata){
        return parsetoInt(bytedata);
    }
    public static String encodeBYTE(int data){
        String hex=encodeInt(data);
        while(hex.length()<BYTE*2) hex="0"+hex;
        return hex;
    }
    public static int fetchParseSHORT(StringBuilder bytedata){
        String data=bytedata.substring(0,4);
        //System.out.println(data);
        bytedata.delete(0,4);
        return parsetoInt(data);
    }
    public static int parseSHORT(String bytedata){
        return parsetoInt(bytedata);
    }
    public static String encodeSHORT(int data){
        String hex=encodeInt(data);
        while(hex.length()<SHORT*2) hex="0"+hex;
        return hex;
    }
    public static int fetchParseINTGER(StringBuilder bytedata){
        String data=bytedata.substring(0,8);
        //System.out.println(data);
        bytedata.delete(0,8);
        return parsetoInt(data);
    }
    public static int parseINTGER(String bytedata){
        return parsetoInt(bytedata);
    }
    public static String encodeINTGER(int data){
        String hex=encodeInt(data);
        while(hex.length()<INTGER*2) hex="0"+hex;
        return hex;
    }

    public static long fetchParseLONG(StringBuilder bytedata){
        String data=bytedata.substring(0,16);
        //System.out.println(data);
        bytedata.delete(0,16);
        return parsetoLong(data);
    }
    public static long parseLONG(String bytedata){
        return parsetoLong(bytedata);
    }
    public static String encodeLONG(int data){
        String hex=encodeInt(data);
        while(hex.length()<LONG*2) hex="0"+hex;
        return hex;
    }

    public static String fetchParseCSTRING(StringBuilder bytedata,int byteLength){
        String data=bytedata.substring(0,byteLength*2);
        //System.out.println(data);
        bytedata.delete(0,byteLength*2);
        return parseCSTRING(data);
    }
    public static String parseCSTRING(String bytedata){
        return parsetoString(bytedata);
    }

    public static String fetchParseBIN(StringBuilder bytedata,int byteLength){
        String data=bytedata.substring(0,byteLength*2);
        //System.out.println(data);
        bytedata.delete(0,byteLength*2);
        return parseBIN(data);
    }
    public static String parseBIN(String bytedata){
        return parsetoString(bytedata);
    }
    public static int parseFLOAT(String bytedata){
        return parsetoInt(bytedata);
    }
    public static String encodeFLOAT(int data){
        String hex=encodeInt(data);
        while(hex.length()<FLOAT*2) hex="0"+hex;
        return hex;
    }

    public static int parseDOUBLE(String bytedata){
        return parsetoInt(bytedata);
    }
    public static String encodeDOUBLE(int data){
        String hex=encodeInt(data);
        while(hex.length()<DOUBLE*2) hex="0"+hex;
        return hex;
    }

    public static int parsetoInt(String bytedata){
        return Integer.parseInt(bytedata, 16);
    }
    public static long parsetoLong(String bytedata){
        return Long.parseLong(bytedata, 16);
    }
    public static String encodeInt(int data){
        return Integer.toHexString(data);
    }

    public static String parsetoString(String bytedata){
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < bytedata.length(); i+=2) {
            String str = bytedata.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        return output.toString();
    }
    public static String encodeString(String data){
        char[] chars = data.toCharArray();
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++)
        {
            hex.append(Integer.toHexString((int) chars[i]));
        }
        return hex.toString();
    }

    public static Date parseDatePacketPosition(StringBuilder bytedata) throws ParseException {
        String data=bytedata.substring(0,12*2);
        //System.out.println(data);
        bytedata.delete(0,12*2);
        //return parseCSTRING(data);
        //String originalString = "2010-07-14 09:00:02";
        //System.out.println(parseCSTRING(data));
        return new SimpleDateFormat("yyMMddHHmmssZ").parse(parseCSTRING(data)+"-0000");
        //String newString = new SimpleDateFormat("H:mm").format(date);
    }

    public static void main(String[] args) throws ParseException {
        /*
        c0
        aa 01 88 02 00 54 00 00 00 00 00 04
        ff 01 00 15 4f 42 44 5f 57 4d 4e 45 57 5f 56 31 2e 30 33 2e 30
        ff 03 00 13 38 36 39 39 32 34 30 30 39 35 31 34 35 34 39
        ff 42 00 05 00
        ff 45 00 06 04 fc
        ff 02 00 15 36 42 45 33 45 42 30 31 33 30 31 30 31 30 31 30 31
        c0
         */
        /*System.out.println(Integer.parseInt("0013",16));
        String hex = "4f 42 44 5f 57 4d 4e 45 57 5f 56";
        hex=hex.replace(" ","");
        StringBuilder output = new StringBuilder();
        for (int i = 0; i < hex.length(); i+=2) {
            String str = hex.substring(i, i+2);
            output.append((char)Integer.parseInt(str, 16));
        }
        System.out.println(output);*/


        String hex = "313430383233303630323134007585ee0010d13d0000000001950000ff3000060000ff49000500ff4000060000ff4100060000ff4800060000ff440007ff00ff";
        hex=hex.replace(" ","");
        StringBuilder output = new StringBuilder();
        output.append(hex);
        System.out.println(output);
        Date date=parseDatePacketPosition(output);

        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date));
        System.out.println(output);


        //parseDatePacketPosition()
    }
}
