package com.amber.server.receiver;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;

import com.amber.server.Evaluate.GenericEvaluator;

public class DeviceSetQuery {	
	public DeviceSetQuery(){
		
	}	
	public void overSpeedSetting(long value){
		Map<String,String>  data=new HashMap<String,String>();
		data.put("TYPE","2001");		
		// -- cmd_seq,tlv_count
		String commandNo="0001";
		commandNo=GenericEvaluator.packetReverse(commandNo);				
		String tlvCount="01";		
		// -- tlv_array
		String tag=GenericEvaluator.packetReverse("1001");
		String tagLen=GenericEvaluator.packetReverse("0004");
		String valueArray=GenericEvaluator.packetReverse("11"+Long.toHexString(value));
		data.put("CONTENT",commandNo+tlvCount+tag+tagLen+valueArray);
		
	}	
	
	public void set(int type,long value){
		if(type==1){
			overSpeedSetting(value);
		}
	}	
	public static void main(String a[]){
		
	}
}
