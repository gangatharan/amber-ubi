package com.amber.server.receiver;

import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import org.apache.log4j.Logger;

import javax.swing.filechooser.FileSystemView;

import org.apache.commons.codec.binary.Hex;

public class BlindReceiver implements Runnable{
	
	static Logger log = Logger.getLogger(BlindReceiver.class.getName());
	/*
	static{		   
		try {
			FileHandler fh=null;
		fh = new FileHandler("log/blind_receiver_log.out",true);
		log.addHandler(fh);
		}catch(Exception exception){
			
		}		
	}
	*/
	private ServerSocket serverSocket;
	Socket server = null;
	public static void main(String a[]){
		Thread d=new Thread(new BlindReceiver());
		d.start();
	}

	public void run() {		
		try {
			System.setOut(new PrintStream(new File(FileSystemView.getFileSystemView() .getDefaultDirectory().toString()
	                + File.separator + "/obd_data/"+System.currentTimeMillis()+"_"+"sinocastle.txt")));
			serverSocket=new ServerSocket(8089);
			serverSocket.setSoTimeout(0);
			log.info("Connection waiting; port = "+8089);
	        server=serverSocket.accept();
	        log.info("Connection established ");
		} catch (IOException e) {
			System.out.println(" Failed to create socket"+e);			
		}		
		try{
			DataInputStream in = null;		
			while(true){
				in =  new DataInputStream(server.getInputStream());				
			  	byte[] bs = new byte[in.available()];		         
		      	in.read(bs);
		      	checkPacket_(bs,server);		   
			}
		}catch(Exception exception){			
		}
	}
	
	public void checkPacket_(byte[] bs,Socket server){
		try {
			boolean parse = true;
			while (parse && bs.length>0) {
				int size = findSize(bs);
				boolean hasProtocolTail = checkProtocolTail(bs);
				if (hasProtocolTail && size == bs.length) {
					log.info(convertToHex(bs));
					System.out.println(convertToHex(bs));
					parse = false;					
				} else {
					byte[] temp_ = new byte[size];
					for(int i=0;i<size;i++){
						temp_[i]=bs[i];
					}
					log.info(convertToHex(temp_));		
					System.out.println(convertToHex(temp_));
					byte[] temp = new byte[bs.length - size];
					for (int j = 0; j < (bs.length - size); j++) {
						temp[j] = bs[j];
					}
					bs=temp;
					parse = true;
				}
			}			
		}catch(Exception exception){
			log.info("Exception occured "+exception);
			exception.printStackTrace();
		}		
	}
	public int findSize(byte[] data){
		byte[] b = new byte[2];
		b[0] = data[3];
		b[1] = data[2];			
		char[] c=Hex.encodeHex(b);
		String s=new String(c);
		int size = Integer.parseInt(s,16);
		return size;
	}
	
	public boolean checkProtocolTail(byte[] data){
		byte[] b = new byte[2];
		b[0] = data[data.length - 2];			
		b[1] = data[data.length - 1];			
		String tail = convertToHex(b);
		if (tail!=null && "0d0a".equals(tail.toLowerCase())) {
			return true;
		}else{
			return false;
		}
	}
	 public static String convertToHex(byte[] data) {
		    StringBuffer buf = new StringBuffer();
		    for (int i = 0; i < data.length; i++) {
		        int halfbyte = (data[i] >>> 4) & 0x0F;
		        int two_halfs = 0;
		        do {
		            if ((0 <= halfbyte) && (halfbyte <= 9))
		                buf.append((char) ('0' + halfbyte));
		            else
		                buf.append((char) ('a' + (halfbyte - 10)));
		            halfbyte = data[i] & 0x0F;
		        } while(two_halfs++ < 1);
		    }
		    return buf.toString();
	}	
}
