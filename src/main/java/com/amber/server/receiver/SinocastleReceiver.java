package com.amber.server.receiver;

 

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;
import java.util.Observable;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.amber.client.Constant;
import com.amber.data.parser.Parser;
import com.amber.server.Evaluate.GenericEvaluator;
import com.amber.server.data.Vehicle;

public class SinocastleReceiver extends Observable implements Runnable {
	static Logger log = Logger.getLogger(SinocastleReceiver.class.getName());
	Parser parser = null;
 
	private ServerSocket serverSocket;
	private static int socketport;	
	private boolean fetchNextPacket=true;
	 
	private WatchDog watchDog=null;
	private Thread watchDogThread=null;
	private boolean dogStarted=false;
	
	
	public static int country=0;
	
	public void connectionReset(){
		log.info("Connetion reset triggered in SinocastelReceived ");
		setFetchNextPacket(false);		
	}
	
	public boolean isFetchNextPacket() {
		return fetchNextPacket;
	}

	public void setFetchNextPacket(boolean fetchNextPacket) {
		this.fetchNextPacket = fetchNextPacket;
	}

	public SinocastleReceiver(){
		
	}
	
	public SinocastleReceiver(int port){
		socketport=port;
		log.setLevel(Level.DEBUG);
		try {
			Date date=new Date(System.currentTimeMillis());			
			File f=new File("obd_data/"+date.toString().replaceAll(" |:","_")+"_"+"sinocastle.txt");
			parser=new Parser(socketport);			
			System.setOut(new PrintStream(f));			
			// start watch dog
			watchDog=new WatchDog(this);
			watchDogThread=new Thread(watchDog);			
			this.addObserver(watchDog);
		} catch (FileNotFoundException e) {
			log.debug( "Unable to create log file ; "+e);
		}
	}
	
	public void run() {
		while(true){
			try {			
			start();
			}catch(Exception exception){
				exception.printStackTrace();
				break;
			}
		}
	}
	
	public void start()throws Exception {
		Socket server = null;
		DataInputStream in = null;
		log.info("Sinocastel Starting ");
		try {
			serverSocket = new ServerSocket(socketport);
			serverSocket.setSoTimeout(0);
			log.info("Connection waiting; port = " + socketport);
			server = serverSocket.accept();
			setFetchNextPacket(true);
			log.info("New server instance created; Connection established ");
			try {
				while (isFetchNextPacket()) {					
					in = new DataInputStream(server.getInputStream());
					byte[] bs = new byte[in.available()];
					in.read(bs);
					checkPacket_(bs, server);
				}
			} catch (IOException e) {
				e.printStackTrace();
				log.debug("Exception in connection "+e);
				
			}
			try {				
				server.close();
				serverSocket.close();
				log.debug("End trip called ; isFetchNextPacket() = "+isFetchNextPacket());
				// end trip if running				
				parser.endTrip(true);				
				setFetchNextPacket(true);
				log.info("Server closed ");
			} catch (IOException e) {
				log.debug("Unable to close the connection ");
				e.printStackTrace();				
			}
		} catch (IOException e) {
			System.out.println(" Failed to create socket");
			log.error(" Failed to create socket"+e);
			e.printStackTrace();
			throw new Exception(e);
		}
	}
	
	public void checkPacket_(byte[] bs,Socket server){		
		try {
			boolean parse = true;
			if(bs.length>0){
				log.debug(" = = = = = = = = = = = = = = = = = = = = = = = New data started = = = = = = =  = =  = = = = = = = = = = = = = = ");				
				if(!dogStarted){
						watchDogThread.start();
						dogStarted=true;
			}			
			log.info("Data received "+convertToHex(bs));
			//watchDog.updateTime();
			setChanged();
			notifyObservers(null); 
			}
			while (parse && bs.length>0) {
				int size = findSize(bs);
				boolean hasProtocolTail = checkProtocolTail(bs);				
				if (hasProtocolTail && size == bs.length) {
					parser.parse(bs,this);					 	
					if (parser.getResponse() != null && parser.getResponse()!=Constant.RESPONSE_TYPE_NO) {
						log.info("Resonse  : "+parser.getResponse());						
						DataOutputStream out = new DataOutputStream(server.getOutputStream());
						out.write(GenericEvaluator.toByteArray(parser.getResponse()));
						log.info("success fully parsed the last packet sent ");
					}
					parse = false;					
				}
				else {
					byte[] temp_=new byte[size];
					for(int i=0;i<size;i++){
						temp_[i]=bs[i];
					}
					log.info("More than one packet; Currently received is "+convertToHex(temp_));
					parser.parse(temp_,this);						
					byte[] temp = new byte[bs.length - size];
					for (int i=0,j = size; i < (bs.length - size); j++,i++) {
						temp[i] = bs[j];
					}
					bs=temp;
					parse = true;
				}
			}			
		}catch(Exception exception){
			log.info("Exception occured "+exception);
			exception.printStackTrace();
		}

	}
	
	public int findSize(byte[] data){
		byte[] b = new byte[2];
		b[0] = data[3];
		b[1] = data[2];			
		char[] c=Hex.encodeHex(b);
		String s=new String(c);
		int size = Integer.parseInt(s,16);
		return size;
	}
	
	public boolean checkProtocolTail(byte[] data){
		byte[] b = new byte[2];
		b[0] = data[data.length - 2];			
		b[1] = data[data.length - 1];			
		String tail = convertToHex(b);
		if (tail!=null && "0d0a".equals(tail.toLowerCase())) {
			return true;
		}else{
			return false;
		}
	}
	
	public byte[] checkPacket(byte[] data) {		
		log.info("Data received "+convertToHex(data));
		try {
		if (data.length > 1) {			
			byte[] b = new byte[2];
			b[0] = data[2];
			b[1] = data[3];			
			char[] c=Hex.encodeHex(b);
			String s=new String(c);
			int size = Integer.parseInt(s,16);
			//log.info("Length of the packet received = " + size);
			b = new byte[2];
			b[0] = data[data.length - 2];			
			b[1] = data[data.length - 1];			
			String tail = convertToHex(b);
			if (tail!=null && "0d0a".equals(tail.toLowerCase())) {
				//log.info("valid packet received ");
				if(data.length==size){
					return data;
				}else if(size<data.length){
					log.debug("inside packet spliting; Packet length = "+data.length);
					byte[] temp=new byte[data.length-size];
					for(int j=0;j<(data.length-size);j++){
						temp[j]=data[j];
					}
					checkPacket(temp);
				}
			} else {
				//log.info("in valid packet received ");
			}
			return null;
		}else{
			return null;
		}
		}catch(Exception exception){
			log.debug( "packet checking failed "+exception);
			return null;
		}
	}
	 public static String convertToHex(byte[] data) {
		    StringBuffer buf = new StringBuffer();
		    for (int i = 0; i < data.length; i++) {
		        int halfbyte = (data[i] >>> 4) & 0x0F;
		        int two_halfs = 0;
		        do {
		            if ((0 <= halfbyte) && (halfbyte <= 9))
		                buf.append((char) ('0' + halfbyte));
		            else
		                buf.append((char) ('a' + (halfbyte - 10)));
		            halfbyte = data[i] & 0x0F;
		        } while(two_halfs++ < 1);
		    }
		    return buf.toString();
	}	
	public static void main(String a[]){
		// 14592.pts-1.behappy - india server
		//scp sinocastel.zip behappyj@208.109.101.173:/home/behappyj/
		//scp sinocastel.zip gangatharan@27.251.20.93:/home/gangatharan/
		//ssh behappyj@208.109.101.173
		//Password  : Wh@mMTp0o!Os#
		//mongo 208.109.101.173:27017/amber -u amber -p eu5us5aeBohmae		
		// kill port # kill -9 $( lsof -i:PORT_NO -t )

		// delete from neo4j ------
		//START n=node(*) WHERE n.AmberAuthToken='43JS7AIFZE68' delete n
		//MATCH (n { AmberAuthToken: '43JS7AIFZE68' })-[r]-() DELETE n, r
		//http://log.trulyhappy.org/log.out -  log file 
		
		// delete from mongo ------
		//FHEZWG5J93RT
//		> db.Trips.remove({'AmberAuthToken':'43JS7AIFZE68'});
		// db.Trips.remove({'':'TRIPID_1421209586000'});TRIPID_1420990441000
//		> db.Trips.find({'AmberAuthToken':'43JS7AIFZE68'});
//		> db.day.remove({'AmberAuthToken':'43JS7AIFZE68'});
//		> db.month.remove({'AmberAuthToken':'43JS7AIFZE68'});
//		> db.week.remove({'AmberAuthToken':'43JS7AIFZE68'});
//		> db.week.find({'AmberAuthToken':'43JS7AIFZE68'});
//		http://behappyjc.org/AmberConnect/public/trips/gettrips?Filter=&DeviceDateTime=2014-09-01%2000:00:00&DeviceOffset=19800&AmberAuthToken=43JS7AIFZE68&Vin=817543&UserToken=A5B1CF7XR8&Page=1&Limit=25
		log.info("============================================================");
		log.info("*\t\t\t\t\t\t\t   *");
		log.info("*\t\t\tSinocastel Server                  *");
		log.info("*\t\t\t\t\t\t\t   *");
		log.info("============================================================");
		log.info("");
		log.info("");
		log.info("\t\tDevice from India set [1], from USA set [2] ?");
		try{
		DataInputStream in=new DataInputStream(System.in);
		country = Integer.parseInt(in.readLine());
		int port=0;
		if(country==1){
			port=8089;
		}else if(country==2){
			port=9089;
		}
		SinocastleReceiver receiver=new SinocastleReceiver(port);
		Thread d=new Thread(receiver);
		d.start();
		d.join();
		}catch(Exception exception){
			log.error(" * * * Failed to start  * * *");
			log.error("Exception "+exception);			
		}
	}	
}

//Lat : a6695d02;Lat = -1503044350 ,417.512319444
//2014-10-27 12:50:23 INFO  GenericEvaluator findLat():248 
//findLat end
//2014-10-27 12:50:23 INFO  GenericEvaluator findLong():251 
//findLong start
//2014-10-27 12:50:23 INFO  GenericEvaluator findLong():256 
//Long : 72d58610 lon = 1926596112,535.165586667
