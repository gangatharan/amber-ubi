package com.amber.server.receiver;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.TimerTask;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 8/22/14
 * Time: 7:33 PM
 * To change this template use File | Settings | File Templates.
 */
public class ReceiveChainsway extends Thread {

    private ServerSocket serverSocket;
    private int socketport;

    public void interrupt(DataInputStream in,Thread th) {
        System.out.println("Timer Timeout");

        /*try {
            serverSocket.close();
        } catch (Exception e) {
            System.out.println("Socket Timer Exception:"+e.toString());
        } // quietly close*/
        try {
            th.interrupt();
        } catch (Exception e) {
            System.out.println("Thread Timer Exception:"+e.toString());
        } // quietly close
        /*try {
            in.close();
        } catch (Exception e) {
            System.out.println("Thread Timer Exception:"+e.toString());
        } // quietly close*/
        System.out.println("Timeout Interrupt Complete");
    }

    public ReceiveChainsway(int port) throws IOException
    {
        socketport=port;

    }

    public void run()
    {
        while (true){
            try
            {

                serverSocket = new ServerSocket(socketport);
                serverSocket.setSoTimeout(0);

                System.out.println("Waiting for client on port " +
                        serverSocket.getLocalPort() + "...");

                //timer.schedule(new RemindTask(null,timer),3*1000);

                Socket server = serverSocket.accept();
                //timer.cancel();
                System.out.println("Just connected to "
                        + server.getRemoteSocketAddress());
                DataInputStream in =
                        new DataInputStream(server.getInputStream());
               

                readNextByte(in);
                server.close();
                try{
                serverSocket.close();
                }catch(Exception s){}

            }
            catch(SocketTimeoutException s)
            {
                s.printStackTrace();
                try{
                    serverSocket.close();
                }catch(Exception ss){}
                System.out.println("Socket timed out!1");
                break;

            }catch(IOException e)
            {
                e.printStackTrace();
                try{
                    serverSocket.close();
                }catch(Exception ss){}
                System.out.println("Socket timed out!2");
                break;

            }catch (Exception ex){
                ex.printStackTrace();
                try{
                    serverSocket.close();
                }catch(Exception ss){}
                System.out.println("Socket timed out!3");
                break;
            }
        }

    }

    private void fetchPacketToEnd(StringBuilder packet, DataInputStream in, int length) throws IOException {
        while((packet.length()/2)<length){
            //System.out.println("Fetching Length:"+packet.length());
            packet.append(readNextByte(in));
        }
    }
    String readNextByte(DataInputStream in) throws IOException {
    	byte b=in.readByte();
    	
    	return ""+b;
    }
    String readNextByte1(DataInputStream in) throws IOException {
        while(in.available()<=0){
            if(Thread.currentThread().isInterrupted()){
                System.out.println("Thread Interupted");
                throw new IOException("Interupted IO");
            }
        }
        
        int byteRead = in.read();
        String hex=Integer.toHexString(byteRead);
        hex=hex.length()==1?"0"+hex:hex;
        hex=hex.length()==0?"00"+hex:hex;
        if(hex.equalsIgnoreCase("db")){
            String next=readNextByte(in);
            if(next.equalsIgnoreCase("DC")){
                return "c0";
            }else if(next.equalsIgnoreCase("DD")){
                return "db";
            }
        }
        return hex;
    }

    private void fetchHeader(StringBuilder packet, DataInputStream in) throws IOException {
        for(int i=0;i<11;i++){
            packet.append(readNextByte(in));
        }
        return;
    }

    public static byte[] hexStringToByteArray(String s) {
        byte[] b = new byte[s.length() / 2];
        for (int i = 0; i < b.length; i++) {
            int index = i * 2;
            int v = Integer.parseInt(s.substring(index, index + 2), 16);
            b[i] = (byte) v;
        }
        return b;
    }

    public static void main(String args[]){
        /*StringBuilder bytes=new StringBuilder();
        bytes.append("aa 01 88 02 00 54 00 00 00 00 00 04 ff 01 00 15 4f 42 44 5f 57 4d 4e 45 57 5f 56 31 2e 30 33 2e 30 ff 03 00 13 38 36 39 39 32 34 30 30 39 35 31 34 35 34 39 ff 42 00 05 00 ff 45 00 06 04 fc ff 02 00 15 36 42 45 33 45 42 30 31 33 30 31 30 31 30 31 30 31".replaceAll("\\s",""));
        System.out.println(bytes.substring(8,12));
        System.out.println(bytes.substring(4,8));
        System.out.println(bytes.length()/2);*/

        //int port = Integer.parseInt(args[0]);
    	int port=9091;
        while(true){
            try
            {
                Thread t = new ReceiveChainsway(port);
                t.start();
                t.join();
            }catch(IOException e)
            {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    class RemindTask extends TimerTask {
        //DataInputStream inputStream;
        //Timer timer;
        Thread th;
        public RemindTask(Thread th){
            //inputStream=in;
            //this.timer=timer;
            this.th=th;
        }
        @Override
        public void run() {
            this.cancel();
            System.out.println("ReminderTask is completed by Java timer");
            th.interrupt();
            //interrupt(inputStream,th);
            System.out.println("kill complete");
            //Not necessary because we call System.exit
            //System.exit(0); //Stops the AWT thread (and everything else)
        }
    }



}
