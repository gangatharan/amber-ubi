package com.amber.server.receiver;

import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;

public class WatchDog implements Runnable, Observer{
	
	static Logger log = Logger.getLogger(WatchDog.class.getName());
	public long currentUpdated=0;
	private SinocastleReceiver receiver=null;
	
	public WatchDog(SinocastleReceiver receiver){
		this.receiver=receiver;
	}
		
	public void updateTime(){	
			currentUpdated=System.currentTimeMillis();
			log.info("Watch dog time updaed ");		
	}
	
	public void check(){		
		long temp=System.currentTimeMillis();
		long result=temp-currentUpdated;		
		log.info("currentUpdated = "+currentUpdated+"; Current Time Ms = "+temp+"; Result = "+result); 		
		if(result>(1000*60*5)){
			log.info("Time Exceed ; Connection resetting "); 
			receiver.connectionReset();
		}
	}

	public void run() {		
		while(true){
			try {
			Thread.sleep(1000*60*5);
			check();
			}catch(Exception exception){
				exception.printStackTrace();
			}
		}
	}

	public void update(Observable o, Object arg) {
		log.info("Watch dog updated ");
		updateTime();		
	}
}
