package com.amber.server.Evaluate;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.amber.client.Constant;
import com.amber.server.receiver.SinocastleReceiver;

public class AlarmChangeEvaluator extends GenericEvaluator{
	static Logger log = Logger.getLogger(AlarmChangeEvaluator.class.getName());
	private String alarmResult="NOT_SET";
	public static Map<String,String> alarmType=new	HashMap<String,String>();
	String alarmNo;
	static{
//log\.setLevel(Level.OFF);
		alarmType.put("01","Over Speeding");
		alarmType.put("02","Low voltage");
		alarmType.put("03","Temperature alarm");
		alarmType.put("04","Abrupt acceleration");
		alarmType.put("05","Abrupt deceleration");
		alarmType.put("06","Stop running without ignition off");
		alarmType.put("07","Towing");
		alarmType.put("08","High RPM speed");
		alarmType.put("09","Power on alarm");
		alarmType.put("0A","Exhaust Emission");
		alarmType.put("0B","Quick Lane change");
		alarmType.put("0C","Sharp turn");
		alarmType.put("0D","Fatigue driving");
		alarmType.put("0E","Power off");
		alarmType.put("0F","Zone alarm");
		alarmType.put("10","Emergency alarm");
		alarmType.put("11","Collision warning");
		alarmType.put("12","Tamper alarm");
		alarmType.put("13","Illegal entry alarm");
		alarmType.put("14","Illegal ignition alarm");
		alarmType.put("15","OBD wire cut alarm");
		alarmType.put("16","ignition on");
		alarmType.put("17","ignition off");
		alarmType.put("18","MIL alam");
	}
	public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){		
		byte[] content=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);
		int totalByteConsumed=0;
		totalByteConsumed=findAlaramNumber(content, totalByteConsumed);
		totalByteConsumed=findStatsData(content, totalByteConsumed);
		totalByteConsumed=findGPSData(content, totalByteConsumed);		
		totalByteConsumed=findAlarmArray(totalByteConsumed, content);
//log\.info(getEvaluationData());
		persistToNode4J();
		return constructResponse(socketPort,deviceId,"c007",alarmNo,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_PROTOCOL_VERSION)));			
	}	
	private void persistToNode4J() {
				
	}
	public int findAlaramNumber(byte[] data,int totalByteConsumed){		
//log\.info("findAlaramNumber start");
		byte[] alarmNoInfo=extract(0, 4,data);
//log\.info("Alarm No : "+SinocastleReceiver.convertToHex(alarmNoInfo));
		alarmNo=SinocastleReceiver.convertToHex(alarmNoInfo);
		totalByteConsumed=4;
//log\.info("findAlaramNumber end");
		return totalByteConsumed;
	}
	public int findAlarmCount(byte[] data,int totalByteConsumed){
//log\.info("findAlaramNumber start");
		byte[] alarmCountInfo=extract(totalByteConsumed, 1,data);		
		String count=SinocastleReceiver.convertToHex(alarmCountInfo);
		int	alarmCount=Integer.parseInt(count);
//log\.info("Alarm count : "+alarmCount);		
//log\.info("findAlaramNumber end");
		return alarmCount;
	}	
	public int findAlarmArray(int totalByteConsumed, byte[] content) {
//log\.info("findAlarmArray start");
		int alarmCount=findAlarmCount(content, totalByteConsumed);
		int length=6*alarmCount;
		totalByteConsumed++;
		byte[] data=extract(totalByteConsumed, length,content);		
		String dt=SinocastleReceiver.convertToHex(data);
//log\.info("Alarm Array = "+dt);		
		findAlarmItem(data);
		totalByteConsumed=totalByteConsumed+data.length;		
//log\.info("findAlarmArray end");
		return totalByteConsumed;
	}
	public void findAlarmItem(byte[] data){
//log\.info("findAlarmItem  start");
		if(data.length==6){
//log\.info("Data length:"+data.length+" Valid alarm item data");
		}else{
//log\.info("Data length:"+data.length+" In valid alarm item data");
		}
		byte[] info=new byte[1];
		info[0]=data[0];
//log\.info("New alarm flag : "+SinocastleReceiver.convertToHex(info));
		info=new byte[1];
		info[0]=data[1];
		String alarm_id=SinocastleReceiver.convertToHex(info).toUpperCase();		
//log\.info("Alarm Type : (Hexa) "+SinocastleReceiver.convertToHex(info));
		if(alarmType.get(alarm_id)!=null & "ignition on".equals(alarmType.get(alarm_id))){
			alarmResult=Constant.ActionType.INFO_TYPE_ENGINE_START;			
//log\.info("Engine start ");
		}else if(alarmType.get(alarm_id)!=null & "ignition off".equals(alarmType.get(alarm_id))){
			alarmResult=Constant.ActionType.INFO_TYPE_ENGINE_STOP;			
//log\.info(" Engine stop " );
		}
		evaluationData.put(Constant.ActionType.INFO_TYPE_ENGINE_STATE, alarmResult);
		evaluationData.put(Constant.ResultName.KEY_ALERT_ID,alarm_id );
		evaluationData.put(Constant.ResultName.KEY_ALERT_TYPE,alarmType.get(alarm_id));
//log\.info("Alarm Type : (Meaning) "+alarmType.get(alarm_id));		
		info=new byte[2];
		info[0]=data[2];
		info[1]=data[3];		
//log\.info("Alarm Desc : "+SinocastleReceiver.convertToHex(info));
		info=new byte[2];
		info[0]=data[4];
		info[1]=data[5];		
//log\.info("Alarm Threshold : "+SinocastleReceiver.convertToHex(info));
//log\.info("findAlarmItem  end");
	}	
	
	
}
