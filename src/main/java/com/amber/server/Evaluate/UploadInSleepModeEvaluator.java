package com.amber.server.Evaluate;

import java.util.Map;

import org.apache.log4j.Level;

import com.amber.client.Constant;

public class UploadInSleepModeEvaluator extends GenericEvaluator{
	
	public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){		
//log\.setLevel(Level.DEBUG);
		byte[] content=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);		
		int totalByteConsumed=0;		
		totalByteConsumed=findUTCTime(0, content, totalByteConsumed);
		byte[] b=new byte[19];
		for(int i=0,j=4;i<19;i++,j++){
			b[i]=content[j];
		}
		//findGPSItem(b);
//log\.info(getEvaluationData());		
		return Constant.ActionType.INFO_TYPE_NO_RESPONSE;
	}
}
