package com.amber.server.Evaluate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.amber.client.Constant;
import com.amber.server.receiver.SinocastleReceiver;

public class PIDEvaluator extends GenericEvaluator{
	static Logger log = Logger.getLogger(PIDEvaluator.class.getName());
	List<String> pidList=new ArrayList<String>();
	
	public static Map<String,Integer> codeAndSize=new HashMap<String,Integer>();
	static{
		codeAndSize.put("2100",4 );
		codeAndSize.put("2101",4 );
		codeAndSize.put("2102",2 );
		codeAndSize.put("2103",2 );
		codeAndSize.put("2104",1 );
		codeAndSize.put("2105",1 );
		codeAndSize.put("2106",1 );
		codeAndSize.put("2107",1 );
		codeAndSize.put("2108",1 );
		codeAndSize.put("2109",1 );
		codeAndSize.put("210A",2 );
		codeAndSize.put("210B",1 );
		codeAndSize.put("210C",2 );
		codeAndSize.put("210D",1 );
		codeAndSize.put("210E",1 );
		codeAndSize.put("210F",1 );
		codeAndSize.put("2110",2 );
		codeAndSize.put("2111",1 );
		codeAndSize.put("2112",1 );
		codeAndSize.put("2113",1 );
		codeAndSize.put("2114",2 );
		codeAndSize.put("2115",2 );
		codeAndSize.put("2116",2 );
		codeAndSize.put("2117",2 );
		codeAndSize.put("2118",2 );
		codeAndSize.put("2119",2 );
		codeAndSize.put("211A",2 );
		codeAndSize.put("211B",2 );
		codeAndSize.put("211C",1 );
		codeAndSize.put("211D",1 );
		codeAndSize.put("211E",1 );
		codeAndSize.put("211F",2 );
		codeAndSize.put("2120",4 );
		codeAndSize.put("2121",2 );
		codeAndSize.put("2122",2 );
		codeAndSize.put("2123",2 );
		codeAndSize.put("2124",4 );
		codeAndSize.put("2125",4 );
		codeAndSize.put("2126",4 );
		codeAndSize.put("2127",4 );
		codeAndSize.put("2128",4 );
		codeAndSize.put("2129",4 );
		codeAndSize.put("212A",4 );
		codeAndSize.put("212B",4 );
		codeAndSize.put("212C",1 );
		codeAndSize.put("212D",1 );
		codeAndSize.put("212E",1 );
		codeAndSize.put("212F",1 );
		codeAndSize.put("2130",1 );
		codeAndSize.put("2131",2 );
		codeAndSize.put("2132",2 );
		codeAndSize.put("2133",1 );
		codeAndSize.put("2134",4 );
		codeAndSize.put("2135",4 );
		codeAndSize.put("2136",4 );
		codeAndSize.put("2137",4 );
		codeAndSize.put("2138",4 );
		codeAndSize.put("2139",4 );
		codeAndSize.put("213A",4 );
		codeAndSize.put("213B",4 );
		codeAndSize.put("213C",2 );
		codeAndSize.put("213D",2 );
		codeAndSize.put("213E",2 );
		codeAndSize.put("213F",2 );
		codeAndSize.put("2140",4 );
		codeAndSize.put("2141",4 );
		codeAndSize.put("2142",2 );
		codeAndSize.put("2143",1 );
		codeAndSize.put("2144",2 );
		codeAndSize.put("2145",1 );
		codeAndSize.put("2146",1 );
		codeAndSize.put("2147",1 );
		codeAndSize.put("2148",1 );
		codeAndSize.put("2149",1 );
		codeAndSize.put("214A",1 );
		codeAndSize.put("214B",1 );
		codeAndSize.put("214C",1 );
		codeAndSize.put("214D",2 );
		codeAndSize.put("214E",2 );
		codeAndSize.put("214F",4 );
		codeAndSize.put("2150",2 );
		codeAndSize.put("2151",1 );
		codeAndSize.put("2152",1 );
		codeAndSize.put("2153",2 );
		codeAndSize.put("2154",2 );
		codeAndSize.put("2155",2 );
		codeAndSize.put("2156",2 );
		codeAndSize.put("2157",2 );
		codeAndSize.put("2158",2 );
		codeAndSize.put("2159",2 );
		codeAndSize.put("215A",1 );		
	}
	public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){
		try {
		byte[] data=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);
		int totalByteConsumed=0;
		totalByteConsumed=findStatsData(data,totalByteConsumed);
		totalByteConsumed=findCollectionInterval(data,totalByteConsumed);
		totalByteConsumed=findPIDCount(data, totalByteConsumed);
		totalByteConsumed=findGroupCount(data, totalByteConsumed);
		}catch(Exception exception){
			log.error("Faile to process "+exception);
		}
		return Constant.RESPONSE_TYPE_NO;
	}
	public int findGroupContent(byte[] data,int totalByteConsumed,int count,int size){
		int length=size*count;
		byte[] content=extract(totalByteConsumed, length,data);
		String dt=SinocastleReceiver.convertToHex(content);
		int bytesUsed=0;
		for(String pid:pidList){	
			pid=pid.toUpperCase();
			if(codeAndSize.get(pid)!=null){
				int codeLength=codeAndSize.get(pid);
				byte b[]=extract(bytesUsed, codeLength,content);
				String value=SinocastleReceiver.convertToHex(b);
				if(codeLength>1){
					value=packetReverse(value);
				}
				log.info("Code = "+pid+"Value = "+value);
				if("210D".equals(pid)){
					log.info("Speed -------------------> "+value+";"+Integer.parseInt(value,16)+"km/hr");
					
				}
				bytesUsed=bytesUsed+codeLength;
			}
		}
		log.info("Group content = "+dt);
		totalByteConsumed=totalByteConsumed+length;
		return totalByteConsumed;
	}
	public int findGroupSize(byte[] data,int totalByteConsumed,int count){
		int length=1;
		byte[] content=extract(totalByteConsumed, length,data);
		String dt=SinocastleReceiver.convertToHex(content);
		int size=Integer.parseInt(dt);		
		log.info("Group size = "+dt);
		totalByteConsumed=totalByteConsumed+length;
		totalByteConsumed=findGroupContent(data, totalByteConsumed, count, size);
		return totalByteConsumed;
	}
	public int findGroupCount(byte[] data,int totalByteConsumed){
		int length=1;
		byte[] content=extract(totalByteConsumed, length,data);
		String dt=SinocastleReceiver.convertToHex(content);
		int count=Integer.parseInt(dt);
		log.info("Group count = "+dt);
		totalByteConsumed=totalByteConsumed+length;
		totalByteConsumed=findGroupSize(data,totalByteConsumed,count);
		return totalByteConsumed;
	}
	public int findCollectionInterval(byte[] data,int totalByteConsumed){
		int length=2;
		byte[] content=extract(totalByteConsumed, length,data);
		String dt=SinocastleReceiver.convertToHex(content);
		dt=packetReverse(dt);
		log.info("Collection interval  =   "+dt);
		totalByteConsumed=totalByteConsumed+length;
		return totalByteConsumed;
	}	
	public int findPIDCount(byte[] data,int totalByteConsumed){
		int length=1;
		byte[] content=extract(totalByteConsumed, length,data);
		String dt=SinocastleReceiver.convertToHex(content);
		int count=Integer.parseInt(dt,16);
		log.info("PID count =   "+count);
		totalByteConsumed=totalByteConsumed+length;
		totalByteConsumed=findContentTypeArray(data, totalByteConsumed,count);
		return totalByteConsumed;
	}
	public int findContentTypeArray(byte[] data,int totalByteConsumed,int contentCount){
		int length=2*contentCount;
		byte[] content=extract(totalByteConsumed, length,data);
		String dt=SinocastleReceiver.convertToHex(content);
		log.info("Pid content : "+dt);
		totalByteConsumed=totalByteConsumed+length;
		int cndtCount=dt.length()/4;
		for(int i=0;i<cndtCount;i++){
			int index=i*4;
			int endIndex=((i+1)*4);
			String pid=dt.substring(index, endIndex);
			pid=packetReverse(pid);
			pidList.add(pid);
			log.info("Id= "+pid);
		}
		return totalByteConsumed;
	}
	
}
