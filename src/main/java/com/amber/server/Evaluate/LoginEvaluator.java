package com.amber.server.Evaluate;


import java.util.Map;

import org.apache.log4j.Logger;

import com.amber.client.Constant;
import com.amber.server.receiver.SinocastleReceiver;

public class LoginEvaluator extends GenericEvaluator{
	static Logger log = Logger.getLogger(LoginEvaluator.class.getName());
	static{
//log\.setLevel(Level.OFF);
	}
	/*
	static{		   
		try {
			FileHandler fh=null;
		fh = new FileHandler("log/log.out",true);
//log\.addHandler(fh);
		}catch(Exception exception){
			
		}		
	}
	*/
	 
	public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){
		byte[] content=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);
		int totalByteConsumed=0;
		totalByteConsumed=findStatsData(content,totalByteConsumed);
		totalByteConsumed=findGPSData(content,totalByteConsumed);		
		totalByteConsumed=findSoftwareVersion(content,totalByteConsumed);
		totalByteConsumed=findHardwareVersion(content,totalByteConsumed);
//log\.info("Response construction....");
		//TODO: String host="1bfb145d";		
		String host="FFFFFFFF";
//log\.info(" Reverse of port start");
		String port=Integer.toHexString(socketPort);		
		//TODO: port=packetReverse(port);
		port="0000";
//log\.info(" Reverse of port end");
//log\.info(" Reverse of time start");
		long l=System.currentTimeMillis()/1000L;		
		String time=Long.toHexString(l);
		time=packetReverse(time);
//log\.info(" Reverse of time end ");
		
		String content_=(host+port+time);
//log\.info("Response content : "+content_);
		String protocolVersion=SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_PROTOCOL_VERSION));
//log\.info(getEvaluationData());
		
		return constructResponse(socketPort,deviceId,"9001",content_,protocolVersion);
	}	
//	public String constructResponse_(int socketPort,String deviceId){
//			String host="ffffffff";
//			String port=Integer.toHexString(socketPort);			
//			String time=Long.toHexString(System.currentTimeMillis());			
//			String protocolHead="4040";
//			String protocolLength="29";
//			String protocolVersion="03";
//			String deviceID=deviceId;
//			String informationType="9001";
//			
//			String content=host+port+time;
//			
//			byte[] crcByte=(protocolHead+protocolLength+protocolVersion+deviceID+informationType+content).getBytes();
//			CRC  name = new CRC();			
//			int crcValue=name.CRC_MakeCrc(crcByte, crcByte.length);			
//			String crc=Integer.toHexString(crcValue);;
//			log.info("CRC value = "+crc);			
//			String protocolEndSign="0d0A";
//			log.info("response consructed success fully "+protocolHead+protocolLength+protocolVersion+deviceId+informationType+content+crc+protocolEndSign);
//			return protocolHead+protocolLength+protocolVersion+deviceId+informationType+content+crc+protocolEndSign;			
//	}
//	
	
	public int findSoftwareVersion(byte[] content,int totalByteConsumed){
//log\.info("findSoftware version start");
		totalByteConsumed=findVersion((totalByteConsumed),content);
//log\.info("findSoftware version end");
		return totalByteConsumed;
	}	
	public int findHardwareVersion(byte[] content,int totalByteConsumed){
//log\.info("findHardware version start");
		totalByteConsumed=findVersion((totalByteConsumed),content);
//log\.info("findHardware version end");
		return totalByteConsumed;
	}
	private void findNewParamCount(byte[] content) {
//log\.info("find stats data start ");
//log\.info("findNewParamCount end ");		
	}
	private void findNewParamArray(byte[] content) {
//log\.info("findNewParamArray start ");		
//log\.info("findNewParamArray end ");
	}
}
