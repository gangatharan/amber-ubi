package com.amber.server.Evaluate;

import java.util.Map;

import com.amber.client.Constant;

public class BaseStationPositionUpload extends GenericEvaluator{
	public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){
		
		int totalByteConsumed=0;
		byte[] content=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);
		totalByteConsumed=findStatsData(content, totalByteConsumed);
		totalByteConsumed=findLocationAreaCode(content,totalByteConsumed);
		totalByteConsumed=findCellId(content,totalByteConsumed);
		//log.info(getEvaluationData());
		return Constant.ActionType.INFO_TYPE_NO_RESPONSE;
	}
	private int findCellId(byte[] content, int totalByteConsumed) {
		return totalByteConsumed;	
	}
	private int findLocationAreaCode(byte[] content, int totalByteConsumed) {
		return totalByteConsumed;	
	}
}

