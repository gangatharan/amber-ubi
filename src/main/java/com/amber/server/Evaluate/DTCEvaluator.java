package com.amber.server.Evaluate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amber.client.Constant;
import com.amber.server.receiver.SinocastleReceiver;
public class DTCEvaluator extends GenericEvaluator{
		private String dtcType;
		private int dtcCodeSize;
		public DTCEvaluator(String dtcType){
			this.dtcType=dtcType;
			if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_DTC_PASSENGER.equals(dtcType)){
				dtcCodeSize=2;
			}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_DTC_PASSENGER.equals(dtcType)){
				dtcCodeSize=4;
			}
		}
		public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){
			int totalByteConsumed=0;
			try{
				byte[] data=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);		
				totalByteConsumed=findStatsData(data, totalByteConsumed);
				totalByteConsumed=findFaultFlag(data, totalByteConsumed);
				totalByteConsumed=findFaultCount(data, totalByteConsumed);				
			}catch(Exception exception){
				log.error("Failed to process the DTC code "+exception);
				exception.printStackTrace();
			}
			return Constant.ActionType.INFO_TYPE_NO_RESPONSE;
		}
		public int findFaultFlag(byte[] data,int totalByteConsumed){			
			byte[] content=extract(totalByteConsumed, 1,data);
			totalByteConsumed++;
			log.debug("Fault flag identification : "+content.length);
			if(content.length>0){
				byte[] b=new byte[1];
				b[0]=content[0];
				String flag=SinocastleReceiver.convertToHex(b);
				log.info("Fault flag type : "+flag);
				if("00".equals(flag)){
					evaluationData.put(Constant.ResultName.KEY_DTC_FLAG_TYPE,"store");
					log.info("Fault flag identified  : store ");
				}else if("01".equals(flag)){
					evaluationData.put(Constant.ResultName.KEY_DTC_FLAG_TYPE,"pending");
					log.info("Fault flag identified  : pending ");
				}
			}
			return totalByteConsumed;
		}	
		public int findFaultCount(byte[] data,int totalByteConsumed){						
			byte[] content=extract(totalByteConsumed, 1,data);
			totalByteConsumed++;
			log.debug("Fault count ; Content length "+content.length);
			if(content.length>0){
				byte[] b=new byte[1];
				b[0]=content[0];
				String faultCount=SinocastleReceiver.convertToHex(b);
				int count=Integer.parseInt(faultCount);
				log.info("Count identified : "+count);
				if(count>0){
					findFaultArray(data, totalByteConsumed,count);
				}
			}
			return totalByteConsumed;
		}		
		public int findFaultArray(byte[] data,int totalByteConsumed,int count){			
			if(count>0){				
				int length=dtcCodeSize*count;
				byte[] content=extract(totalByteConsumed, length,data);
				String type="";
				try {
				String bits = String.format("%8s", Integer.toBinaryString(content[0] & 0xFF)).replace(' ', '0');
				log.info(bits);
				    type=bits.substring(0, 2);
				if(type.equals(Constant.DtcType.DTC_BODY)){
					type="B";
				}else if(type.equals(Constant.DtcType.DTC_CHASSIS)){
					type="C";
				}else if(type.equals(Constant.DtcType.DTC_NETWORK)){
					type="N";
				}else if(type.equals(Constant.DtcType.DTC_POWERTRAIN)){
					type="P";
				}
				}catch(Exception exception){
					log.info("Failed to identify the DTC type "+exception);
				}
				String dtcNo=SinocastleReceiver.convertToHex(content);				
				log.debug("DTC No : "+dtcNo);
				dtcNo=packetReverse(dtcNo);
				int startIndex=0;
				int endIndex=dtcCodeSize*2;
				List<String> code=new ArrayList<String>();
				try {
				for(int i=0;i<count;i++){
					String dtcCode=type+dtcNo.substring(startIndex, endIndex);
					code.add(dtcCode);
					log.info("DTC Code = "+dtcCode);					
					startIndex=endIndex;
					endIndex=endIndex+dtcCodeSize;
				}				
				}catch(Exception exception){
					log.debug("Failed to extract data "+exception);					
				}
				evaluationData.put(Constant.ResultName.KEY_DTC_CODE_RESULT, code);
			}
			return totalByteConsumed;
		}
		public static void main(String a[]){
			String dtcNo="430004323133";
			int dtcCodeSize=2*2;
			int count=3;
			int startIndex=0;
			int endIndex=dtcCodeSize;
			
			try {
				System.out.println(dtcNo);
			for(int i=0;i<count;i++){
				String dtcCode=dtcNo.substring(startIndex, endIndex);
				System.out.println(dtcCode);
				startIndex=endIndex;
				endIndex=endIndex+dtcCodeSize;
			}
			}catch(Exception exception){
				log.debug("asd");
			}
		}	
}
