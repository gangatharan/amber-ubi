package com.amber.server.Evaluate;

import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.amber.client.Constant;
import com.amber.server.receiver.SinocastleReceiver;

public class TerminalUploadGPSDataEvaluator extends GenericEvaluator{

	static Logger log = Logger.getLogger(TerminalUploadGPSDataEvaluator.class.getName());

	static{
//log\.setLevel(Level.OFF);
	}
	
	public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){		
		int totalByteConsumed=0;
		byte[] data=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);
		totalByteConsumed=findGpsDataMark(data);
		totalByteConsumed=findStatsData(data, totalByteConsumed);
		totalByteConsumed=findGPSData(data, totalByteConsumed);
		totalByteConsumed=findRPMData(data, totalByteConsumed);
		//return constructResponse(socketPort, deviceId, informationType, content);
//log\.info(getEvaluationData());
		return Constant.ActionType.INFO_TYPE_NO_RESPONSE;
	}	
	public int findGpsDataMark(byte[] data){
		if(data.length>0){
			byte[] b=new byte[1];
			b[0]=data[0];
			String flag=SinocastleReceiver.convertToHex(b);
			if("00".equals(flag)){
				evaluationData.put(Constant.ResultName.KEY_GPS_HISTORICAL_DATA, "conventional");
			}else if("01".equals(flag)){
				evaluationData.put(Constant.ResultName.KEY_GPS_HISTORICAL_DATA, "historical");
			}
			log.info("Type of GPS Data = "+evaluationData.get(Constant.ResultName.KEY_GPS_HISTORICAL_DATA));			
		}
		return 1;
	}
	
}

