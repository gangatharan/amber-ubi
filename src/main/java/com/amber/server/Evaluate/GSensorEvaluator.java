/*

 * See the NOTICE file distributed with this work for additional

 * information regarding copyright ownership.

 *

 * This is free software; you can redistribute it and/or modify it

 * under the terms of the GNU Lesser General Public License as

 * published by the Free Software Foundation; either version 2.1 of

 * the License, or (at your option) any later version.

 *

 * This software is distributed in the hope that it will be useful,

 * but WITHOUT ANY WARRANTY; without even the implied warranty of

 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU

 * Lesser General Public License for more details.

 *

 * You should have received a copy of the GNU Lesser General Public

 * License along with this software; if not, write to the Free

 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA

 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.

 */

package com.amber.server.Evaluate;



import java.util.Map;



import org.apache.log4j.Level;

import org.apache.log4j.Logger;



import com.amber.client.Constant;

import com.amber.server.receiver.SinocastleReceiver;



/**

 * 

 * @version $Id$

 */

public class GSensorEvaluator extends GenericEvaluator{

	static Logger log = Logger.getLogger(GSensorEvaluator.class.getName());

	static{
//log\.setLevel(Level.OFF);

	}

	public String evaluate(Map<String, Object> parsedData, int socketPort,String deviceId){

		

		byte[] content=(byte[]) parsedData.get(Constant.PTYPE_FIELD_CONTENT);

		int totalByteConsumed=0;

		totalByteConsumed=findStatsData(content,totalByteConsumed);

		totalByteConsumed=findSamplingCollectionInterval(content, totalByteConsumed);

		totalByteConsumed=findGroupcount(content, totalByteConsumed);
//log\.info("GSensor evaluation data : "+evaluationData);		

		return Constant.RESPONSE_TYPE_NO;

	}	

	public int findSamplingCollectionInterval(byte[] content,int totalByteConsumed){
//log\.info("findLastAccessTime start");

		int length=2;

		byte[] data=extract(totalByteConsumed, length,content);

		String dt=SinocastleReceiver.convertToHex(data);		

		totalByteConsumed=totalByteConsumed+data.length;

		try {
//log\.info("-->"+dt);

			dt=packetReverse(dt);

			int interval=Integer.parseInt(dt,16);
//log\.info("Sampling collection interval "+interval);

		}catch(Exception exception){
//log\.error(exception);

		}
//log\.info("findLastAccessTime end");

		return totalByteConsumed;

	}

	

	public int findGroupcount(byte[] content,int totalByteConsumed){
//log\.info("find group count start");

		int length=1;

		byte[] data=extract(totalByteConsumed, length,content);

		totalByteConsumed=totalByteConsumed+data.length;

		String dt=SinocastleReceiver.convertToHex(data);

		try{

			int count=Integer.parseInt(dt);

			if(count>0){

				totalByteConsumed=findAccelaration(content,totalByteConsumed,Constant.ResultName.KEY_ACCELARATION_X);				

				totalByteConsumed=findAccelaration(content,totalByteConsumed,Constant.ResultName.KEY_ACCELARATION_Y);

				totalByteConsumed=findAccelaration(content,totalByteConsumed,Constant.ResultName.KEY_ACCELARATION_Z);

			}

		}catch(Exception exception){
//log\.error("Exception occured "+exception);

		}
//log\.info("find group count end");

		return totalByteConsumed;

	}

	

	public int findAccelaration(byte[] content,int totalByteConsumed,String type){
//log\.info("find accelaration start "+ type);

		int length=2;

		byte[] data=extract(totalByteConsumed, length,content);

		totalByteConsumed=totalByteConsumed+data.length;

		String dt=SinocastleReceiver.convertToHex(data);

		try{

			int accelaration=Integer.parseInt(dt,16);

			evaluationData.put(type, ""+accelaration);

		}catch(Exception exception){
//log\.error("Exception occured "+exception);

		}
//log\.info("find accelaration end "+type);

		return totalByteConsumed;

	}

}

