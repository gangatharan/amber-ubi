package com.amber.server.Evaluate;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.amber.client.Constant;
import com.amber.data.parser.CRC;
import com.amber.server.receiver.SinocastleReceiver;

public class GenericEvaluator {	
	public static Map<Integer,List<String>> alarm=new HashMap<Integer,List<String>>();
	static{		
		List<String> l1=new ArrayList<String>();
		l1.add("Low voltage alarm");		l1.add("Towing alarm");		l1.add("Over speed alarm");		l1.add("Too high temprature alarm");		l1.add("Abrupt acceleratioon alarm");		l1.add("Abrupt deceleratioon alarm");		l1.add("Parking without ignition off");		l1.add("Exhaust Emission alarm");
		List<String> l2=new ArrayList<String>();
		l2.add("High RPM alarm");		l2.add("Power on");		l2.add("quick lane change");		l2.add("sharp turn");		l2.add("Fatigue driving");		l2.add("Emergency alarm");		l2.add("Tamper alarm");		l2.add("crsh alarm");
		List<String> l3=new ArrayList<String>();
		l3.add("Illegal entry alarm");		l3.add("Illegal ignition alarm");		l3.add("Ignition condition");		l3.add("Privacy status");		l3.add("No GPS device access");		l3.add("Power-off alarm");		l3.add("OBD write cut alarm");		l3.add("IL DTC Alarm");
		alarm.put(0, l1);		alarm.put(1, l2);		alarm.put(2, l3);
		
	}
	static Logger log = Logger.getLogger(GenericEvaluator.class.getName());
	public GenericEvaluator(){		
	}
	public Map<String,Object> evaluationData=new HashMap<String,Object>();
	public Map<String, Object> getEvaluationData() {
		return evaluationData;
	}
	public void setEvaluationData(Map<String, Object> evaluationData) {
		this.evaluationData = evaluationData;
	} 
	public int findStatsData(byte[] content,int totalByteConsumed){	
		totalByteConsumed=findLastAccessTime(totalByteConsumed,content,totalByteConsumed);
		totalByteConsumed=findUTCTime(totalByteConsumed, content,totalByteConsumed);		
		totalByteConsumed=findTripMileage(totalByteConsumed, content,totalByteConsumed);
		totalByteConsumed=findCurrentTripMileage(totalByteConsumed, content,totalByteConsumed);
		totalByteConsumed=findTotalFuel(totalByteConsumed, content,totalByteConsumed);
		totalByteConsumed=findCurrentFuel(totalByteConsumed, content,totalByteConsumed);
		totalByteConsumed=findVstate(totalByteConsumed,content,totalByteConsumed);
		totalByteConsumed=findReserve(totalByteConsumed,content,totalByteConsumed);
		return totalByteConsumed;
	}
	public   int findLastAccessTime(int indexPst,byte[] content, int totalByteConsumed){
		int length=4;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);		
		totalByteConsumed=totalByteConsumed+data.length;
		Date date=null;
		try {
			dt=packetReverse(dt);
			long data_ =Long.parseLong(dt,16);
			TimeZone tzone = TimeZone.getTimeZone("GMT");		      
		    // set time zone to default
		    tzone.setDefault(tzone);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
			date=new Date(data_*1000L);
			String i=sdf.format(date);
		}catch(Exception exception){
			log.error(exception);
			exception.printStackTrace();
		}
		//log.info("ACC time : "+dt+"; Date = "+date);
		evaluationData.put(Constant.ResultName.KEY_ACC_TIME, date);
		return totalByteConsumed;
	}
	public int findUTCTime(int indexPst,byte[] content, int totalByteConsumed){
		int length=4;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);		
		totalByteConsumed=totalByteConsumed+data.length;
		try{
			dt=packetReverse(dt);
			long data_ =Long.parseLong(dt,16);
			TimeZone tzone = TimeZone.getTimeZone("GMT");		      
		    // set time zone to default
		    tzone.setDefault(tzone);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
		//	sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
			Date date=new Date(data_*1000L);
			String i=sdf.format(date);						
			//log.info("time (Current time of device ) : "+dt+"; Date = "+date);
			// BUG Fix : time irrelevant
			evaluationData.put(Constant.ResultName.KEY_DATE, date);
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return totalByteConsumed;
	}
	public    int findTripMileage(int indexPst,byte[] content, int totalByteConsumed){
 		int length=4;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;
 		try{
			dt=packetReverse(dt);			
			long lat=Long.parseLong(dt,16);
	 		double tripMileage =lat;
	 		evaluationData.put(Constant.ResultName.KEY_DISTANCE, ""+tripMileage);
 		}catch(Exception exception){
			log.error(exception);
			exception.printStackTrace();
		} 
		return totalByteConsumed;
	}
	public    int findCurrentTripMileage(int indexPst,byte[] content, int totalByteConsumed){
 		int length=4;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;
 		try{
			dt=packetReverse(dt);			
			long lat=Long.parseLong(dt,16);
	 		double currentTripMileage =lat;
 			evaluationData.put(Constant.ResultName.KEY_CURRENT_DISTANCE, ""+currentTripMileage);
		}catch(Exception exception){
			log.error(exception);exception.printStackTrace();
		}
		return totalByteConsumed;
	}
	public    int findTotalFuel(int indexPst,byte[] content, int totalByteConsumed){
 		int length=4;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;
 		 try{
			 dt=packetReverse(dt);
			 double totalFuel=(Integer.parseInt(dt,16))*0.01;			 
			 evaluationData.put(Constant.ResultName.KEY_TOTAL_FUEL, totalFuel);
		 }catch(Exception exception){
			 log.error(exception);exception.printStackTrace();
		 }
		return totalByteConsumed;
	}
	public int findCurrentFuel(int indexPst,byte[] content,int totalByteConsumed){
 		int length=2;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;
 		try{			
			dt=packetReverse(dt);
			double currentFuel=(Integer.parseInt(dt,16))*0.01;
 			evaluationData.put(Constant.ResultName.KEY_CURRENT_FUEL, ""+currentFuel);			
		}catch(Exception exception){
			log.error(exception);exception.printStackTrace();
		}
 		return totalByteConsumed;
	}
	public    int findVstate(int indexPst, byte[] content, int totalByteConsumed) {		 
 		int length=4;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;
		List<String> alarmResult=new ArrayList<String>();
		for(int i=0;i<3;i++){
			String s1 = String.format("%8s", Integer.toBinaryString(data[i] & 0xFF)).replace(' ', '0');			
			alarmResult=findAlarm(s1, i, alarmResult);
		}
		//log.info("FROM VSTATE INFORMATION following alerts  "+alarmResult);		
 		return totalByteConsumed;
	}
	public List<String> findAlarm(String s,int index,List<String> alarmResult){
		char elt[]=s.toCharArray();
		int i=7;
		for(char e:elt){
			if(e=='1'){
				alarmResult.add(alarm.get(index).get(i));
			}
			i--;			
		}	
		return alarmResult;
	}
	public int findReserve(int indexPst, byte[] content, int totalByteConsumed) {
 		int length=8;
		byte[] data=extract(indexPst, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;
 		return totalByteConsumed;
	}
	public int findRPMData(byte[] content,int totalByteConsumed){
 		String data=findRPMCount((totalByteConsumed),content);
		String dt[]=data.split(",");
		totalByteConsumed=Integer.parseInt(dt[0]);
		int gpsCount=Integer.parseInt(dt[1]);
		if(gpsCount>0){
			totalByteConsumed=findRPMArray((totalByteConsumed),content,gpsCount);
		}
 		return  totalByteConsumed;
	}
	public  String findRPMCount(int totalByteConsumed, byte[] content) {
 		int length=1;
		byte[] data=extract(totalByteConsumed, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;		
 		return totalByteConsumed+","+Integer.parseInt(dt, 16);
	}
	
	public int findGPSData(byte[] content,int totalByteConsumed){		
		String data=findGPSCount((totalByteConsumed),content);
		String dt[]=data.split(",");
		totalByteConsumed=Integer.parseInt(dt[0]);
		int gpsCount=Integer.parseInt(dt[1]);
		if(gpsCount>0){
			totalByteConsumed=findGPSArray((totalByteConsumed),content,gpsCount);
		}
		return totalByteConsumed;
	}
	
	public  int findRPMArray(int totalByteConsumed, byte[] content,int gpsCount) {
		int length=2*gpsCount;
		byte[] data=extract(totalByteConsumed, length,content);		
		String dt=SinocastleReceiver.convertToHex(data);
		if(data.length==2){
		
		}
		totalByteConsumed=totalByteConsumed+data.length;
		return totalByteConsumed;
	}	
	public   String findGPSCount(int totalByteConsumed, byte[] content) {
		int length=1;
		byte[] data=extract(totalByteConsumed, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		totalByteConsumed=totalByteConsumed+data.length;
		return totalByteConsumed+","+Integer.parseInt(dt, 16);
	}
	
	public   int findGPSArray(int totalByteConsumed, byte[] content,int gpsCount) {
		return findGPSArray_(totalByteConsumed, content, gpsCount);
//		int length=19*gpsCount;
//		byte[] data=extract(totalByteConsumed, length,content);		
//		String dt=SinocastleReceiver.convertToHex(data);
//		findGPSItem(data);
//		totalByteConsumed=totalByteConsumed+data.length;				
//		return totalByteConsumed;
	}
	public int findGPSArray_(int totalByteConsumed, byte[] content,int gpsCount) {
		List<Map<String,Object>> gpsData=new ArrayList<Map<String,Object>>();
		for(int i=1;i<=gpsCount;i++){
			int length=19;
			byte[] data=extract(totalByteConsumed, length,content);		
			String dt=SinocastleReceiver.convertToHex(data);
			Map<String,Object> gps=new HashMap<String,Object>();
			gps=findGPSItem(data,gps);
			gpsData.add(gps);
			totalByteConsumed=totalByteConsumed+data.length;
		}
		//log.info(gpsData);
		evaluationData.put(Constant.ResultName.KEY_GPS_ARRAY_DATA,gpsData);
		return totalByteConsumed;
	}
	public Map<String,Object> findGPSItem(byte[] data,Map<String,Object> gps){
			String dateInfo=findGPSDate(data);
			findGPSTime(data,dateInfo);		
			gps=findLat(data, gps);
			gps=findLong(data, gps);
			gps=findSpeed(data, gps);
			findDirection(data);
			gps=findValFlag(data,gps);
			return gps;
	}	
	
	private   String findGPSDate(byte[] content){
		int length=3;
		byte[] data=extract(0, length,content);		
		String dateInfo="";				
		Byte b=data[0];		
		dateInfo+=b.intValue()+"-";
		b=data[1];
		dateInfo+=b.intValue()+"-";
		b=data[2];		
		dateInfo+="20"+b.intValue();

		return dateInfo;
		
	}	
	private   void findGPSTime(byte[] content, String dateInfo){
		int length=3;
		byte[] data=extract(3, length,content);				
		String dt=SinocastleReceiver.convertToHex(data);
		String timeInfo="";
		Byte b=data[0];
		timeInfo+=b.intValue()+":";
		b=data[1];
		timeInfo+=b.intValue()+":";		
		b=data[2];
		timeInfo+=b.intValue();
		
		String time=new String(data);
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");	
			TimeZone tzone = TimeZone.getTimeZone("GMT");	      
			// set time zone to default
		    tzone.setDefault(tzone);
			//sdf.setTimeZone(TimeZone.getTimeZone("GMT"));			
			Date dateObj=sdf.parse(dateInfo+" "+timeInfo);			
			//BUG:evaluationData.put(Constant.ResultName.KEY_DATE, dateObj);			
		}catch(Exception exception){
			log.error(exception);exception.printStackTrace();
		}	
	}
	private   Map<String,Object>  findLat(byte[] content,Map<String,Object> gps){
		int length=4;
		byte[] data=extract(6, length,content);
		String dt=SinocastleReceiver.convertToHex(data);		
		try{
			dt=packetReverse(dt);
			long lat=Long.parseLong(dt,16);
			double result =lat/3600000.0;			
			evaluationData.put(Constant.ResultName.KEY_LATITUDE, ""+result);
			gps.put(Constant.ResultName.KEY_LATITUDE, ""+result);
		}catch(Exception exception){
			log.error("Exception "+exception);exception.printStackTrace();
		}
		return gps;
	}
	private   Map<String,Object>  findLong(byte[] content,Map<String,Object> gps){
		int length=4;
		byte[] data=extract(10, length,content);		
		String dt=SinocastleReceiver.convertToHex(data);
		try{
			dt=packetReverse(dt);
			long lon=Long.parseLong(dt,16);
			double result=lon/3600000.0;
			evaluationData.put(Constant.ResultName.KEY_LONGITUDE, ""+result);
			gps.put(Constant.ResultName.KEY_LONGITUDE, ""+result);
		}catch(Exception exception){
			log.error("Exception "+exception);exception.printStackTrace();
		}
		return gps;
	}
	private   Map<String,Object>  findSpeed(byte[] content,Map<String,Object> gps){
		int length=2;
		byte[] data=extract(14, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		try{
			String dt_=packetReverse(dt);
			int speedInfo=Integer.parseInt(dt_,16);
			double speedPerHour=speedInfo*(0.036);
			Double double1=new Double(speedPerHour);
			evaluationData.put(Constant.ResultName.KEY_SPEED, ""+double1.intValue());
			gps.put(Constant.ResultName.KEY_SPEED,  ""+double1.intValue());
		}catch(Exception exception){
			log.error(" Exception "+exception);exception.printStackTrace();
		}
		return gps;
	}
	 
	private void findDirection(byte[] content) {
		int length=2;
		byte[] data=extract(16, length,content);
		String dt=SinocastleReceiver.convertToHex(data);
		dt=packetReverse(dt);
		int direction=Integer.parseInt(dt,16);
		log.info("Direction : "+direction);
			
	}
	 
	private   Map<String,Object>  findValFlag(byte[] content,Map<String,Object> gps){
		int length=1;
		byte[] data=extract(18, length,content);		
		try {
		String s1 = String.format("%8s", Integer.toBinaryString(data[0] & 0xFF)).replace(' ', '0');
						
		log.info("Before "+s1);
		StringBuilder sbi=new StringBuilder(s1);
		s1=sbi.reverse().toString();
		log.info("After "+s1);
		//bit 0 - longitiude
		//bit 1 - latitude		
		String bit_0=s1.substring(0, 1);
		String bit_1=s1.substring(1, 2);
		String longitude="";
		String latitude="";		
		if(bit_0.equals("1") || bit_0.equals("0")){
			if(bit_0.equals("1")){
				evaluationData.put(Constant.ResultName.KEY_LONGITUDE_DIRECTION, "+");
			}else{
				evaluationData.put(Constant.ResultName.KEY_LONGITUDE_DIRECTION, "-");				
			}
			gps.put(Constant.ResultName.KEY_LONGITUDE,evaluationData.get(Constant.ResultName.KEY_LONGITUDE_DIRECTION).toString()+gps.get(Constant.ResultName.KEY_LONGITUDE).toString());
		}
		if(bit_1.equals("1") || bit_1.equals("0")){
			if(bit_1.equals("1")){
				evaluationData.put(Constant.ResultName.KEY_LATITUDE_DIRECTION, "+");
				
			}else{
				evaluationData.put(Constant.ResultName.KEY_LATITUDE_DIRECTION, "-");				
			}
			gps.put(Constant.ResultName.KEY_LATITUDE,evaluationData.get(Constant.ResultName.KEY_LATITUDE_DIRECTION).toString()+gps.get(Constant.ResultName.KEY_LATITUDE).toString());
		}
		}catch(Exception exception){
			log.error("Failed to identify the east west latitude "+exception);			
		}
		//String dt=SinocastleReceiver.convertToHex(data);
		return gps;
	}
	public   int findVersion(int totalByteConsumed, byte[] content) {
		boolean find=true;
		int index=totalByteConsumed;
		int i=1;
		try{
		while(find){
			byte[] b=new byte[1];
			b[0]=content[index];
			String hex=SinocastleReceiver.convertToHex(b);						
			if(hex.equals("00")){
				find=false;
				byte[] version=extract(totalByteConsumed, i,content);
				totalByteConsumed=totalByteConsumed+i;				
				return totalByteConsumed;
			}else{
				index++;
				i++;
			}
		}
		}catch(Exception exception){
			log.error("Failed "+exception);			
		}
		return 0;
	}	
	public  byte[] extract(int indexPst, int length, byte[] content) {
		byte[] data = new byte[length];
		try {
			for (int i = indexPst, j = 0; i < ((indexPst + length)); i++, j++) {
				data[j] = content[i];
			}
		} catch (Exception exception) {			
				log.error( "Failed to identify bit location "+exception);
		}
		return data;
	}	
	public static byte[] toByteArray(String s) {

		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character.digit(s.charAt(i + 1), 16));
		}
		return data;	
		}
	public String constructResponse(int socketPort,String deviceId,String informationType,String content, String protocolVersion){
		String protocolHead="4040";		
		String deviceID=deviceId;		
		String hexProtocolLength="2900";	
		String protocolBody=protocolHead+hexProtocolLength+protocolVersion+deviceID+informationType+content;
		byte[] crcByte=toByteArray(protocolBody);
		CRC  name = new CRC();			
		int crcValue=name.CRC_MakeCrc(crcByte, crcByte.length);			
		String crc=Integer.toHexString(crcValue);;		
		if(crc.length()<4){
			int count=4-crc.length();
			String zeroPrefix="";
			for(int i=1;i<=count;i++){
				zeroPrefix+="0";
			}
			crc=zeroPrefix+crc;
			
		}
		crc=packetReverse(crc);
		String protocolEndSign="0d0a";
		return protocolHead+hexProtocolLength+protocolVersion+deviceId+informationType+content+crc+protocolEndSign;			
	}
	public static void milisecond_to_decimal(long a) {
		long s=a/1000;
		int d=(int)s/3600;
		s=s/3600;
		int m=(int) (s/60);
		s=s%60;
		long result=d+(((m*60)+(s))/3600)-0;
		System.out.println("Result = "+result);
    }
	
	public static String packetReverse(String data){
		String a[]=new String[data.length()/2];		
		char[] dataArray=data.toCharArray();
		for(int i=0,j=0;i<data.length();i+=2,j++){
			a[j]=dataArray[i]+""+dataArray[i+1];
		}
		data="";
		for(int i=a.length-1;i>=0;i--){
			data+=a[i];
		}

		return data;
	}	
	public static void main(String a[]){	 
		System.out.println(packetReverse("54c5cb82"));
		long l=Long.parseLong("54c5cb82");
		
	}
}
