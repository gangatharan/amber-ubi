package com.amber.server.db;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.DynamicLabel;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.rest.graphdb.RestAPIFacade;
import org.neo4j.rest.graphdb.RestGraphDatabase;
import org.neo4j.rest.graphdb.query.RestCypherQueryEngine;
import org.neo4j.rest.graphdb.util.QueryResult;

import com.amber.client.Constant;
import com.amber.data.parser.Parser;
import com.amber.server.data.Vehicle;
import com.mongodb.DBObject;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 8/23/14
 * Time: 1:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class Neo4j {
	Logger log = Logger.getLogger(Neo4j.class.getName());
	GraphDatabaseService gds;
    Node tripNode;
    Node lastNode;
    mongo mongoObj;
    Vehicle vehicleData;
    Node lastLocation;
    String neo4jUrl="http://208.109.101.173:7474/db/data";
    int overSpeedLimit=0;
    //String neo4jUrl="http://localhost:7474/db/data";
    int speedMonitior=0;
    public Node getLastLocation() {
  		return lastLocation;
  	}
  	public void setLastLocation(Node lastLocation) {
  		this.lastLocation = lastLocation;
  	}
    public void insertToMongoDirect(Vehicle vehicle, String alarmType, String alarmId, double latitude, double longitude, int odometer, Date date) {
        mongoObj.alertEntry(vehicle.getAmberToken(),vehicle.getVin(),latitude, longitude, odometer, alarmType, alarmId, date);
    }
    public int sppedLimit(){
    	return mongoObj.findOverSpeedInfo();
    }
    private static enum RelTypes implements RelationshipType
    {
        NextPoint;
    }
    public Neo4j() throws UnknownHostException {
        mongoObj=new mongo();
        lastLocation=null;
        gds = new RestGraphDatabase(neo4jUrl);
    }
    public Node tripNodeSetProperty(Node tripNode,Date startTime, String tripId, Vehicle vehicle){
    		tripNode=gds.createNode(DynamicLabel.label("Trip"));
    		tripNode.setProperty("startTime",startTime);
    		tripNode.setProperty("tripId",tripId);
    		tripNode.setProperty("AmberAuthToken",vehicle.getAmberToken());
    		tripNode.setProperty("VINNumber",vehicle.getVin());
    		tripNode.setProperty("IdleFlag",false);
    		long idletime=0;
    		tripNode.setProperty("IdleTime",idletime);	
    		tripNode.setProperty("distance",0.0);
    	return tripNode;         
    }
    public void createBufferedTripStart(Date startTime, String tripId, Vehicle vehicle){
    	Node bufferNode=null;
    	bufferNode=tripNodeSetProperty(bufferNode, startTime, tripId, vehicle);
    	bufferNode.setProperty("Fuel", 0.0);
    	bufferNode.setProperty("TopSpeed",0);
    	bufferNode.setProperty("Time", startTime);
    	bufferNode.setProperty("startTime", startTime);    	
    	mongoInsertNewTrip(bufferNode,0);
    	mongoInsertTripSave(bufferNode, bufferNode, 0, true);
    }
    
    public void removeBufferedTripStart(String tripId){
    	try {
    	 RestCypherQueryEngine engine ;
         QueryResult<Map<String, Object>> result;
         engine = new RestCypherQueryEngine(new RestAPIFacade(neo4jUrl));
         result = engine.query("MATCH (n:`Trip`) WHERE n.tripId='"+tripId+"' delete n", null);                  
         result = engine.query("MATCH (n { tripId:'" +tripId +"' })-[r]-() DELETE n, r", null);
    	}catch(Exception exception){
    		log.error("Faile to delete the buffered trip start data"+exception);
    	}
                  
         
    }
    
    public void tripStart(Date startTime, String tripId, Vehicle vehicle){
        vehicleData=vehicle;
        lastNode=null;
        lastLocation=null;
        tripNode=null;
        log.info(vehicle);
        if(!findExistingTrip()){
//            tripNode=gds.createNode(DynamicLabel.label("Trip"));
//            tripNode.setProperty("startTime",startTime);
//            tripNode.setProperty("tripId",tripId);
//            tripNode.setProperty("AmberAuthToken",vehicle.getAmberToken());
//            tripNode.setProperty("VINNumber",vehicle.getVin());
//            tripNode.setProperty("IdleFlag",false);
//            long idletime=0;
//            tripNode.setProperty("IdleTime",idletime);
//            tripNode.setProperty("distance",0.0);
        	tripNode=tripNodeSetProperty(tripNode,startTime,tripId,vehicle);
        }
        fetchLastnode();
        fetchNode();
        //tripNode.setProperty("Fuel",0);
        //ha	5
        //sb	2
        //speeding	1
        //TopSpeed	46
        //distance	5.47
    }
    
    

    public boolean findtrip(Vehicle vehicle){
        vehicleData=vehicle;
        lastNode=null;
        lastLocation=null;
        tripNode=null;
        if(findExistingTrip()){
            fetchLastnode();
            fetchNode();
            return true;
        }
        return false;
    }
    public boolean findExistingTrip(){
        RestCypherQueryEngine engine ;
        QueryResult<Map<String, Object>> result;
        engine = new RestCypherQueryEngine(new RestAPIFacade(neo4jUrl));
        result = engine.query("MATCH (n:`Trip`) where n.AmberAuthToken = '" + vehicleData.getAmberToken()
                + "' and n.VINNumber='" + vehicleData.getVin() + "' return n", null);
        for (Map<String, Object> row : result) {
            for(Map.Entry<String,Object> entry:row.entrySet()){
                tripNode=(Node)entry.getValue();
                return true;
            }
        }
        return false;
    }
    public void fetchNode(Vehicle vehicle){
    	if(vehicleData==null){
    		this.vehicleData=vehicle;
    		fetchNode();
    	}
    }
    public void fetchNode(){     
    	if(vehicleData!=null) {
        RestCypherQueryEngine engine ;
        QueryResult<Map<String, Object>> result;
        engine = new RestCypherQueryEngine(new RestAPIFacade(neo4jUrl));
            result = engine.query("MATCH (n:`LastLocation`) where n.AmberAuthToken = '" + vehicleData.getAmberToken()
                    + "' and n.VINNumber='" + vehicleData.getVin() + "' return n", null);
        for (Map<String, Object> row : result) {
            for(Map.Entry<String,Object> entry:row.entrySet()){
                lastLocation=(Node)entry.getValue();
            }
        }
        }
    }
    public int checkAndUpdateOverspeedLimit(){
    	boolean overSpeed=false;
        if(speedMonitior>7){
        	speedMonitior=0;
        }        
        if(speedMonitior==0){
        	try{
        	overSpeedLimit=mongoObj.findOverSpeedInfo();
        	log.info("Over speed limit from Mongo : "+overSpeedLimit);
        	}catch(Exception exception){
        		log.error("Exception occured while fetching over speed limit from mongo "+exception);	
        	}        	
        }
        speedMonitior++;      
        return overSpeedLimit;
    }
    
    public void insertGpsPoint(double fuel,double distance,double latitude,double longitude,int speed,Date time,Parser parser,boolean idelTimeExist){
        log.info("Insert Gps point called fuel = "+fuel+", distance ="+distance+", latitude="+latitude+", longitude="+longitude+", speed="+speed+", time="+time);
        log.debug("Speed monitior count "+speedMonitior);
        if(latitude==0 && longitude==0 && lastLocation!=null){
            latitude=(Double)lastLocation.getProperty("latitude");
            longitude=(Double)lastLocation.getProperty("longitude");
        }
        Node currentNode=gds.createNode(DynamicLabel.label("GPSPoint"));
        currentNode.setProperty("Fuel",fuel);
        currentNode.setProperty("Journey",distance);
        currentNode.setProperty("latitude",latitude);
        currentNode.setProperty("longitude",longitude);
        currentNode.setProperty("Speed",speed);
        currentNode.setProperty("Time",time);
       // Code 
        int overSpeedLimit=checkAndUpdateOverspeedLimit();
        log.debug("Speed = "+speed+" && over speed limit = "+overSpeedLimit);
        if(overSpeedLimit!=0 && overSpeedLimit<=speed  && speed!=0){        	 
          	log.info("Over speed identified"); 
        	insertAlertPoint("OVERSPEED","01", latitude, longitude, overSpeedLimit, time, parser);
             //Bug fix  - over speed not shown in alert section but showing in trips section ; end 
             log.info("Over speed identified; call to php  ");
             otherAlerts(latitude, longitude, "OVERSPEED", speed+"","01");
             log.info("Over speed identified; call to php  success ");
             log.info("Over speed identified");
        }            
        boolean idleFlag;
        long idleTime=0l;
        if(lastNode == null){
            tripNode.createRelationshipTo(currentNode,RelTypes.NextPoint);
            tripNode.setProperty("Fuel",fuel);
            tripNode.setProperty("TopSpeed",speed);
            tripNode.setProperty("distance",distance);
            idleFlag= speed==0?true:false;
            idleTime=0l;
            tripNode.setProperty("IdleFlag",idleFlag);
            tripNode.setProperty("IdleTime",idleTime);
        }else{
            idleFlag=(Boolean)tripNode.getProperty("IdleFlag");
            idleTime=Long.parseLong(tripNode.getProperty("IdleTime").toString());
            lastNode.createRelationshipTo(currentNode,RelTypes.NextPoint);
            tripNode.setProperty("Fuel",fuel);
            int topspeed=(Integer)tripNode.getProperty("TopSpeed");
            if(speed>topspeed){ 
            	tripNode.setProperty("TopSpeed",speed);
            	log.debug("Top speed : "+speed);            	
            }            
            try{
            	tripNode.getProperty("distance");
            }catch(Exception exception){
            	tripNode.setProperty("distance",(Double)0d);
            }
            //distance=distance-Double.parseDouble(lastNode.getProperty("Journey").toString());
            tripNode.setProperty("distance",distance);           
            if(idleFlag=true && speed==0 && idelTimeExist){            	
                Date lastTime=new Date((Long)lastLocation.getProperty("Time"));
                try {
                if((time.getTime()-lastTime.getTime())>0){
                	idleTime=idleTime+(time.getTime()-lastTime.getTime());
                    log.debug("Idel Time identified  : "+idleTime);	
                } else{
                	log.debug("Idel Time identified  is less than 0 : "+(time.getTime()-lastTime.getTime()));
                }}catch(Exception exception){
                	log.error(exception);                	
                }
                tripNode.setProperty("IdleTime",idleTime);
                tripNode.setProperty("IdleFlag",true);
            }else if(speed==0){
                tripNode.setProperty("IdleFlag",true);
            }else if(speed!=0){
                tripNode.setProperty("IdleFlag",false);
            }
        }
        lastNode=currentNode;
        if(lastLocation==null){
            lastLocation=gds.createNode(DynamicLabel.label("LastLocation"));
            lastLocation.setProperty("AmberAuthToken",vehicleData.getAmberToken());
            lastLocation.setProperty("VINNumber",vehicleData.getVin());
        }
        lastLocation.setProperty("latitude",latitude);
        lastLocation.setProperty("longitude",longitude);
        lastLocation.setProperty("Time",time);
        lastLocation.setProperty("RunningStatus",1);
        log.info("Geo fence update called ");
        geoFenceCall(latitude,longitude);
    }


    public void insertAlertPoint(String alertType,String alertId, double latitude,double longitude,int speed,Date time,Parser parser){
    	log.debug("insert alert point called ");    	
    	if(latitude==0 && longitude==0){
            latitude=(Double)lastLocation.getProperty("latitude");
            longitude=(Double)lastLocation.getProperty("longitude");
        }
        Node currentNode=gds.createNode(DynamicLabel.label("AlertPoint"));
        currentNode.setProperty("AlertType",alertType);
        currentNode.setProperty("AlertId",alertId);
        currentNode.setProperty("latitude",latitude);
        currentNode.setProperty("longitude",longitude);
        currentNode.setProperty("Speed",speed);
        currentNode.setProperty("Time",time);
        if(lastNode == null){
            tripNode.createRelationshipTo(currentNode,RelTypes.NextPoint);
            tripNode.setProperty("TopSpeed",speed);
        }else{
            lastNode.createRelationshipTo(currentNode,RelTypes.NextPoint);
            int topspeed=(Integer)tripNode.getProperty("TopSpeed");
            if(speed>topspeed) tripNode.setProperty("TopSpeed",speed);
        }
        lastNode=currentNode;
        //ha	5
        //sb	2
        //speeding	1
        if(alertId.equals("01") ){
            if(tripNode.hasProperty("speeding")){
                tripNode.setProperty("speeding",((Integer)tripNode.getProperty("speeding"))+1);
            }else{
                tripNode.setProperty("speeding",1);//speeding   0
            }
            alertId="";            		
        }else if(alertId.equals("05")){
            if(tripNode.hasProperty("sb")){
                tripNode.setProperty("sb",((Integer)tripNode.getProperty("sb"))+1);
            }else{
                tripNode.setProperty("sb",1);//sb   4
            }
        }else if(alertId.equals("04")){
            if(tripNode.hasProperty("ha")){
                tripNode.setProperty("ha",((Integer)tripNode.getProperty("ha"))+1);
            }else{
                tripNode.setProperty("ha",1);//ha   5
            }
        }
        if(lastLocation==null){
            lastLocation=gds.createNode(DynamicLabel.label("LastLocation"));
            lastLocation.setProperty("AmberAuthToken",vehicleData.getAmberToken());
            lastLocation.setProperty("VINNumber",vehicleData.getVin());
        }
        lastLocation.setProperty("latitude",latitude);
        lastLocation.setProperty("longitude",longitude);
        lastLocation.setProperty("Time",time);
        
        if(parser.isEngineRunning){
        	lastLocation.setProperty("RunningStatus",1);
        }else{
        	lastLocation.setProperty("RunningStatus",0);	
        }       	
        otherAlerts(latitude, longitude, alertType, speed+"",alertId);        
    }
    public void endTripFromNeo(){
    	try {
    		log.debug("Trip will be deleted from neo4j only ");
            if((Double)tripNode.getProperty("distance")==0){
                deleteTrip();
                log.error("Delete from neo4j success");
            }
    	}catch(Exception exception){
    		log.debug("Failed to delete from neo4j ");
    	}
    }
    public void endTrip(boolean isForcefullStop){
    	log.debug("End trip called "+"; isForceFullStop = "+isForcefullStop);
        String tripId= (String)tripNode.getProperty("tripId");
        lastLocation.setProperty("RunningStatus",0);
        lastLocation.setProperty("ParkedTime",lastLocation.getProperty("Time"));
        try {
        if((Double)tripNode.getProperty("distance")==0){
            deleteTrip();
            log.error("Delete from neo4j success");
        }else{
            traverseandMovetoMongo(isForcefullStop);
            deleteTrip();
            hitUrl(tripId);
            log.debug("traverse to mongo called ");
        }
        }catch(org.neo4j.graphdb.NotFoundException notFoundException){
        	notFoundException.printStackTrace();
        	log.error("Distance property not found ", notFoundException);
        	tripNode.setProperty("distance",0);
            deleteTrip();
            lastLocation.setProperty("RunningStatus",0);
            lastLocation.setProperty("ParkedTime",lastLocation.getProperty("Time"));
            log.error("Delete from neo4j success");
        }
        log.debug("End trip called ");        
    }
    public void hitUrl(String tripId) {
        //System.out.println("ReminderTask is completed by Java timer");        
        URL oracle = null;
        try {
            oracle = new URL("http://behappyjc.org/AmberConnect/public/trips/updatetripname?TripId="+tripId);
            BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) inputLine="";
            //System.out.println(inputLine);
            in.close();
            
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    
    public static void alertsHitUrl(String url) {
        //System.out.println("ReminderTask is completed by Java timer");

        URL oracle = null;
        try {
            oracle = new URL(url);
            BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) inputLine="";
            //System.out.println(inputLine);
            in.close();
            
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    
	public void otherAlerts(double latitude,double longitude,String alertType,String speedValue,String alertId){
    	/* Following three alerts will be handled : 
    	 * SUDDENBRAKE - 
		 * HARSHACCELERATION -04
		 * OVERSPEED -01
    	 */
		log.info("Other alerts called  ");
    	String type="";
    	 String url="";
    	if(alertId.equals("01")){
    		type="OVERSPEED";
    	}else if(alertId.equals("04")){
    		type="HARSHACCELERATION";
    	}else if(alertId.equals("05")){
    		type="SUDDENBRAKE";
    	}
    	log.info("Other alerts called ; Type verified =  "+alertId+"; type="+alertType);
    	try{
    		if(!type.isEmpty()){
             url="http://behappyjc.org/AmberConnect/public/push/sendpush?"
            		+ "AmberAuthToken="+vehicleData.getAmberToken()
            		+ "&Vin="+vehicleData.getVin()
            		+ "&UserToken="
            		+ "&Latitude="+latitude
            		+ "&Longitude="+longitude
            		+ "&Type="+type
            		+ "&SpeedValue="+speedValue;        	
        	alertsHitUrl(url);
            log.info("Alerts to PHP server "+url);
    		}
        	}catch(Exception exception){
        		log.error("Failed to give alert "+exception);
        	}
    	log.info("Alerts to PHP server "+url);
    }
    
    public void geoFenceCall(double latitude,double longitude) {
        //System.out.println("ReminderTask is completed by Java timer");
    	log.info("Geofence called in method ");
    	try{ 
    	long diff = new Date((Long)lastNode.getProperty("Time")).getTime() - new Date((Long)tripNode.getProperty("startTime")).getTime();
    	log.info("Geofence called in method ; Time diff = "+diff);
        String url="http://behappyjc.org/AmberConnect/public/push/geofence?"
        		+ "AmberAuthToken="+vehicleData.getAmberToken()
        		+ "&Vin="+vehicleData.getVin()
        		+ "&Latitude="+latitude
        		+ "&Longitude="+longitude
        		+ "&RunTime="+diff;
     
    	log.info("GEO Fence url "+url);
    	alertsHitUrl(url);
        log.info("GEO Fence url invoked to server "+url);
    	}catch(Exception exception){
    		log.error("Failed to geoFence alert "+exception);
    	}
    	log.info("GEO Fence url called end ");
    }
    
    

    class RemindTask extends TimerTask {
        String tripId;
        public RemindTask(String tripId){
            this.tripId=tripId;
        }
        @Override
        public void run() {
            //System.out.println("ReminderTask is completed by Java timer");

            URL oracle = null;
            try {
                oracle = new URL("http://behappyjc.org/AmberConnect/public/trips/updatetripname?TripId="+tripId);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(oracle.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) inputLine="";
                //System.out.println(inputLine);
            in.close();                
            } catch (Exception e) {
                //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }


    public void fetchLastnode(){
    	TraversalDescription friendsTraversal = gds.traversalDescription()
                .depthFirst()
                .relationships( RelTypes.NextPoint,Direction.OUTGOING );
        Node currentNode=tripNode;
        lastNode=null;
        boolean foundnextnode=true;
        while(foundnextnode){
            foundnextnode=false;
            for ( Node traverseNode : friendsTraversal
                    .traverse( currentNode )
                    .nodes() )
            {
                foundnextnode=true;
                currentNode=traverseNode;
                lastNode=currentNode;
            }
        }
    }

    public void deleteTrip(){
        TraversalDescription friendsTraversal = gds.traversalDescription()
                .depthFirst()
                .relationships( RelTypes.NextPoint,Direction.OUTGOING );
        Node currentNode=tripNode;
        boolean foundnextnode=true;
        while(foundnextnode){
            foundnextnode=false;
            for ( Node traverseNode : friendsTraversal
                    .traverse( currentNode )
                    .nodes() )
            {
                foundnextnode=true;
                for(Relationship rel:currentNode.getRelationships()){
                    rel.delete();
                }
                currentNode.delete();
                currentNode=traverseNode;
            }
            if(!foundnextnode) currentNode.delete();
        }
    } 
    
    public void mongoInsertNewTrip(Node tripNode,long runTime){
    	  mongoObj.newTrip(new Date((Long)tripNode.getProperty("startTime")),(String)tripNode.getProperty("tripId")
                  , (Double)tripNode.getProperty("Fuel")
                  ,(Integer)tripNode.getProperty("TopSpeed")
                  ,(Double)tripNode.getProperty("distance")
                  ,(String)tripNode.getProperty("AmberAuthToken")
                  ,(String)tripNode.getProperty("VINNumber"),runTime,
                  Long.parseLong(String.valueOf(tripNode.getProperty("IdleTime")))               //(Long)tripNode.getProperty("IdleTime")
                  ,tripNode.hasProperty("ha")?(Integer)tripNode.getProperty("ha"):0
                  ,tripNode.hasProperty("sb")?(Integer)tripNode.getProperty("sb"):0
                  ,tripNode.hasProperty("speeding")?(Integer)tripNode.getProperty("speeding"):0);
          log.info("--- New trip inserted into Mongo --- "+(String)tripNode.getProperty("VINNumber")+";"+(String)tripNode.getProperty("AmberAuthToken"));
    
    }
    public void mongoInsertTripSave(Node tripNode,Node currentNode,long runTime,boolean isForcefullStop){
    	mongoObj.endTripSave(new Date((Long) currentNode.getProperty("Time"))
        ,new Date((Long)tripNode.getProperty("startTime")),
        (String)tripNode.getProperty("AmberAuthToken"),
        (String)tripNode.getProperty("VINNumber"),
        (Double)tripNode.getProperty("Fuel"),
        (Integer)tripNode.getProperty("TopSpeed"),
        (Double)tripNode.getProperty("distance"),
        (String)tripNode.getProperty("tripId"),runTime,
        Long.parseLong(String.valueOf(tripNode.getProperty("IdleTime")))   //(Long)tripNode.getProperty("IdleTime")
        ,tripNode.hasProperty("ha")?(Integer)tripNode.getProperty("ha"):0
        ,tripNode.hasProperty("sb")?(Integer)tripNode.getProperty("sb"):0
        ,tripNode.hasProperty("speeding")?(Integer)tripNode.getProperty("speeding"):0
        ,isForcefullStop);

    }
    public void traverseandMovetoMongo(boolean isForcefullStop){
        boolean foundnextnode=true;
        TraversalDescription friendsTraversal = gds.traversalDescription().depthFirst().relationships( RelTypes.NextPoint,Direction.OUTGOING );
        Node currentNode=tripNode;
        long diff = new Date((Long)lastNode.getProperty("Time")).getTime() - new Date((Long)tripNode.getProperty("startTime")).getTime();
        log.info("Run time for trip "+diff);
        log.info("Idel Time inserted = "+String.valueOf(tripNode.getProperty("IdleTime")));    
        if(diff<0){
        	log.debug("Trip run time conflict occurs "+"start time "+(Long)tripNode.getProperty("startTime")+"; End time = "+(Long)lastNode.getProperty("Time"));
        }        
//        mongoObj.newTrip(new Date((Long)tripNode.getProperty("startTime")),(String)tripNode.getProperty("tripId")
//                , (Double)tripNode.getProperty("Fuel")
//                ,(Integer)tripNode.getProperty("TopSpeed")
//                ,(Double)tripNode.getProperty("distance")
//                ,(String)tripNode.getProperty("AmberAuthToken")
//                ,(String)tripNode.getProperty("VINNumber"),diff,
//                Long.parseLong(String.valueOf(tripNode.getProperty("IdleTime")))               //(Long)tripNode.getProperty("IdleTime")
//                ,tripNode.hasProperty("ha")?(Integer)tripNode.getProperty("ha"):0
//                ,tripNode.hasProperty("sb")?(Integer)tripNode.getProperty("sb"):0
//                ,tripNode.hasProperty("speeding")?(Integer)tripNode.getProperty("speeding"):0);
//        log.info("--- New trip inserted into Mongo --- "+(String)tripNode.getProperty("VINNumber")+";"+(String)tripNode.getProperty("AmberAuthToken"));
        mongoInsertNewTrip(tripNode,diff);
        while(foundnextnode){
            foundnextnode=false;
            for (Node traverseNode :friendsTraversal.traverse(currentNode).nodes())
            {
                foundnextnode=true;
                currentNode=traverseNode;
                 
                if(currentNode.hasLabel(DynamicLabel.label("AlertPoint"))){
                    mongoObj.addAlertPoint((Double) currentNode.getProperty("latitude")
                            , (Double) currentNode.getProperty("longitude")
                            , (Integer) currentNode.getProperty("Speed")
                            , (String) currentNode.getProperty("AlertType")
                            , currentNode.getProperty("AlertId").toString()
                            , new Date((Long) currentNode.getProperty("Time")));
                    log.info("--- Alert point inserted into Mongo --- "+ (String) currentNode.getProperty("AlertType"));
                }else{
                    mongoObj.addGpsPoint((Double)currentNode.getProperty("latitude")
                        ,(Double)currentNode.getProperty("longitude")
                        ,(Integer) currentNode.getProperty("Speed")
                        ,(Double) currentNode.getProperty("Journey")
                        ,(Double) currentNode.getProperty("Fuel")
                        ,new Date((Long)currentNode.getProperty("Time")));
                    log.info("--- GPS point inserted into Mongo --- "+(Double)currentNode.getProperty("latitude")+","+(Double)currentNode.getProperty("longitude"));
                }
            }
        }
        log.info("Idel time inserted is "+String.valueOf(tripNode.getProperty("IdleTime")));        
//        mongoObj.endTripSave(new Date((Long) currentNode.getProperty("Time"))
//                ,new Date((Long)tripNode.getProperty("startTime")),
//                (String)tripNode.getProperty("AmberAuthToken"),
//                (String)tripNode.getProperty("VINNumber"),
//                (Double)tripNode.getProperty("Fuel"),
//                (Integer)tripNode.getProperty("TopSpeed"),
//                (Double)tripNode.getProperty("distance"),
//                (String)tripNode.getProperty("tripId"),diff,
//                Long.parseLong(String.valueOf(tripNode.getProperty("IdleTime")))   //(Long)tripNode.getProperty("IdleTime")
//                ,tripNode.hasProperty("ha")?(Integer)tripNode.getProperty("ha"):0
//                ,tripNode.hasProperty("sb")?(Integer)tripNode.getProperty("sb"):0
//                ,tripNode.hasProperty("speeding")?(Integer)tripNode.getProperty("speeding"):0
//                ,isForcefullStop);
        mongoInsertTripSave(tripNode, currentNode, diff, isForcefullStop);
        log.info("--- End trip inserted into Mongo --- "+(String)tripNode.getProperty("VINNumber")+";"+(String)tripNode.getProperty("AmberAuthToken")+";"+tripNode.getProperty("tripId"));
    }
    
    
    public void updateBufferData(DBObject dbObject){
    	mongoObj.updateBufferData(dbObject);    	
    }
    public DBObject getExistingTripDetails(Date date){
    	log.info("Existing trip info called ");
    	long time=date.getTime();
    	DBObject dbObject= mongoObj.getExistingTripDetails(time);
    	return dbObject;
    }    
    public List<String> getExistingTripDetails_(Date date){
    	long time=date.getTime();
    	DBObject dbObject= mongoObj.getExistingTripDetails(time);    	
    	DBObject gpsPoint= (DBObject) dbObject.get("GPSPoints");
    	List<String> latlongPoints=new ArrayList<String>();
    	if(gpsPoint!=null){
    		String dt=gpsPoint.toString();
    		Pattern pattern=Pattern.compile("\\{.*?\\}\\s*,");
    		Pattern latLong=Pattern.compile("\"latitude\"\\s*:\\s*(.*?)\\s*,|\"longitude\"\\s*:\\s*(.*?)\\s*,|");
    		Matcher matcher=pattern.matcher(dt);    		
    		while(matcher.find()){
    			String row=dt.substring(matcher.start(), matcher.end());    			
    			Matcher matcher2=latLong.matcher(row);
    			String latlongData="";
    			while(matcher2.find()){
    				String col=row.substring(matcher2.start(), matcher2.end());
    				if(!col.isEmpty()){
    					latlongData+=col.replaceAll("latitude|longitude|\\{|:|,|\"|\\s*","")+",";
    				}
    			}
    			latlongPoints.add(latlongData.replaceAll(",$", ""));
    		}
    	}
    	System.out.println(latlongPoints.size()+" "+latlongPoints);
    	return latlongPoints;
    }
    
    //MATCH (n)
    //OPTIONAL MATCH (n)-[r]-()
    //DELETE n,r
    public static void main(String a[])
    {
    	Neo4j neo4j=null;
		try {
			neo4j = new Neo4j();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Date date=new Date(1417965077933l);
    	if(neo4j!=null){
    		//System.out.println(neo4j.getExistingTripByID(date));
    	}
    	//neo4j.hitUrl("TRIPID_1418900735000");
    	neo4j.removeBufferedTripStart("TRIPID_1422344624000");    	
    }
	public void processDtcAlerts(Date time,Vehicle vehicle,List<String> codes) {		
		for(String code:codes){			
			String description=mongoObj.findDtcDescription(code);
			String type="";
			if(code.startsWith("P")){
				type=Constant.DtcType.DTC_TYPE_POWERTRAIN;
			}else if(code.startsWith("C")){
				type=Constant.DtcType.DTC_TYPE_CHASSIS;
			}else if(code.startsWith("N")){
				type=Constant.DtcType.DTC_TYPE_NETWORK;
			}else if(code.startsWith("B")){
				type=Constant.DtcType.DTC_TYPE_BODY;
			}
			mongoObj.dtcAlertEntry(vehicle.getAmberToken(), vehicle.getVin(), time, code,type,description);
		}
	}
}
