package com.amber.server.db;

import com.amber.server.data.Vehicle;
import com.mongodb.*;
import com.mongodb.util.JSON;

import org.apache.log4j.Logger;
import org.bson.types.ObjectId;

import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 8/23/14
 * Time: 1:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class mongo {
	Logger log = Logger.getLogger(mongo.class.getName());
    BasicDBObject trip;
    MongoClient mongoClient;
    DB db;
    DBCollection trips;
    DBCollection dayCol;
    DBCollection weekCol;
    DBCollection monthCol;
    DBCollection alert;    
    DBCollection deviceMapping;
    DBCollection gentricDtcCodes;
    String mongoip="208.109.101.173";
    //String mongoip="localhost";
    
    DBCollection dtccodes;

    public mongo() throws UnknownHostException {
        mongoClient = new MongoClient( mongoip , 27017 );
        //mongo --host 208.109.101.173 -u amber -p eu5us5aeBohmae --authenticationDatabase amber
        db = mongoClient.getDB("amber");
        boolean auth = db.authenticate("amber", "eu5us5aeBohmae".toCharArray());
        trips = db.getCollection("Trips");
        dayCol = db.getCollection("day");
        weekCol=db.getCollection("week");
        monthCol=db.getCollection("month");
        alert=db.getCollection("alert");
        deviceMapping=db.getCollection("deviceMapping");
        
        gentricDtcCodes=db.getCollection("GentricDtcCodes");        		
        dtccodes=db.getCollection("dtccodes");
    }

    public void newTrip(Date startTime,String tripId, double Fuel,int TopSpeed,double distance,String Auth,String vin,long RunTime,long IdleTime,int ha,int sb,int speeding){
        //"TripId" : "1", "StartTime" : "2014-07-04 18:53:41", "EndTime" : "2014-07-04 19:18:06", "Distance" : "5.47", "Fuel" : "698", "HA" : "5", "SB" : "2", "TopSpeed" : "46", "Speeding" : "1",
        //ha	5
        //sb	2
        //speeding	1
        trip=new BasicDBObject();
        trip.put("_id", tripId);
        trip.put("StartTime",startTime);//	1408977357000
        trip.put("TripId",tripId);//	AF3OPRB41408977357000
        trip.put("Fuel",Fuel);//	0.2790000010281801
        trip.put("TopSpeed",TopSpeed);//	21
        trip.put("Distance",distance);//	2031
        trip.put("AmberAuthToken",Auth);//	AF3OPRB4
        trip.put("VINNumber",vin);//	121
        trip.put("RunTime",RunTime);
        trip.put("IdleTime",IdleTime);
        if(ha>0) trip.append("HA",ha);
        if(sb>0) trip.append("SB",sb);
        if(speeding>0) trip.append("Speeding",speeding);

        //VINNumber" : "121", "AmberAuthToken" : "AF3OPRB4
    }

    public void addGpsPoint(double latitude,double longitude,int Speed,double Journey,double Fuel,Date Time){
        // { "latitude" : "11.021231", "longitude" : "77.015973", "Speed" : "0", "Journey" : "0", "Fuel" : "0", "Time" : "2014-07-04 18:53:41" }

        //GPSPoints
        BasicDBObject GPSPoint=new BasicDBObject();
        GPSPoint.append("latitude",latitude)
                .append("longitude",longitude)
                .append("Speed",Speed)
                .append("Journey",Journey)
                .append("Fuel",Fuel)
                .append("Time",Time);
        BasicDBList GPSPoints=null;

        if(trip.containsKey("GPSPoints")){
            GPSPoints= (BasicDBList) trip.get("GPSPoints");
            GPSPoints.add(GPSPoint);

        }else{
            GPSPoints=new BasicDBList();
            GPSPoints.add(GPSPoint);
            trip.append("GPSPoints",GPSPoints);
        }

    }

    public void addAlertPoint(Double latitude, Double longitude, Integer speed, String alertType, String alertId, Date time) {
        BasicDBObject alertPoint=new BasicDBObject();
        alertPoint.append("latitude",latitude)
                .append("longitude",longitude)
                .append("Speed",speed)
                .append("alertType",alertType)
                .append("alertId",alertId)
                .append("Time",time);
        BasicDBList alertPoints=null;

        if(trip.containsKey("Alerts")){
            alertPoints= (BasicDBList) trip.get("Alerts");
            alertPoints.add(alertPoint);

        }else{
            alertPoints=new BasicDBList();
            alertPoints.add(alertPoint);
            trip.append("Alerts",alertPoints);
        }

        alertEntry((String)trip.get("AmberAuthToken"),(String)trip.get("VINNumber"),latitude, longitude, speed, alertType, alertId+"", time);
        //trip.put("AmberAuthToken",Auth);//	AF3OPRB4
        //trip.put("VINNumber",vin);//	121
    }
   
    public void dtcAlertEntry(String auth,String vin,Date time,String dtcAlertNo,String type,String description){
    	BasicDBObject alertData=new BasicDBObject();    	
        alertData.append("AmberAuthToken",auth)
                .append("VINNumber",vin).append("DTC_Codes", dtcAlertNo).append("Time", time)
                .append("Type", type)
                .append("Description", description)
                .append("ResolvedFlag", "Y");       
        dtccodes.insert(alertData);
    }
    public void alertEntry(String auth,String vin,Double latitude, Double longitude, Integer speed, String alertType, String alertId, Date time){
        BasicDBObject alertData=new BasicDBObject();
        alertData.append("AmberAuthToken",auth)
                .append("VINNumber",vin)
                .append("latitude",latitude)
                .append("longitude",longitude)
                .append("Speed",speed)
                .append("alertType",alertType)
                .append("alertId",alertId)
                .append("Time",time).append("Category","Driving");
        alert.insert(alertData);
    }

    public void endTripSave(Date endTime,Date tripStartDate,String Auth,String vin,double Fuel,int TopSpeed,double distance,String tripId,long RunTime,long IdleTime,int ha,int sb,int speeding,boolean isForcefullStop){
    	trip.append("force_full_stop",isForcefullStop);
    	trip.append("EndTime", endTime);
        trips.insert(trip);
        consolidateDay(tripStartDate,Auth,vin,Fuel,TopSpeed,distance,tripId,RunTime,IdleTime, ha, sb, speeding);
        consolidateWeek(tripStartDate, Auth, vin, Fuel, TopSpeed, distance, tripId, RunTime,IdleTime, ha, sb, speeding);
        consolidateMonth(tripStartDate, Auth, vin, Fuel, TopSpeed, distance, tripId, RunTime,IdleTime, ha, sb, speeding);
    }
    public void consolidateDay(Date tripStartDate,String Auth,String vin,double Fuel,int TopSpeed,double distance,String tripId,long RunTime,long IdleTime,int ha,int sb,int speeding){
        TimeZone tz = TimeZone.getTimeZone("IST");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(tz);
        String datelocal=formatter.format(tripStartDate);
        BasicDBObject dayFind=new BasicDBObject();
        dayFind.append("_id",Auth+vin+datelocal.replaceAll("-",""));
        if(dayCol.find(dayFind).count()>0){
            BasicDBObject incElem=new BasicDBObject();
            incElem.append("Distance",distance);
            incElem.append("Fuel",Fuel)
            //incElem.append("TopSpeed",TopSpeed)
                    .append("RunTime", RunTime)
                    .append("IdleTime",IdleTime);
            if(ha>0) incElem.append("HA",ha);
            if(sb>0) incElem.append("SB",sb);
            if(speeding>0) incElem.append("Speeding",speeding);

            BasicDBObject upsertElem =new BasicDBObject();
            upsertElem.append("$inc",incElem)
                    .append("$push",new BasicDBObject().append("Trips",tripId));

            dayCol.update(dayFind,upsertElem, true, false);
            dayFind.append("TopSpeed" , new BasicDBObject("$lt" ,TopSpeed));
            //{ key : "key1", value: { $lt : aValue }},
            //{ $set : { value : aValue }}
            dayCol.update(dayFind,new BasicDBObject("$set",new BasicDBObject("TopSpeed",TopSpeed)));
        }else{
            dayFind.append("Day",datelocal);
            dayFind.append("Distance",distance);
            dayFind.append("Fuel",Fuel);
            dayFind.append("TopSpeed",TopSpeed);
            BasicDBList list=new BasicDBList();
            list.add(tripId);
            dayFind.append("Trips",list)
                    .append("RunTime", RunTime)
                    .append("IdleTime",IdleTime);

            dayFind.append("VINNumber",vin);
            dayFind.append("AmberAuthToken",Auth);
            if(ha>0) dayFind.append("HA",ha);
            if(sb>0) dayFind.append("SB",sb);
            if(speeding>0) dayFind.append("Speeding",speeding);
            dayCol.insert(dayFind);
        }


        //"Day" : "2014-07-04", "User" : "1", "Device" : "1", "Distance" : "10.94", "Fuel" : "1396", "HA" : "10", "SB" : "4", "TopSpeed" : "46", "Speeding" : "2", "RunTime" : "2930",
        //"VINNumber" : "121", "AmberAuthToken" : "AF3OPRB4"
    }
    public void consolidateWeek(Date tripStartDate,String Auth,String vin,double Fuel,int TopSpeed,double distance,String tripId,long RunTime,long IdleTime,int ha,int sb,int speeding){
        TimeZone tz = TimeZone.getTimeZone("IST");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(tz);
        Calendar c = Calendar.getInstance();
        c.setTime(tripStartDate);
        c.setTimeZone(tz);
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        String WeekStart=formatter.format(c.getTime());
        c.add(Calendar.DAY_OF_WEEK, 6);
        String WeekEnd=formatter.format(c.getTime());
        BasicDBObject dayFind=new BasicDBObject();
        dayFind.append("_id",Auth+vin+WeekStart.replaceAll("-",""));
        if(weekCol.find(dayFind).count()>0){
            BasicDBObject incElem=new BasicDBObject();
            incElem.append("Distance",distance);
            incElem.append("Fuel",Fuel)
                   .append("RunTime",RunTime)
                   .append("IdleTime",IdleTime);
            if(ha>0) incElem.append("HA",ha);
            if(sb>0) incElem.append("SB",sb);
            if(speeding>0) incElem.append("Speeding",speeding);

            BasicDBObject upsertElem =new BasicDBObject();
            upsertElem.append("$inc",incElem)
                    .append("$push",new BasicDBObject().append("Trips",tripId));

            weekCol.update(dayFind,upsertElem, true, false);
            dayFind.append("TopSpeed" , new BasicDBObject("$lt" ,TopSpeed));
            weekCol.update(dayFind,new BasicDBObject("$set",new BasicDBObject("TopSpeed",TopSpeed)));

        }else{
            dayFind.append("WeekStart",WeekStart);
            dayFind.append("WeekEnd",WeekEnd);
            dayFind.append("Distance", distance);
            dayFind.append("Fuel",Fuel);
            dayFind.append("TopSpeed",TopSpeed)
                    .append("RunTime", RunTime)
                    .append("IdleTime",IdleTime);
            BasicDBList list=new BasicDBList();
            list.add(tripId);
            dayFind.append("Trips",list);

            dayFind.append("VINNumber",vin);
            dayFind.append("AmberAuthToken",Auth);
            if(ha>0) dayFind.append("HA",ha);
            if(sb>0) dayFind.append("SB",sb);
            if(speeding>0) dayFind.append("Speeding",speeding);
            weekCol.insert(dayFind);
        }


        //"Day" : "2014-07-04", "User" : "1", "Device" : "1", "Distance" : "10.94", "Fuel" : "1396", "HA" : "10", "SB" : "4", "TopSpeed" : "46", "Speeding" : "2", "RunTime" : "2930",
        //"VINNumber" : "121", "AmberAuthToken" : "AF3OPRB4"
        //"WeekStart" : "2014-06-30", "WeekEnd" : "2014-07-05"
    }


    public void consolidateMonth(Date tripStartDate,String Auth,String vin,double Fuel,int TopSpeed,double distance,String tripId,long RunTime,long IdleTime,int ha,int sb,int speeding){
        TimeZone tz = TimeZone.getTimeZone("IST");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(tz);
        Calendar c = Calendar.getInstance();
        c.setTime(tripStartDate);
        c.setTimeZone(tz);
        //c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        c.set(Calendar.DAY_OF_MONTH, 1);
        String monthStart=formatter.format(c.getTime());
        DateFormat formatterId = new SimpleDateFormat("yyyy-MM");
        formatterId.setTimeZone(tz);
        String monthId=formatterId.format(c.getTime());
        //c.add(Calendar.DAY_OF_WEEK, 6);
        //String WeekEnd=formatter.format(c.getTime());
        BasicDBObject dayFind=new BasicDBObject();
        dayFind.append("_id",Auth+vin+monthId.replaceAll("-",""));
        if(monthCol.find(dayFind).count()>0){
            BasicDBObject incElem=new BasicDBObject();
            incElem.append("Distance",distance);
            incElem.append("Fuel",Fuel)
            //incElem.append("TopSpeed",TopSpeed)
                    .append("RunTime", RunTime)
                    .append("IdleTime",IdleTime);
            if(ha>0) incElem.append("HA",ha);
            if(sb>0) incElem.append("SB",sb);
            if(speeding>0) incElem.append("Speeding",speeding);
            BasicDBObject upsertElem =new BasicDBObject();
            upsertElem.append("$inc",incElem)
                    .append("$push",new BasicDBObject().append("Trips",tripId));

            monthCol.update(dayFind,upsertElem, true, false);

            dayFind.append("TopSpeed" , new BasicDBObject("$lt" ,TopSpeed));
            monthCol.update(dayFind,new BasicDBObject("$set",new BasicDBObject("TopSpeed",TopSpeed)));
        }else{
            dayFind.append("Month",monthStart);
            dayFind.append("Distance",distance);
            dayFind.append("Fuel",Fuel);
            dayFind.append("TopSpeed",TopSpeed)
                    .append("RunTime", RunTime)
                    .append("IdleTime",IdleTime);
            BasicDBList list=new BasicDBList();
            list.add(tripId);
            dayFind.append("Trips",list);

            dayFind.append("VINNumber",vin);
            dayFind.append("AmberAuthToken",Auth);
            if(ha>0) dayFind.append("HA",ha);
            if(sb>0) dayFind.append("SB",sb);
            if(speeding>0) dayFind.append("Speeding",speeding);
            monthCol.insert(dayFind);
        }


        //"Day" : "2014-07-04", "User" : "1", "Device" : "1", "Distance" : "10.94", "Fuel" : "1396", "HA" : "10", "SB" : "4", "TopSpeed" : "46", "Speeding" : "2", "RunTime" : "2930",
        //"VINNumber" : "121", "AmberAuthToken" : "AF3OPRB4"
        //"WeekStart" : "2014-06-30", "WeekEnd" : "2014-07-05"
    }

    public static void main1(String [] args)
            throws Exception // Just for simplicity!
    {
        String fromDateString = "2014-09-04T03:47:31"+ "GMT-00:00";
        //DateFormat formatterPares = new SimpleDateFormat
        //        ("EEE MMM dd HH:mm:ss zzz yyyy");

        DateFormat formatterPares = new SimpleDateFormat ("yyyy-MM-dd'T'HH:mm:ssZ");
        Date fromDate = (Date)formatterPares.parse(fromDateString);
        //TimeZone central = TimeZone.getTimeZone("IST");
        //for(String str:TimeZone.getAvailableIDs())
        //    System.out.println(str);
        /*DateFormat formatter1 = new SimpleDateFormat
                ("yyyyMMdd");
        formatter.setTimeZone(central);
        System.out.println(formatter1.format(fromDate));
        Calendar c = Calendar.getInstance();
        c.setTime(fromDate);
        c.setTimeZone(central);
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        System.out.println("Date " + (c.getTime()));*/
        //c.add(Calendar.DAY_OF_WEEK, 6);
        //System.out.println("Date " + (c.getTime()));
        TimeZone tz = TimeZone.getTimeZone("IST");
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        formatter.setTimeZone(tz);
        Calendar c = Calendar.getInstance();
        c.setTime(fromDate);
        //c.setTimeZone(tz);
        c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        String WeekStart=formatter.format(c.getTime());
        System.out.println("WeekStart:"+WeekStart);
        c.add(Calendar.DAY_OF_WEEK, 6);
        String WeekEnd=formatter.format(c.getTime());
        System.out.println("WeekEnd:"+WeekEnd);
    }
    public String findDtcDescription(String code){    	
    	String description="";    	
    	BasicDBObject allQuery = new BasicDBObject();    	    	
    	BasicDBObject fields = new BasicDBObject();
    	fields.put("Code", code);		
    	DBCursor cursor = gentricDtcCodes.find(fields,allQuery);    	
    	while (cursor.hasNext()) {
			DBObject json=cursor.next();
			description=json.get("Description").toString();
			log.info("Code = "+code+"; Description = "+description);
			return description;
    	}    	
    	log.info("Code = "+code+"; Description = "+description);
    	return description; 
    }
	public int findOverSpeedInfo() {
		int speed=0;
		String speed_string="";
		BasicDBObject allQuery = new BasicDBObject();		
		BasicDBObject fields = new BasicDBObject();
		Vehicle vehicle=new Vehicle();
		fields.put("VinNumber", vehicle.getVin()); 
		fields.put("DeviceAuthenticationToken", vehicle.getAmberToken());		
		DBCursor cursor = deviceMapping.find(fields,allQuery);
			
		while (cursor.hasNext()) {
			DBObject json=cursor.next();			
			DBObject json_=(DBObject)json.get("Settings");
			speed_string=json_.get("OverSpeedLimit").toString();
		}
		if(!speed_string.isEmpty()){
			speed=Integer.parseInt(speed_string);
		}
		return speed;
	}	
	public DBObject getExistingTripDetails(long time){
		BasicDBObject allQuery = new BasicDBObject();		
		BasicDBObject fields = new BasicDBObject();
		fields.put("TripId", "TRIPID_"+time);
		DBCursor cursor = trips.find(fields,allQuery);				
		while (cursor.hasNext()) {
			DBObject json=cursor.next();
			return json;
		}
		return null;
	}
	public DBObject getExistingTripById(String id){
		BasicDBObject allQuery = new BasicDBObject();		
		BasicDBObject fields = new BasicDBObject();
		fields.put("TripId", id);
		DBCursor cursor = trips.find(fields,allQuery);				
		while (cursor.hasNext()) {
			DBObject json=cursor.next();
			return json;
		}
		return null;
	}
	public static void main(String a[]){
		mongo mongo=null;
		try {
			mongo = new mongo();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Result "+e);
		}
		String result=mongo.findDtcDescription("0417");
		System.out.println(result);
		
	}
	public static void main_(String a[]){
		mongo mongo=null;
		try {
			mongo = new mongo();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("Result "+e);
		}
	//	 System.out.println(mongo.getExistingTripById("TRIPID_1418978647000"));
		DBObject dbObject=mongo.getExistingTripById("TRIPID_1423131793000");
		BasicDBList dbObjectGpsPoints=(BasicDBList) dbObject.get("GPSPoints");
		Set<Long> time=new TreeSet<Long>();
		for(int i=0;i<dbObjectGpsPoints.size();i++){
			BasicDBObject object=(BasicDBObject) dbObjectGpsPoints.get(i);
			Date d=object.getDate("Time");
			System.out.println(d.getTime());
			time.add(d.getTime());
		}
		System.out.println(time);
	}
	public void updateBufferData(DBObject dbObject) {
		DBObject object=new BasicDBObject();
		object.put("TripId", dbObject.get("TripId").toString());
		trips.remove(object);
		
		trips.insert(dbObject);		
	}
}
