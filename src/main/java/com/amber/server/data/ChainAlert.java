package com.amber.server.data;

import com.amber.server.device.ParseData;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 9/9/14
 * Time: 12:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChainAlert {
    int protectionStatus=1;
    int reportInterval=30;
    int engineoffDelay=60;
    int overSpeedLimit=30;
    int overSpeedDelay=120;
    int fatigueDrivingDuration=4*60;
    int fatigueDrivingDelay=1*60*60;
    int vibrationAlarm=2;
    int obdStatus=1;

    public int getProtectionStatus() {
        return protectionStatus;
    }

    public void setProtectionStatus(int protectionStatus) {
        this.protectionStatus = protectionStatus;
    }

    public int getReportInterval() {
        return reportInterval;
    }

    public void setReportInterval(int reportInterval) {
        this.reportInterval = reportInterval;
    }

    public int getEngineoffDelay() {
        return engineoffDelay;
    }

    public void setEngineoffDelay(int engineoffDelay) {
        this.engineoffDelay = engineoffDelay;
    }

    public int getOverSpeedLimit() {
        return overSpeedLimit;
    }

    public void setOverSpeedLimit(int overSpeedLimit) {
        this.overSpeedLimit = overSpeedLimit;
    }

    public int getOverSpeedDelay() {
        return overSpeedDelay;
    }

    public void setOverSpeedDelay(int overSpeedDelay) {
        this.overSpeedDelay = overSpeedDelay;
    }

    public int getFatigueDrivingDuration() {
        return fatigueDrivingDuration;
    }

    public void setFatigueDrivingDuration(int fatigueDrivingDuration) {
        this.fatigueDrivingDuration = fatigueDrivingDuration;
    }

    public int getFatigueDrivingDelay() {
        return fatigueDrivingDelay;
    }

    public void setFatigueDrivingDelay(int fatigueDrivingDelay) {
        this.fatigueDrivingDelay = fatigueDrivingDelay;
    }

    public int getVibrationAlarm() {
        return vibrationAlarm;
    }

    public void setVibrationAlarm(int vibrationAlarm) {
        this.vibrationAlarm = vibrationAlarm;
    }

    public int getObdStatus() {
        return obdStatus;
    }

    public void setObdStatus(int obdStatus) {
        this.obdStatus = obdStatus;
    }

    public String generateklv(){
        StringBuilder packetklv=new StringBuilder();
        //Protection Status-- Byte
        packetklv.append("ff040005").append(ParseData.encodeBYTE(protectionStatus));

        //Report Interval-- Short
        packetklv.append("ff110006").append(ParseData.encodeSHORT(reportInterval));

        //Engine Off delay-- Short
        packetklv.append("ff120006").append(ParseData.encodeSHORT(engineoffDelay));

        //Over Speed Limit-- Short
        packetklv.append("ff130005").append(ParseData.encodeBYTE(overSpeedLimit));

        //Over Speed Delay-- Short
        packetklv.append("ff140006").append(ParseData.encodeSHORT(overSpeedDelay));

        //Fatique Driving Duration-- Short
        packetklv.append("ff150008").append(ParseData.encodeINTGER(fatigueDrivingDuration));

        //Fatique Driving Delay-- Short
        packetklv.append("ff160006").append(ParseData.encodeSHORT(fatigueDrivingDelay));

        //Vibration Sensitivity-- Short
        packetklv.append("ff460005").append(ParseData.encodeBYTE(vibrationAlarm));

        //OBD Status-- Short
        packetklv.append("ff470005").append(ParseData.encodeBYTE(obdStatus));
        return packetklv.toString();
    }

    public static void main(String arg[]){
        ChainAlert alert=new ChainAlert();
        System.out.println(alert.generateklv());
    }
}
