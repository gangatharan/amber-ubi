package com.amber.server.data;

import com.amber.server.receiver.SinocastleReceiver;

/**
 * Created with IntelliJ IDEA.
 * User: biju-hubbl
 * Date: 8/23/14
 * Time: 12:59 AM
 * To change this template use File | Settings | File Templates.
 */

public class Vehicle {
	
    private static String vin;
    private static String imei;    
    private static String AmberToken;
    
    public static boolean isInit=false;
    
    private ChainAlert alert;    
   
    public Vehicle(){
    	//TODO: hot coded value need to be removed    	
//      Sinocastel India
    	if(!isInit && SinocastleReceiver.country==1){
    		this.AmberToken="43JS7AIFZE68";
    		this.imei="3231334531313431373030303438000000000000";
    		this.vin="817543";
    		isInit=true;
    	}
//      Sinocastel USA
    	if(!isInit && SinocastleReceiver.country==2){
    		this.AmberToken="FHEZWG5J93RT";
        	this.imei="3231334531313431373030303439000000000000";
        	this.vin="419678";
        	isInit=true;
    	}    	
        alert=new ChainAlert();
    }

    public String getAlertKlv() {
        return alert.generateklv();
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getAmberToken() {
        return AmberToken;
    }

    public void setAmberToken(String amberToken) {
        AmberToken = amberToken;
    }
    public String toString(){
    	return getAmberToken()+";"+getImei()+";"+getVin()+";";
    }
}
