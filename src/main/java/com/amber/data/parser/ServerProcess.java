package com.amber.data.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

import com.amber.client.Constant;



public class ServerProcess {
	
	private static ServerProcess process=null;
	private static String url="";
	static Logger log = Logger.getLogger(ServerProcess.class.getName());
	
	public static  ServerProcess getInstance(){
		if(process==null){
			process=new ServerProcess();
			url="http://localhost:8080/amber-api/";
		}
		return process;
	}	
	public void toAlert(Map<String,Object> evaluationData) throws Exception{
		String query=url;
		try{
			HttpClient client = new DefaultHttpClient();            
			HttpPost post = new HttpPost(query+"alert");			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(12);			
			nameValuePairs.add(new BasicNameValuePair("lat",evaluationData.get(Constant.ResultName.KEY_LATITUDE).toString()));
			nameValuePairs.add(new BasicNameValuePair("lang",evaluationData.get(Constant.ResultName.KEY_LONGITUDE).toString()));
			nameValuePairs.add(new BasicNameValuePair("fuel",evaluationData.get(Constant.ResultName.KEY_TOTAL_FUEL).toString()));
			nameValuePairs.add(new BasicNameValuePair("speed",evaluationData.get(Constant.ResultName.KEY_SPEED).toString()));
			nameValuePairs.add(new BasicNameValuePair("distance",evaluationData.get(Constant.ResultName.KEY_CURRENT_DISTANCE).toString()));

			// TODO : ENGINE STATUS , ALERT_ID,ACCUMULATION hot coded value need to be updated to real time
			nameValuePairs.add(new BasicNameValuePair("engine_status","RUNNING"));
			nameValuePairs.add(new BasicNameValuePair("alert_id","01"));
			
			nameValuePairs.add(new BasicNameValuePair("device_id",evaluationData.get(Constant.ResultName.KEY_DEVICE_ID).toString()));
			nameValuePairs.add(new BasicNameValuePair("trip_id_time",evaluationData.get(Constant.ResultName.KEY_ACC_TIME).toString()));
			nameValuePairs.add(new BasicNameValuePair("time",evaluationData.get(Constant.ResultName.KEY_DATE).toString()));
			nameValuePairs.add(new BasicNameValuePair("accumulation","false"));			
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));			
			
			HttpResponse response = client.execute(post);
		}catch(Exception exception){
			log.error("Failed to hit the http server "+exception);
			exception.printStackTrace();
		}		
	}
	public void toGPS(Map<String,Object> evaluationData){
		String query=url;
		try{			
			HttpClient client = new DefaultHttpClient();            
			HttpPost post = new HttpPost(query+"gps");			
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(12);			
			nameValuePairs.add(new BasicNameValuePair("lat",evaluationData.get(Constant.ResultName.KEY_LATITUDE).toString()));
			nameValuePairs.add(new BasicNameValuePair("lang",evaluationData.get(Constant.ResultName.KEY_LONGITUDE).toString()));
			nameValuePairs.add(new BasicNameValuePair("fuel",evaluationData.get(Constant.ResultName.KEY_CURRENT_FUEL).toString()));
			nameValuePairs.add(new BasicNameValuePair("speed",evaluationData.get(Constant.ResultName.KEY_SPEED).toString()));
			nameValuePairs.add(new BasicNameValuePair("distance",evaluationData.get(Constant.ResultName.KEY_CURRENT_DISTANCE).toString()));
			nameValuePairs.add(new BasicNameValuePair("device_id",evaluationData.get(Constant.ResultName.KEY_DEVICE_ID).toString()));
			nameValuePairs.add(new BasicNameValuePair("trip_id_time",evaluationData.get(Constant.ResultName.KEY_ACC_TIME).toString()));
			nameValuePairs.add(new BasicNameValuePair("time",evaluationData.get(Constant.ResultName.KEY_DATE).toString()));
			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = client.execute(post);
		}catch(Exception exception){
			log.error("Failed to hit the http server "+exception);
			exception.printStackTrace();
		}		
	}	
	public void toServer(String query){
		
	}
	public static void main(String a[]){
		
	}
	
}
