package com.amber.data.parser;

import java.util.Map;

import com.amber.server.receiver.SinocastleReceiver;

public interface DataParser {
	public Map<String,Object> parse(byte[] bs,SinocastleReceiver receiver);
	
}
