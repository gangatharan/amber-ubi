package com.amber.data.parser;

import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;
import org.neo4j.graphdb.Node;

import com.amber.client.Constant;
import com.amber.server.Evaluate.AlarmChangeEvaluator;
import com.amber.server.Evaluate.DTCEvaluator;
import com.amber.server.Evaluate.GSensorEvaluator;
import com.amber.server.Evaluate.GenericEvaluator;
import com.amber.server.Evaluate.HeartBeatEvaluator;
import com.amber.server.Evaluate.LoginEvaluator;
import com.amber.server.Evaluate.PIDEvaluator;
import com.amber.server.Evaluate.SupportDataFlowType;
import com.amber.server.Evaluate.TerminalUploadGPSDataEvaluator;
import com.amber.server.Evaluate.UploadInSleepModeEvaluator;
import com.amber.server.data.Vehicle;
import com.amber.server.db.Neo4j;
import com.amber.server.receiver.SinocastleReceiver;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

public class Parser implements DataParser{
	static Logger log = Logger.getLogger(Parser.class.getName());
	int socketPort=0;
	String response;
	Map<String,HashMap<String,Object>> deviceSpecificInfo=new HashMap<String,HashMap<String,Object>>();
	public boolean isEngineRunning=false;
	public long currentTripAccTime=0l;
	public Date engineStartedTime=null;
	public Date accTime=null;
	//Neo4j neo4j=null;

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public Parser(int socketPort){
//		try {
//			neo4j=new Neo4j();
//		} catch (UnknownHostException e) {
//			e.printStackTrace();
//		}
		this.socketPort=socketPort;
	}
	
	public Map<String, Object> parse(byte[] bs, SinocastleReceiver sinocastleReceiver) {		
		Map<String,Object> parsedData=new HashMap<String,Object>();
		try{
		findProtocolType(bs, parsedData);
		contentEvaluate(bs, parsedData,sinocastleReceiver);
		}catch(Exception exception){
			exception.printStackTrace();
		}	
		return parsedData;
	}
	
	public void contentEvaluate(byte[] bs,Map<String,Object> parsedData, SinocastleReceiver sinocastleReceiver){
		if (parsedData.get(Constant.PTYPE_FIELD_TYPE) != null) {
			byte[] protocol_type = (byte[]) parsedData.get(Constant.PTYPE_FIELD_TYPE);
			char[] type_ = Hex.encodeHex(protocol_type);
			String type = new String(type_);
			evaluate(type, parsedData, bs,sinocastleReceiver);
		}
	}
	
//	public Node getLastLocation(){		
//		Node lastLocation=neo4j.getLastLocation();
//		log.info("Last location update called while engine stopped "+lastLocation);
//		if(lastLocation==null){
//			Vehicle vehicle=new Vehicle();
//			neo4j.fetchNode(vehicle);
//			lastLocation=neo4j.getLastLocation();
//		}
//		return lastLocation;
//	}
	
	public void updateLastLocation(Node lastLocation,Map<String,Object> data){
		// TODO: check the vehicle object and update the code according to that
//		Vehicle vehicle=new Vehicle();		
//		lastLocation.setProperty("AmberAuthToken",vehicle.getAmberToken());
//		lastLocation.setProperty("VINNumber",vehicle.getVin());	            
//		lastLocation.setProperty("latitude",Double.parseDouble(data.get(Constant.ResultName.KEY_LATITUDE).toString()));
//		lastLocation.setProperty("longitude",Double.parseDouble(data.get(Constant.ResultName.KEY_LONGITUDE).toString()));
//		lastLocation.setProperty("Time",(Date)data.get(Constant.ResultName.KEY_DATE));
//		lastLocation.setProperty("RunningStatus",0);
//		neo4j.setLastLocation(lastLocation);		
		List<Map<String,Object>> gpsArrayData=(List<Map<String, Object>>) data.get(Constant.ResultName.KEY_GPS_ARRAY_DATA);
		for(Map<String,Object> gps:gpsArrayData){
			Vehicle vehicle=new Vehicle();		
			lastLocation.setProperty("AmberAuthToken",vehicle.getAmberToken());
			lastLocation.setProperty("VINNumber",vehicle.getVin());	            
			lastLocation.setProperty("latitude",Double.parseDouble(gps.get(Constant.ResultName.KEY_LATITUDE).toString()));
			lastLocation.setProperty("longitude",Double.parseDouble(gps.get(Constant.ResultName.KEY_LONGITUDE).toString()));
			lastLocation.setProperty("Time",(Date)data.get(Constant.ResultName.KEY_DATE));
			lastLocation.setProperty("RunningStatus",0);
//			neo4j.setLastLocation(lastLocation);
		}		
	}
	
	public void processEngineStart(AlarmChangeEvaluator evaluator){
		log.info("* * * Engine started * * * ");
		Date startDate=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_DATE);
		Vehicle vehicle= new Vehicle();
		engineStartedTime=startDate;		
		if(accTime!=null){
			log.debug("Started when acc time exist; Trip id: "+((Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME)).getTime());
//			neo4j.tripStart(startDate,"TRIPID_"+((Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME)).getTime(),vehicle);
		}
//		if(neo4j.getLastLocation()!=null  ){		
//			try {
//				insertGPSPointFromAlerts(neo4j, evaluator.getEvaluationData());
//			}catch(Exception exception){
//				log.info("Exception in inserting  gps point  "+exception);
//			}		
//		}		
		log.info("Engine started ");
		isEngineRunning=true;				
	}
	
	
	public void processEngineStop(AlarmChangeEvaluator evaluator,boolean isForcefullStop){
		log.info(" Data info while stopping "+evaluator.getEvaluationData());
		log.info("* * * Engine stoped * * * ");				
//		long timeDiff=0l;				
//		try{				
//		Date timeAtAlame=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_DATE);				
//		if(engineStartedTime!=null && timeAtAlame!=null) {
//			 timeDiff=timeAtAlame.getTime()-engineStartedTime.getTime();
//			 log.info("Time Diff = "+timeDiff);
//		}	
//		}catch(Exception exception){
//			log.error(exception);
//		}
//		if(neo4j.getLastLocation()!=null && timeDiff>=0l ){		
		// Following commented for UBI - api implementation
//		if(neo4j.getLastLocation()!=null  ){
//			try {
//				insertGPSPointFromAlerts(neo4j, evaluator.getEvaluationData());
//			}catch(Exception exception){
//				log.info("Exception in inserting  gps point  "+exception);
//			}
//			endTrip(isForcefullStop);
//		}		
	}
	
	public void endTrip(boolean isForcefullStop){
		if(isEngineRunning){
			try{
//			neo4j.endTrip(isForcefullStop);
			}catch(Exception exception){
				log.error(exception);				
			}
			currentTripAccTime=0l;
			log.info("Trip deleted from neo4j ");
			engineStartedTime=null;
			isEngineRunning=false;
			//accTime=null;
		}
	}
	
	public void processGPSPoints(GenericEvaluator evaluator,Date latestAccTime){
		try{
			Map<String,Object> data=evaluator.getEvaluationData();
			log.info("GPS Data : "+data);			
			if(isEngineRunning && accTime!=null && latestAccTime.getTime()==accTime.getTime()){
//				insertGPSPoint(neo4j, data);				
			}else{
//				updateBufferGPS(neo4j,data,latestAccTime);
			}
			}catch(Exception exception){
				log.error("Exception occured while inserting GPS point to NEO4j", exception);
			}
	}
	public void evaluate(String type,Map<String,Object> parsedData,byte[] bs,SinocastleReceiver sinocastleReceiver){
		log.debug("Information type ----------------------------------------------------------------------------> "+type);
		if(Constant.InformationType.INFO_TYPE_LOGIN_LAUNCH.equals(type)){
			LoginEvaluator evaluator=new LoginEvaluator();			
			String resp=evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
			accTime=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME);
			setResponse(resp);
			try{
//			Node lastLocation=getLastLocation();
//			updateLastLocation(lastLocation, evaluator.getEvaluationData());
			}catch(Exception exception){
				log.error("Failed to update the last location "+exception);
			}
		}else if(Constant.InformationType.INFO_TYPE_LOGIN_CANCEL.equals(type)){
			sinocastleReceiver.setFetchNextPacket(false);
			
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_GPS.equals(type)){
			TerminalUploadGPSDataEvaluator evaluator=new TerminalUploadGPSDataEvaluator();
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));			
			Date latestAccTime=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME);
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
			processGPSPoints(evaluator,latestAccTime);
			log.info(evaluator.getEvaluationData());			
			ServerProcess.getInstance().toGPS(evaluator.getEvaluationData());			
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_PID.equals(type)){
			PIDEvaluator evaluator=new PIDEvaluator();
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));			
			Date latestAccTime=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME);			
			//processGPSPoints(evaluator,latestAccTime);
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_ALARMCHANGE.equals(type)){
			AlarmChangeEvaluator evaluator=new AlarmChangeEvaluator();
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
			String engineState=evaluator.getEvaluationData().get(Constant.ActionType.INFO_TYPE_ENGINE_STATE).toString();
			log.info("Alert data "+evaluator.getEvaluationData());			
			Date latestAccTime=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME); 
			try{
				//processAlarmChange(latestAccTime,engineState,evaluator);
				log.info(evaluator.getEvaluationData());
				ServerProcess.getInstance().toAlert(evaluator.getEvaluationData());
			}catch(Exception exception){
				log.error(exception);
				exception.printStackTrace();
			}
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_SUPPORT.equals(type)){
			SupportDataFlowType evaluator=new SupportDataFlowType();
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
			log.info(evaluator.getEvaluationData());
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_FIXED_UPLOAD.equals(type)){
			UploadInSleepModeEvaluator evaluator=new UploadInSleepModeEvaluator();			
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));			
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_SENSOR.equals(type)){
			GSensorEvaluator evaluator=new GSensorEvaluator();			
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
			try {
			//insertGPSPoint(new Neo4j(), evaluator.getEvaluationData());
			}catch(Exception exception){
				log.error(" Error while inserting data "+exception);
			}
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_DTC_PASSENGER.equals(type)){
			DTCEvaluator evaluator=new DTCEvaluator(type);
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));			
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
			dtcAlert(evaluator.evaluationData);
		}else if(Constant.InformationType.TERMINAL_UPLOAD_TYPE_DTC_COMMERCIAL.equals(type)){
			DTCEvaluator evaluator=new DTCEvaluator(type);
			evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));			
			evaluator.getEvaluationData().put(Constant.ResultName.KEY_DEVICE_ID, new Vehicle().getImei());
			dtcAlert(evaluator.evaluationData);
		}else if(Constant.InformationType.TERMINAL_HEARTBEAT.equals(type)){
			HeartBeatEvaluator evaluator=new HeartBeatEvaluator();
			String resp=evaluator.evaluate(parsedData,this.socketPort,SinocastleReceiver.convertToHex((byte[]) parsedData.get(Constant.PTYPE_FIELD_DEVICE_ID)));
			setResponse(resp);
		}		
	}
	public void dtcAlert(Map<String,Object> data){
		try {
			Vehicle vehicle=new Vehicle();			
			List<String> codes=(List<String>) data.get(Constant.ResultName.KEY_DTC_CODE_RESULT);
			Date time=(Date)data.get(Constant.ResultName.KEY_DATE);
//			neo4j.processDtcAlerts(time,vehicle,codes);
		}catch(Exception exception){
			log.error(exception.getMessage());
		}
	}
	public void updateLastLocation(TerminalUploadGPSDataEvaluator evaluator,Map<String,Object> data){		
		Node lastLocation=null;
		//getLastLocation();
		if(lastLocation!=null){
			Date timeAtGPS=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_DATE);				
			long timeDiff=0l;
			long lastlocationTime= 0l;
			lastlocationTime=Long.parseLong(lastLocation.getProperty("Time").toString());				
			if(timeAtGPS!=null) {
				timeDiff=timeAtGPS.getTime()-lastlocationTime;
			}				
			if(lastLocation!=null && timeDiff>=0l && "conventional".equals(data.get(Constant.ResultName.KEY_GPS_HISTORICAL_DATA))){
				updateLastLocation(lastLocation,data);
			}
		}else{
			log.debug("lost location null; not exist in neo4j "); 
		}	
	}
	
	public void processAlarmChange(Date latestAccTime,String engineState,AlarmChangeEvaluator evaluator){		
		if(accTime!=null && latestAccTime.getTime()>=accTime.getTime()){
			if(engineState.equals(Constant.ActionType.INFO_TYPE_ENGINE_START) && !isEngineRunning){				
				processEngineStart(evaluator);		
				accTime=latestAccTime;
			}else if(engineState.equals(Constant.ActionType.INFO_TYPE_ENGINE_STOP) && isEngineRunning){
				processEngineStop(evaluator,false);
			}else if(engineState.equals(Constant.ActionType.INFO_TYPE_ENGINE_STOP) && !isEngineRunning){
				updateBufferAlerts(latestAccTime,engineState,evaluator);
			}else{
				log.info("insert point called for alert data ");
				insertPoints(evaluator);
			}
		}else{
			log.debug("Buffered data ; latest ACC = "+latestAccTime+"; Global Acc = "+accTime); 
			updateBufferAlerts(latestAccTime,engineState,evaluator);
		}		
		
	}	
	public void insertPoints(AlarmChangeEvaluator evaluator){
		//(String alertType,int alertId, double latitude,double longitude,int speed,Date time)
		log.debug("insert points ");		
		Map<String,Object> data=evaluator.getEvaluationData();
		Date timeAtAlame=(Date)data.get(Constant.ResultName.KEY_DATE);				
		long timeDiff=0l;
		if(engineStartedTime!=null && timeAtAlame!=null) {
			 timeDiff=timeAtAlame.getTime()-engineStartedTime.getTime();
			 log.debug("Alarm Data; Time Diff = "+timeDiff); 
		}
		if(isEngineRunning && timeDiff>=0l) {			
			if (data.get(Constant.ResultName.KEY_ALERT_TYPE) != null
					&& !"16".equals(data.get(Constant.ResultName.KEY_ALERT_ID)
							.toString())
					&& !"17".equals(data.get(Constant.ResultName.KEY_ALERT_ID)
							.toString())) {
				log.debug("Inset alert point called insertPoints() "); 
				insertAlertPoint(data);
			} else {
				log.debug("insertGPSPoint() called insertPoints() ");
//				insertGPSPoint(neo4j, data);
			}
		} else {
			log.debug("insert into mongo direct called ");			
			insertToMongoDirect(data);
		}
	}
	public void findProtocolType(byte[] bs,Map<String,Object> parsedData){
		//index position 25 and 26 byte arrays are protocol_type
		if(bs.length>1) {
		byte[] protocol_type=new byte[2];		
		protocol_type[0]=bs[25];
		protocol_type[1]=bs[26];
		parsedData.put(Constant.PTYPE_FIELD_TYPE, protocol_type);		
		//
//log.info("Protocol type : "+SinocastleReceiver.convertToHex(protocol_type));
		findContent(bs, parsedData);
		}		
	}

	public void findContent(byte[] bs,Map<String,Object> parsedData){
		int length=bs.length;
		int topFiveLength=27;
		int bottomTwoLegth=4;
		byte[] content=new byte[length-(bottomTwoLegth+topFiveLength)];		
		for(int i=(topFiveLength),j=0;i<(length-bottomTwoLegth);i++,j++){
			content[j]=bs[i];
		}				
		parsedData.put(Constant.PTYPE_FIELD_CONTENT, content);
//log.info("Content : "+SinocastleReceiver.convertToHex(content));
//log.info("Length of content : "+content.length);
		findOBDDeviceId(bs, parsedData);
	}
	
	public void findOBDDeviceId(byte[] bs,Map<String,Object> parsedData){
		byte[] deviceId=new byte[20];
		for(int i=5,j=0;i<25;i++,j++){
			deviceId[j]=bs[i];
		}
		parsedData.put(Constant.PTYPE_FIELD_DEVICE_ID, deviceId);		
		log.info("Device Id : "+SinocastleReceiver.convertToHex(deviceId));
		log.info("Length of device id : "+deviceId.length);
		findProtocolVersion(bs, parsedData);		
	}	
	public void findProtocolVersion(byte[] bs,Map<String,Object> parsedData){
		if(bs.length>1) {
			byte[] protocol_version=new byte[1];
			protocol_version[0]=bs[4];
			parsedData.put(Constant.PTYPE_FIELD_PROTOCOL_VERSION,protocol_version);
		}
	}
	public void insertAlertPoint(Map<String,Object> data){
		log.debug("Insert alert point called ");
//		neo4j.insertAlertPoint(
//				data.get(Constant.ResultName.KEY_ALERT_TYPE).toString(),
//				data.get(Constant.ResultName.KEY_ALERT_ID).toString(),
//				Double.parseDouble(data.get(Constant.ResultName.KEY_LATITUDE).toString()),
//				Double.parseDouble(data.get(Constant.ResultName.KEY_LONGITUDE).toString()),
//				Integer.parseInt(data.get(Constant.ResultName.KEY_SPEED).toString()),
//				(Date)data.get(Constant.ResultName.KEY_DATE),this);		
//		insertGPSPoint(neo4j, data);
		log.debug("Insert alert point success fully called  ");
	}
	public void insertToMongoDirect(Map<String,Object> data){
		if(data.get(Constant.ResultName.KEY_ALERT_TYPE)!=null && !"16".equals(data.get(Constant.ResultName.KEY_ALERT_ID).toString()) && !"17".equals(data.get(Constant.ResultName.KEY_ALERT_ID).toString())) { 
//			neo4j.insertToMongoDirect(new Vehicle(),
//					data.get(Constant.ResultName.KEY_ALERT_TYPE).toString(),
//					data.get(Constant.ResultName.KEY_ALERT_ID).toString(),
//					Double.parseDouble(data.get(Constant.ResultName.KEY_LATITUDE).toString()),
//					Double.parseDouble(data.get(Constant.ResultName.KEY_LONGITUDE).toString()),
//					Integer.parseInt(data.get(Constant.ResultName.KEY_SPEED).toString()),
//					(Date)data.get(Constant.ResultName.KEY_DATE));
			log.info("Data is inserted while engine is stopped ");
		}
		log.info("Alert data "+data);
	}
	public void insertGPSPointFromAlerts(Neo4j neo4j,Map<String,Object> data){
//		neo4j.insertGpsPoint(
//				Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_FUEL).toString()),
//				Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_DISTANCE).toString()),
//				Double.parseDouble(data.get(Constant.ResultName.KEY_LATITUDE).toString()),
//				Double.parseDouble(data.get(Constant.ResultName.KEY_LONGITUDE).toString()),
//				Integer.parseInt(data.get(Constant.ResultName.KEY_SPEED).toString()),
//				(Date)data.get(Constant.ResultName.KEY_DATE),
//				this,
//				false);
		List<Map<String,Object>> gpsArrayData=(List<Map<String, Object>>) data.get(Constant.ResultName.KEY_GPS_ARRAY_DATA);
		for(Map<String,Object> gps:gpsArrayData){
			neo4j.insertGpsPoint(
					Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_FUEL).toString()),
					Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_DISTANCE).toString()),
					Double.parseDouble(gps.get(Constant.ResultName.KEY_LATITUDE).toString()),
					Double.parseDouble(gps.get(Constant.ResultName.KEY_LONGITUDE).toString()),
					Integer.parseInt(gps.get(Constant.ResultName.KEY_SPEED).toString()),
					(Date)data.get(Constant.ResultName.KEY_DATE),
				this,true);
		}
	}
	public void insertGPSPoint(Neo4j neo4j,Map<String,Object> data){
		try {		
			Date latestAccTime=(Date)data.get(Constant.ResultName.KEY_ACC_TIME);
		if(accTime!=null){
		if("conventional".equals(data.get(Constant.ResultName.KEY_GPS_HISTORICAL_DATA))	|| accTime.getTime()==latestAccTime.getTime()) {
			List<Map<String,Object>> gpsArrayData=(List<Map<String, Object>>) data.get(Constant.ResultName.KEY_GPS_ARRAY_DATA);
			for(Map<String,Object> gps:gpsArrayData){
				neo4j.insertGpsPoint(
						Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_FUEL).toString()),
						Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_DISTANCE).toString()),
						Double.parseDouble(gps.get(Constant.ResultName.KEY_LATITUDE).toString()),
						Double.parseDouble(gps.get(Constant.ResultName.KEY_LONGITUDE).toString()),
						Integer.parseInt(gps.get(Constant.ResultName.KEY_SPEED).toString()),
						(Date)data.get(Constant.ResultName.KEY_DATE),
					this,true);
			}			
		}else{
			log.info("Historical GPS data insertion filtered");
			updateBufferGPS(neo4j, data, (Date)data.get(Constant.ResultName.KEY_ACC_TIME));
		}
		}
		}catch(Exception exception){
			log.error("Error while inserting the GPS point "+exception);
			exception.printStackTrace();
		}
	}	
	public void updateBufferGPS(Neo4j neo4j,Map<String,Object> data,Date latestAccTime){
		DBObject trip= neo4j.getExistingTripDetails(latestAccTime);
		if(trip!=null) {
			log.debug("Trip  exist to update buffere GPS "+latestAccTime.getTime());			
			BasicDBList dbObjectGpsPoints=(BasicDBList) trip.get("GPSPoints");
			if(dbObjectGpsPoints==null){
				dbObjectGpsPoints=new BasicDBList();	
			}
			log.debug("GPS point size before update "+dbObjectGpsPoints.size());
			dbObjectGpsPoints=insertGPSPoint(dbObjectGpsPoints,data,true);
			log.debug("GPS point size after update "+dbObjectGpsPoints.size());
			
			trip.put("GPSPoints", dbObjectGpsPoints);
			trip=checkAndUpdateOtherData(trip,data,dbObjectGpsPoints);
			neo4j.updateBufferData(trip);			
			neo4j.hitUrl(trip.get("TripId").toString());
			log.debug("Buffered GPS point updated in existing trips");
		}else{
			log.debug("Trip not exist to update buffere GPS ");
		}
	}
	public DBObject checkAndUpdateOtherData(DBObject trip,Map<String,Object> data,BasicDBList dbObjectGpsPoints){		
		trip=checkAndUpdateFuelSpend(trip, data);
		trip=checkAndUpdateTopSpeed(trip, data);
		trip=checkAndUpdateIdelTime(trip, data, dbObjectGpsPoints);
		return trip;
	}

	public DBObject checkAndUpdateTopSpeed(DBObject trip,Map<String, Object> data) {
		try {
			int existSpeed = (Integer) trip.get("TopSpeed");
			int currentSpeed = Integer.parseInt(data.get(Constant.ResultName.KEY_SPEED).toString());
			if (currentSpeed > existSpeed) {
				trip.put("TopSpeed", currentSpeed);
			}
		} catch (Exception exception) {
			log.error("Failed to process top speed for buffer GPS data  \n"+exception.getMessage()+"\n"+exception);
		}
		return trip;
	}

	public DBObject checkAndUpdateFuelSpend(DBObject trip,Map<String, Object> data) {
		try {
			double existFuel = Double.parseDouble(trip.get("Fuel").toString());
			double currentFuel = Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_FUEL).toString());			
			if (existFuel < currentFuel) {
				trip.put("Fuel", currentFuel);
			}
		} catch (Exception exception) {
			log.error("Failed to process top fuel spend for buffer GPS data   \n"+exception.getMessage()+"\n"+exception);
		}
		return trip;
	}

	public DBObject checkAndUpdateIdelTime(DBObject trip,Map<String, Object> data, BasicDBList dbObjectGpsPoints) {
		try {
			int currentSpeed = Integer.parseInt(data.get(Constant.ResultName.KEY_SPEED).toString());			
			if(dbObjectGpsPoints.size()>3) {
			BasicDBObject gpsData = (BasicDBObject) dbObjectGpsPoints.get((dbObjectGpsPoints.size() - 2));
			int lastPointSpeed = Integer.parseInt(gpsData.get("Speed").toString());
			log.debug("Current Speed = "+currentSpeed+"; Last point speed = "+lastPointSpeed);				
			if (currentSpeed == 0 && lastPointSpeed == 0) {
				long existTime = ((Date) gpsData.get("Time")).getTime();
				long currentTime = ((Date) data.get(Constant.ResultName.KEY_DATE)).getTime();
				if (existTime < currentTime) {
					long idelTime = currentTime - existTime;
					trip.put("IdleTime", idelTime);
				}
			}
			}
		} catch (Exception exception) {
			log.debug("Size of GPS points : "+dbObjectGpsPoints.size());
			log.error("Failed to process idel time for buffer GPS data  \n"+exception.getMessage()+"\n"+exception);			
		}
		return trip;
	}
	public void updateBufferAlerts(Date latestAccTime,String engineState,AlarmChangeEvaluator evaluator){
//		DBObject trip= neo4j.getExistingTripDetails(latestAccTime);
//		Map<String, Object> data=evaluator.getEvaluationData();
//		try {
//			log.debug("Buffered alerts called ");
//			if(trip!=null) {				
//				 if ("17".equals(data.get(Constant.ResultName.KEY_ALERT_ID).toString())) {
//					 log.debug("Trip end point updated ");
//					updateTripEndPoint(trip, data);					
//				}else{ 
//					updateAlerts(trip, data);
//				}				
//			}else{
//				 if ("16".equals(data.get(Constant.ResultName.KEY_ALERT_ID).toString())) {
//					createTrip(trip, data,evaluator);					
//					updateBufferGPS(neo4j,data,latestAccTime);
//					neo4j.removeBufferedTripStart("TRIPID_"+((Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME)).getTime());
//				 }
//			}			
//		}catch(Exception exception){
//			log.error("Exception occured while updating bufferef alerts "+exception);
//			exception.printStackTrace();
//		}
//		log.debug("Buffered alerts completed");	
	}	
	public void createTrip(DBObject trip,Map<String, Object> data,AlarmChangeEvaluator evaluator){
		Date startDate=(Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_DATE);
		Vehicle vehicle=new Vehicle();
//		neo4j.createBufferedTripStart(startDate,"TRIPID_"+((Date)evaluator.getEvaluationData().get(Constant.ResultName.KEY_ACC_TIME)).getTime(),vehicle);		
//	
	}
	public void updateTripEndPoint(DBObject trip,Map<String, Object> data){		
		
		BasicDBList dbObjectAlerts=(BasicDBList) trip.get("Alerts");
		// EndTime, update last GPS point,Distance ,Fuel, RunTime, update gps address
		trip=setUpdateTripData(trip, data);
		BasicDBList dbObjectGpsPoints=(BasicDBList) trip.get("GPSPoints");
		if(dbObjectGpsPoints==null){
		dbObjectGpsPoints=new BasicDBList();
		}
		log.debug("Trip end point will be updated ; GPS size "+dbObjectGpsPoints.size());
		try{
			if(dbObjectGpsPoints.size()>=2){
				dbObjectGpsPoints.remove((dbObjectGpsPoints.size()-1));
				log.debug("last gps point in the trip removed "+dbObjectGpsPoints.size());
				
			}
						
		}catch(Exception exception){
			log.debug("Unable to remove last GPS point from the existing trip "+exception);
		}
		dbObjectGpsPoints=insertGPSPoint(dbObjectGpsPoints,data,false);		
		log.debug("Trip end point  updated ; GPS size "+dbObjectGpsPoints.size());
		// hit url		
		trip.put("GPSPoints", dbObjectGpsPoints);
		String tripId=trip.get("TripId").toString();
//		neo4j.updateBufferData(trip);
//		neo4j.hitUrl(tripId);
		log.debug("After update trip data :"+trip);
	}
	
	public DBObject setUpdateTripData(DBObject trip,Map<String, Object> data){		
		trip.put("EndTime", (Date)data.get(Constant.ResultName.KEY_DATE));
		trip.put("Distance", Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_DISTANCE).toString()));
		trip.put("Fuel", Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_FUEL).toString()));		
		Date startTime=(Date)trip.get("StartTime");
		long runTime=((Date)data.get(Constant.ResultName.KEY_DATE)).getTime()-startTime.getTime();		
		trip.put("RunTime", runTime);
		return trip;
	}
	
	
	public BasicDBList insertGPSPoint(BasicDBList dbObjectGpsPoints,Map<String, Object> data,boolean reorder){
//{ "latitude" : 11.023326666666666 ,"longitude" : 77.00185166666667 ,"Speed" : 1 ,"Journey" : 2298.0 , "Fuel" : 0.33 , 
//	"Time" : { "$date" : "2014-12-07T15:12:44.000Z"}} ,
		List<Map<String,Object>> gpsArrayData=(List<Map<String, Object>>) data.get(Constant.ResultName.KEY_GPS_ARRAY_DATA);
		log.debug("GPS point to inserted "+gpsArrayData.size());		
		for(Map<String,Object> gps:gpsArrayData){
			BasicDBObject gpsData=new BasicDBObject();
			gpsData.append("latitude", Double.parseDouble(gps.get(Constant.ResultName.KEY_LATITUDE).toString()));		
			gpsData.append("longitude", Double.parseDouble(gps.get(Constant.ResultName.KEY_LONGITUDE).toString()));
			gpsData.append("Speed", Integer.parseInt(gps.get(Constant.ResultName.KEY_SPEED).toString()));
			gpsData.append("Journey", Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_DISTANCE).toString()));
			gpsData.append("Fuel", Double.parseDouble(data.get(Constant.ResultName.KEY_CURRENT_FUEL).toString()));
			gpsData.append("Time", (Date)data.get(Constant.ResultName.KEY_DATE));		
			if(reorder){
				log.debug("GPS re ordering called ");
				dbObjectGpsPoints=doReorderGPS(dbObjectGpsPoints, gpsData);
			}else {
				log.debug("GPS directly inserted ");				
				dbObjectGpsPoints.add(gpsData);
			}
		}		
		return dbObjectGpsPoints;
	}
	
	public BasicDBList doReorderGPS(BasicDBList dbObjectGpsPoints,BasicDBObject gpsData){
		
		if(dbObjectGpsPoints.size()>1){
		try {
		BasicDBObject endPoint=(BasicDBObject) dbObjectGpsPoints.get(dbObjectGpsPoints.size()-1);
		dbObjectGpsPoints.remove(dbObjectGpsPoints.size()-1);
		dbObjectGpsPoints.add(gpsData);
		dbObjectGpsPoints.add(endPoint);
		log.debug("GPS point re ordered ");		
		}catch(Exception exception){
			log.error("Failed to update buffered gps point "+exception);
		}
		}else{
			log.debug("GPS point not re ordered ");
			dbObjectGpsPoints.add(gpsData);
		}
		
		return dbObjectGpsPoints;
	}
	public void updateAlerts(DBObject trip,Map<String, Object> data){
		BasicDBList dbObjectAlerts=(BasicDBList) trip.get("Alerts");
		if(dbObjectAlerts==null){
			dbObjectAlerts=new BasicDBList(); 
		}
		log.info("Existing alerts "+dbObjectAlerts);		
    	BasicDBObject alertData=new BasicDBObject();   	
    	Vehicle vehicle=new Vehicle();    	
    	alertData=setAlertData(data, vehicle,trip);
        dbObjectAlerts.add(alertData);
        
        log.info("After alerts update "+dbObjectAlerts);
        trip.put("Alerts", dbObjectAlerts);        
    
//        neo4j.updateBufferData(trip);
        log.debug("buffered alerts inserted into db "+dbObjectAlerts);       
	}
	
	public DBObject updateAlertsCount(DBObject trip,String type){
		log.debug("Before update : "+trip);
		try {
		if(trip.get("HA")==null && "HARSHACCELERATION".equals(type)){
			trip.put("HA", 1);
		}else if(trip.get("HA")!=null && "HARSHACCELERATION".equals(type)){
			int count=Integer.parseInt(trip.get("HA").toString())+1;
			trip.put("HA",count);
		}else if(trip.get("SB")==null && "SUDDENBRAKE".equals(type) ){
			trip.put("SB", 1);
		}else if(trip.get("SB")!=null && "SUDDENBRAKE".equals(type) ){
			int count=Integer.parseInt(trip.get("SB").toString())+1;
			trip.put("SB",count);
		}else if(trip.get("Speeding")==null && "OVERSPEED".equals(type) ){
			trip.put("Speeding", 1);
		}else if(trip.get("Speeding")==null  && "OVERSPEED".equals(type) ){
			int count=Integer.parseInt(trip.get("Speeding").toString())+1;
			trip.put("Speeding",count);
		}	
		}catch(Exception exception){
			log.error("Exception while updateAlerts count "+exception.getMessage());
			log.equals(exception);
		}
		log.debug("After update : "+trip);
		return trip;
	}
	
	public BasicDBObject setAlertData(Map<String, Object> data,Vehicle vehicle,DBObject trip){
		String alertId=data.get(Constant.ResultName.KEY_ALERT_ID).toString();
    	String type=findAlertType(alertId);
    	// 
		BasicDBObject alertData=new BasicDBObject();
    	log.debug("Alert type identified "+type);    	
		alertData.append("AmberAuthToken",vehicle.getAmberToken())
        .append("VINNumber",vehicle.getVin())
        .append("latitude", Double.parseDouble(data.get(Constant.ResultName.KEY_LATITUDE).toString()))
        .append("longitude",Double.parseDouble(data.get(Constant.ResultName.KEY_LONGITUDE).toString()))
        .append("Speed",Integer.parseInt(data.get(Constant.ResultName.KEY_SPEED).toString()))
        .append("alertType",type)
        .append("alertId",alertId)
        .append("Time",null).
         append("Category","Driving");		
		updateAlertsCount(trip,type);		
		// alerts 
		hitAlertURL(vehicle,data,type);
		return alertData;
	}
	public void hitAlertURL(Vehicle vehicleData,Map<String, Object> data,String type){
		String url="http://behappyjc.org/AmberConnect/public/push/sendpush?"
        		+ "AmberAuthToken="+vehicleData.getAmberToken()
        		+ "&Vin="+vehicleData.getVin()
        		+ "&UserToken="
        		+ "&Latitude="+Double.parseDouble(data.get(Constant.ResultName.KEY_LATITUDE).toString())
        		+ "&Longitude="+Double.parseDouble(data.get(Constant.ResultName.KEY_LONGITUDE).toString())
        		+ "&Type="+type
        		+ "&SpeedValue="+Integer.parseInt(data.get(Constant.ResultName.KEY_SPEED).toString());
		Neo4j.alertsHitUrl(url);
	}
	public static String findAlertType(String alertId){
		String type="";
    	if(alertId.equals("01")){
    		type="OVERSPEED";
    	}else if(alertId.equals("04")){
    		type="HARSHACCELERATION";
    	}else if(alertId.equals("05")){
    		type="SUDDENBRAKE";
    	}
    	return type;
	}
	public static void main(String a[]){
		int t=46;		 
		int tp=27;
		int bt=4;
		for(int i=tp+1;i<=(t-bt);i++){
			System.out.println(i);
		}		
	} 
}

