package com.amber.data.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trip {	
	
	public long tripId=0;
	public List<HashMap<String,Object>> gpsPoints=new ArrayList<HashMap<String,Object>>();
	public List<HashMap<String,Object>> alertPoints=new ArrayList<HashMap<String,Object>>();	
	
	public boolean isCompleteTrip=false;
	
	public boolean isStarted=false;
	public boolean isStopped=false;
	
	public Map<String,Object> tripStart=new HashMap<String,Object>(); 
	public Map<String,Object> tripEnd=new HashMap<String,Object>();
	
	
	public long getTripId() {
		return tripId;
	}

	public void setTripId(long tripId) {
		this.tripId = tripId;
	}
	
	
}
